<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
<div class="row col-md-12">
    
        <div class="col-md-6">
            <h3><i class="fa fa-bars"></i>Banner</h3>
            <hr class="hr-short">
			<div class="form-group">
					<? if(isset($row->about_us_banner)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->about_us_banner;?>" width="150">
					<? endif; ?>
						<label class="control-label">About Us Page Banner</label>
						<input type="file" name="about_us_banner"  class="form-control" >
			</div>
			<div class="form-group">
					<? if(isset($row->why_join_banner)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->why_join_banner;?>" width="150">
					<? endif; ?>
						<label class="control-label">Why Join Page Banner</label>
						<input type="file" name="why_join_banner"  class="form-control" >
			</div>
			
			<div class="form-group">
					<? if(isset($row->surf_board_detail_banner)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->surf_board_detail_banner;?>" width="150">
					<? endif; ?>
						<label class="control-label">Surf Board Detail Page Banner</label>
						<input type="file" name="surf_board_detail_banner"  class="form-control" >
			</div>
			
			
			
			<h3><i class="fa fa-bars"></i>Home Page Banner</h3>
            <hr class="hr-short">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Heading Top Text <span class="symbol required"></span></label>
                    <input type="text" name="headin_top_text" value="<?=@$row->headin_top_text?>" class="form-control" required><br>
					Color: <input type="color" name="headin_top_color" value="<?=@$row->headin_top_color?>" > 
					Font Size in PX: <input type="number" min="2"  step="0.5" name="headin_top_size" value="<?=@$row->headin_top_size?>" >
					Font Family: <input type="text"  name="headin_top_family" value="<?=@$row->headin_top_family?>" >
					Letter Spacing: <input type="number" min="0" max="50" step="0.01" name="headin_top_letter_space" value="<?=@$row->headin_top_letter_space?>" >
				</div>
            </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label"> Heading <span class="symbol required"></span></label>
                        <input type="text" name="main_heading" value="<?=@$row->main_heading?>" class="form-control" required><br>
					Color: <input type="color" name="main_color" value="<?=@$row->main_color?>" > 
					Font Size in PX: <input type="number" min="2"  step="0.5" name="main_size" value="<?=@$row->main_size?>" >
					Font Family: <input type="text"  name="main_family" value="<?=@$row->main_family?>" >
					Letter Spacing: <input type="number" min="0" max="50" step="0.01" name="main_family_space" value="<?=@$row->main_family_space?>" >
						
					</div>
                </div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Heading Sologan  <span class="symbol required"></span></label>
						<input type="text" name="main_heading_sologan" value="<?=@$row->main_heading_sologan?>" class="form-control" required><br>
					Color: <input type="color" name="heading_sologan_color" value="<?=@$row->heading_sologan_color?>" > 
					Font Size in PX: <input type="number" min="2"  step="0.5" name="heading_sologan_size" value="<?=@$row->heading_sologan_size?>" >
					Font Family: <input type="text"  name="heading_sologan_family" value="<?=@$row->heading_sologan_family?>" >
					Letter Spacing: <input type="number" min="0" max="50" step="0.01" name="main_sologan_space" value="<?=@$row->main_sologan_space?>" >
					</div>
				</div>
				
				<h3><i class="fa fa-bars"></i> How It Work</h3>
				<hr class="hr-short">
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Heading  <span class="symbol required"></span></label>
					<input type="text" name="how_heading" value="<?=@$row->how_heading?>" class="form-control" required></div>
				</div>
				<div class="form-group">
				<div class="col-md-12">
					<label class="control-label"> Heading text</label>
					<input type="text" name="how_detail" value="<?=@$row->how_detail?>" class="form-control" required></div>
				</div>
           
				<div class="form-group">
					<? if(isset($row->first_image)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->first_image;?>" width="150">
					<? endif; ?>
						<label class="control-label"> Firt Image</label>
						<input type="file" name="first_image"  class="form-control" ></div>
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Firt text</label>
						<textarea name="how_text1" class="form-control" required><?=@$row->how_text1;?></textarea>
					</div>
				</div>
				<div class="form-group">
					<? if(isset($row->second_image)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->second_image;?>" width="150">
					<? endif; ?>
					<div class="col-md-12">
						<label class="control-label"> Second Image</label>
						<input type="file" name="second_image"  class="form-control" ></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Second text</label>
						<textarea name="how_text2" class="form-control" required><?=@$row->how_text2;?></textarea>
					</div>
				</div>
				<div class="form-group">
					<? if(isset($row->third_image)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->third_image;?>" width="150">
					<? endif; ?>
					<div class="col-md-12">
						<label class="control-label"> Third Image</label>
						<input type="file" name="third_image"  class="form-control" >
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Third text</label>
						<textarea name="how_text3" class="form-control" required><?=@$row->how_text3;?></textarea>
					</div>
				</div>
        </div>
        <div class="col-md-6">
            <h3><i class="fa fa-bars"></i> Why Join</h3>
            <hr class="hr-short">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Heading <span class="symbol required"></span></label>
                     <input type="text" name="why_heading" value="<?=@$row->why_heading?>" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Description <span class="symbol required"></span></label>
					<textarea name="why_description" class="form-control" required><?=@$row->why_description;?></textarea>
                </div>
            </div>
           
           <div class="form-group">
					<? if(isset($row->why_image1)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->why_image1;?>" width="150" />
					<? endif; ?>
					<div class="col-md-12">
						<label class="control-label"> Firt Image</label>
						<input type="file" name="why_image1"  class="form-control" ></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> First Heading</label>
						<textarea name="why_heading1" class="form-control" required><?=@$row->why_heading1;?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Firt text</label>
						<textarea name="why_text1" class="form-control" required><?=@$row->why_text1;?></textarea>
					</div>
				</div>
				<div class="form-group">
				<? if(isset($row->why_image2)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->why_image2;?>" width="150" />
					<? endif; ?>
					<div class="col-md-12">
						<label class="control-label"> Second Image</label>
						<input type="file" name="why_image2"  class="form-control" ></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Second Heading</label>
						<textarea name="why_heading2" class="form-control" required><?=@$row->why_heading2;?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Second text</label>
						<textarea name="why_text2" class="form-control" required><?=@$row->why_text2;?></textarea>
					</div>
				</div>
				<div class="form-group">
				<? if(isset($row->why_image3)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->why_image3;?>" width="150">
					<? endif; ?>
					<div class="col-md-12">
						<label class="control-label"> Third Image</label>
						<input type="file" name="why_image3"  class="form-control" >
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Third Heading</label>
						<textarea name="why_heading3" class="form-control" required><?=@$row->why_heading3;?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Third text</label>
						<textarea name="why_text3" class="form-control" required><?=@$row->why_text3;?></textarea>
					</div>
				</div>
               <h3><i class="fa fa-bars"></i> Middle Banner</h3>
				<hr class="hr-short">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Heading <span class="symbol required"></span></label>
						 <input type="text" name="middle_banner_heading" value="<?=@$row->middle_banner_heading?>" class="form-control" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Description <span class="symbol required"></span></label>
						<textarea name="middle_banner_description" class="form-control" required><?=@$row->middle_banner_description;?></textarea>
					</div>
				</div>
                    <div class="clearfix"></div>
                    <div class="col-md-12"><hr class="hr-short">
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-green btn-lg" value="Update Settings">
                            </div>
                        </div>
                    </div>
                    <br><br>
                </form>
            </div>            <p>&nbsp;</p>
            <div class="clearfix"></div>
