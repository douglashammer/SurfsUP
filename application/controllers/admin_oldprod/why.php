public function about_us(){
		if($data = $this->input->post()){
			
			$this->db->where('web_page','about');
			$row = $this->db->get("web_setting")->row()->web_setting;			
			
			foreach($_FILES as $key=>$array){
				if($upload = $this->myadmin->do_upload($key)){
					if(!empty($upload)){
						$data[$key] = $upload;
					}
				}
			}
			
			$mydata = array_diff(json_decode($row,true),$data);
			$data = array_merge($mydata,$data);			
			
			$this->db->set('web_setting',json_encode($data));
			$this->db->where('web_page','about');
			$this->db->update("web_setting");
		}
		$this->db->where('web_page','about');
		$row = @$this->db->get("web_setting")->row()->web_setting;
		$this->data['row'] = json_decode($row);
		$this->data['page'] = 'pages/about';
		$this->load->view('admin/include/sitemaster',$this->data);
	}