
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Tables</a>
		</li>
		<li class="active">
			<strong>Data Tables</strong>
		</li>
	</ol>
	
	<h3>Add/Remove Surfboards</h3>
		<form action="" method="post">
		<div class="text-right">
			<button type="submit" class="btn btn-info btn-lg" >Update Order</button>
			<a href="<?=base_url(ADMIN.'/surfboards/add');?>" class="btn btn-info btn-lg btn-icon icon-left "> <i class="entypo-plus"></i> Add Surfboard</a>
		</div>
		<br><br>
		<script type="text/javascript">
			$(function() {
				var $table4 = jQuery( "#table-4" );
				$table4.DataTable({	
					dom: 'Bfrtip',
					buttons:
					[{
						extend: 'excel',
						text: 'Excel',
						exportOptions: {
							columns: 'th:not(:last-child)'
						}
					}],
					rowReorder: {
						selector: '.myreorder'
					},
					reorder:true,

					columnDefs: 
					[
						{ orderable: true, className: 'reorder', targets: 0 },
						{ orderable: true, className: 'reorder', targets: 1 },
						{ orderable: true, className: 'reorder', targets: 2 },
						{ orderable: true, className: 'reorder', targets: 3 },
						{ orderable: true, className: 'reorder', targets: 4 },
						{ orderable: false, targets: '_all' }
					],
					"bPaginate": false,
					"bLengthChange": false,
					"bFilter": true,
					"bInfo": false,
					"bAutoWidth": false,
					"language": {
							"sEmptyTable":   	"Nenhum dado na tabela",
							"sInfo":         	"_START_ bis _END_ von _TOTAL_ entradas",
							"sInfoEmpty":    	"0 a 0 de 0 entradas",
							"sInfoFiltered": 	"(filtrado por entradas _MAX_)",
							"sInfoPostFix":  	"",
							"sInfoThousands":  	".",
							"sLengthMenu":   	"_MENU_ Mostrar entradas",
							"sLoadingRecords": 	"Carregando...",
							"sProcessing":   	"Aguarde...",
							"sSearch":       	"Pesquisar",
							"sZeroRecords":  	"Nenhuma entrada disponível.",
							"oPaginate": {
								"sFirst":    	"Primeiro",
								"sPrevious": 	"Voltar",
								"sNext":     	"Próximo",
								"sLast":     	"Último"
							},
							"oAria": {
								"sSortAscending":  ": Ativar a ordenação da coluna crescente",
								"sSortDescending": ": Ativar a ordenação da coluna decrescente"
						}
					}
					});
			});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					<th>SN</th>
					<th>Título</th>
					<th>Localização</th>					
					<th>Condição</th>
					<th>Status</th>
					<th>Ações</th>
				</tr>
			</thead>
			
			<tbody>
				<!--sur_title	sur_location	sur_volume	sur_size	sur_condition	sur_quantity	sur_status-->
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					<td class="myreorder">
						<?=$sn++;?>
					</td>
					<td>
						<?=$r->sur_title;?>
						<input type="hidden" name="order[]" value="<?=$r->sur_id;?>">
					</td>
					<td><?=$r->lcc_title;?></td>
					<td><?=$r->sur_condition;?></td>
					<td><?=ucfirst($r->sur_status);?></td>
					<td>
						<a href="<?=base_url(ADMIN."/surfboards/add/{$r->sur_id}");?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Editar
						</a>				
						<a href="<?=base_url(ADMIN."/surfboards/emManutencao/{$r->sur_id}/{$r->sur_inmanutencao}");?>" class="btn 
							<?php echo $r->sur_inmanutencao==1?'btn-info':'btn-default'; ?> btn-sm btn-icon icon-left">
							<i class="entypo-tools"></i>
							Em Manutenção
						</a>
						<a href="<?=base_url(ADMIN."/surfboards/delete/{$r->sur_id}");?>" class="btn btn-danger btn-sm btn-icon icon-left delete_confirm">
							<i class="entypo-cancel"></i>
							Deletar
						</a>
						
					</td>
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		
		</form>
