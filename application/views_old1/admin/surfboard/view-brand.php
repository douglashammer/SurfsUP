
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Region</a>
		</li>
		<li class="active">
			<strong>Region</strong>
		</li>
	</ol>
	
	<h3>Add/Edit  Surfboard Brands</h3>
		<div class="text-right">
			<a href="<?=base_url(ADMIN.'/surfboards/add_brand');?>" class="btn btn-info btn-lg btn-icon icon-left "> <i class="entypo-plus"></i> Add Brand</a>
		</div>
		<br><br>
		<script type="text/javascript">
		jQuery( window ).load( function() {
			var $table4 = jQuery( "#table-4" );
			$table4.DataTable();
		});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					<th>SN</th>
					<th>Title</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					
					<td><?=$sn++;?></td>
					<td><?=$r->sur_brands;?></td>
					<td><?=($r->brand_status==1)?'Active':'Inactive';?></td>
					<td>
						<a href="<?=base_url(ADMIN."/surfboards/add_brand/{$r->brand_id}");?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
						<!--
						<a href="<?=base_url(ADMIN."/surfboards/deleteCategory/{$r->type_id}");?>" class="btn btn-danger btn-sm btn-icon icon-left delete_confirm">
							<i class="entypo-cancel"></i>
							Delete
						</a>
						-->
					</td>
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		
