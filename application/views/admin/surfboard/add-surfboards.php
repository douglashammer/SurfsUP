
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="<?=base_url(ADMIN.'/surfboards/');?>">Surfboards</a>
		</li>
		<li class="active">
			<strong>Add/Edit Surfboards</strong>
		</li>
	</ol>

	<h3>Add/Edit Surfboars</h3>
	<form role="form" action="" method="post" class="form-horizontal form-groups-bordered" enctype="multipart/form-data">
			
			<div class="form-group">
				<label for="field-1"  class="col-sm-3 control-label">Title</label>
				
				<div class="col-sm-5">
					<input type="text" required name="sur_title" value="<?=@$row->sur_title;?>" class="form-control" id="field-1" placeholder="Title">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Select Location</label>
				
				<div class="col-sm-5">
					<select name="sur_location" required class="form-control">
						<option value="">--Select--</option>
					<? $get_location = $this->myadmin->get_location(); ?>
					<? foreach($get_location->result() as $location): ?>
						<option <?=$location->lcc_id==@$row->sur_location?'selected':'';?> value="<?=$location->lcc_id;?>"><?=$location->lcc_title;?></option>
					<? endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group">
			   <label for="field-1" class="col-sm-3 control-label">Volume </label>
			   <div class="col-sm-5">
				  <input  type="number" step="0.1" name="sur_volume" value="<?=@$row->sur_volume;?>" class="form-control" id="field-1" placeholder="eg 2.8">
			   </div>
			</div>
			<div class="form-group">
			   <label for="field-1" class="col-sm-3 control-label">Length </label>	
			   <div class="col-sm-5">					
				  <input  type="number" step="0.1" name="sur_length" value="<?=@$row->sur_length;?>" class="form-control" id="field-1" placeholder="eg 2.7">
			   </div>
			</div>
			<div class="form-group">
			   <label for="field-1" class="col-sm-3 control-label">Type </label>								
				<div class="col-sm-5">
					<select name="sur_type" required class="form-control">
						<option value="">--Select--</option>
					<? foreach($type->result() as $t): ?>
						<option <?=$t->type_id==@$row->sur_type?'selected':'';?> value="<?=$t->type_id;?>"><?=$t->sur_types;?></option>
					<? endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group">
			   <label for="field-1" class="col-sm-3 control-label">Brand </label>								
			   <div class="col-sm-5">
					<select name="sur_brand" required class="form-control">
						<option value="">--Select--</option>
					<? foreach($brand->result() as $b): ?>
						<option <?=$b->brand_id==@$row->sur_brand?'selected':'';?> value="<?=$b->brand_id;?>"><?=$b->sur_brands;?></option>
					<? endforeach; ?>
					</select>
				</div>
			   </div>
			<div class="form-group">
			   <label for="field-1" class="col-sm-3 control-label">Size</label>
			   <div class="col-sm-5">
				  <input  type="text"  name="sur_size" value="<?=@$row->sur_size;?>" class="form-control" id="field-1" placeholder="eg 5'4 x 19.75 x 2.38">
			   </div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Condition</label>
				
				<div class="col-sm-5">
					<input  type="text" name="sur_condition" value="<?=@$row->sur_condition;?>" class="form-control" id="field-1" placeholder="eg Perfect">
				</div>
			</div>
			<div class="form-group" style="display:none;">
				<label class="col-sm-3 control-label">Quantity</label>
				
				<div class="col-sm-5">
				
					<div class="input-spinner">
						<button type="button" class="btn btn-primary btn-sm">-</button>
						<input type="hidden" name="sur_quantity" class="form-control size-1 input-sm"  value="1" />
						<button type="button" class="btn btn-primary btn-sm">+</button>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Show This Home Page</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input value="1" type="checkbox" <?=@$row->sur_home==1?'checked':'';?> name="sur_home" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" <?=@$row->sur_status=='active'?'checked':'';?> name="sur_status" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Description</label>
				
				<div class="col-sm-5">
					<textarea name="sur_description" rows="10" class="form-control" placeholder="Description"><?=@$row->sur_description;?></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="preview-area">
				<?php
					if(isset($images)):
						foreach($images->result() as $img) : ?>
					<div class="uploadedImages">
						
						<img  src="<?=base_url(UPLOAD_PATH.$img->sui_images)?>" height="100px" />
						<input type="hidden" name="images[<?=$img->sui_id;?>]">
						<a href="#" class="btn btn-danger remove_img"><i class="entypo-trash"></i> Remove</a>
					</div>
				<?php
						endforeach;
					endif;?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Select Images</label>
				<div class="col-sm-5">
					
					<input type="file" id="image-input" name="images[]" multiple="1" />
					
				</div>
			</div>
			<div class="col-sm-12">
			<center>
				<div style="width:80%">
					<table class="table table-bordered property_table ">
						<thead>
							<tr>
								<td colspan="4"><h2>Add Remove Surfboard Properties</h2></td>
							</tr>
							<tr>
								<th>Property</th>
								<th>Value</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
				<?php
				if(isset($properties)):
					foreach($properties->result() as $property): ?>
							<tr>
								<td><input type="text" value="<?=$property->sbp_property;?>" name="sbp_property[]" class="form-control size-4 input-sm" placeholder="Property" /></td>
								<td><input type="text" value="<?=$property->sbp_value;?>" name="sbp_value[]" class="form-control size-4 input-sm" placeholder="Value" /></td>
								<td>
									<div class="make-switch" data-on-label="Active" data-off-label="Inactive">
										<input type="checkbox" <?=$property->sbp_status=='active'?'checked':'';?> name="sbp_status[]"  />
									</div>
								</td>
								<td>
									<a href="" class="btn btn-danger btn-sm btn-icon icon-left remove_property">
										<i class="entypo-trash"></i>
										Delete
									</a>
									
								</td>
							</tr>
				<?php
					endforeach;
				endif;
				?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4" align="right">
									<a href="#" class="btn btn-info btn-sm btn-icon icon-left add_property"> <i class="entypo-plus"></i> Add New Property</a>
								</td>
							</tr>
						</tfoot>

					</table>
				</div>
			</div>
			<div class="form-group">			
				<div class="col-sm-8 text-center">
					<input type="submit" lass="form-control" class="btn btn-success bt-large" value="Save">
				</div>
			</div>
	</form>
<style>
.uploadedImages{
	text-align:center;
	width:150px;
	float:left;
	margin:4px 4px;
	padding:1px;
	border:1px solid gray;
	border-radius:2px;
}
</style>
<script>
$(function(){
	$('.add_property').click(function(e){
		e.preventDefault();
		$('.property_table tbody').append('<tr> <td><input type="text" name="sbp_property[]" class="form-control size-4 input-sm" placeholder="Property" /></td> <td><input type="text" name="sbp_value[]" class="form-control size-4 input-sm" placeholder="Value" /></td> <td> <div class="make-switch" data-on-label="Active" data-off-label="Inactive"> <input type="checkbox" name="sbp_status[]" checked /> </div> </td> <td> <a href="" class="btn btn-danger btn-sm btn-icon icon-left remove_property"> <i class="entypo-trash"></i> Delete </a> </td> </tr>').removeProperty();
	});
	$.fn.removeProperty = function(){
		$(".remove_property").each(function(){
			$(this).click(function(e){
				e.preventDefault();
				$(this).parent().parent().remove();
			});
		});
		
	}
	$(".remove_property").each(function(){
		$(this).click(function(e){
			e.preventDefault();
			$(this).parent().parent().remove();
		});
	});
	$(".remove_img").each(function(){
		$(this).click(function(e){
			e.preventDefault();
			$(this).parent().find("input[type=hidden]").val("deleted");
			$(this).parent().hide(500);
		});
	});
});
var inputLocalFont = document.getElementById("image-input");
inputLocalFont.addEventListener("change",previewImages,false); //bind the function to the input

function previewImages(){
    var fileList = this.files;

    var anyWindow = window.URL || window.webkitURL;

        for(var i = 0; i < fileList.length; i++){
          //get a blob to play with
          var objectUrl = anyWindow.createObjectURL(fileList[i]);
          // for the next line to work, you need something class="preview-area" in your html
          $('.preview-area').append('<div class="uploadedImages"><img src="' + objectUrl + '" height="100px" /></div>');
          // get rid of the blob
          window.URL.revokeObjectURL(fileList[i]);
        }


}
</script>