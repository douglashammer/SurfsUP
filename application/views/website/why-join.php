<!doctype html>
<html>
<head>
<title>Porque participar – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}	
?>
<main>


<section id="sBanner" style="background-image: url('<?=base_url(UPLOAD_PATH."website/".$this->website_m->CMS('home')->why_join_banner);?>');">
    <div class="contain">
        <div class="content">
            <h1><?=@$website_why->top_main_heading;?></h1>
            <p class="pre" style="margin-top:10px;"><?=@$website_why->top_main_sub_heading;?></p>
        </div>
    </div>
</section>
<!-- sBanner -->


<section id="whyJoin">
    <div class="contain">
        <div class="content">
            <ul class="lst flex">
			<? foreach($row->result() as $r): ?>
                <li>
                    <div class="inner">
                        <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$r->why_image);?>" alt=""></div>
                        <h4><?=$r->why_title;?></h4>
                        <p><?=$r->why_description;?></p>
                    </div>
                </li>
			<? endforeach; ?>
                
            </ul>
            <div class="bTn text-center"><a href="<?=base_url();?>plans" class="webBtn lgBtn colorBtn"><?=$buttons->why_button?></a></div>
        </div>
    </div>
</section>
<!-- whyJoin -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>