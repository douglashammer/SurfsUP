
	<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="<?=base_url(ADMIN);?>">
						<img src="<?=base_url(CLIENT_ASSETS);?>images/logo.svg" width="80px" alt="">
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
									
			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				<li class=" has-sub">
					<a href="<?=base_url(ADMIN.'/admin/dashboard');?>">
						<i class="entypo-gauge"></i>
						<span class="title">Dashboard</span>
					</a>
					
				</li>
			<? if($this->myadmin->is_admin()): ?>
				<li class="has-sub <?=isset($manage_active)?'opened':'';?>">
					<a href="#">
						<i class="entypo-layout"></i>
						<span class="title">Gerenciar Website</span>
					</a>
					<ul>
						<li>
							<a href="<?=base_url(ADMIN.'/home/index');?>">
								<span class="title">Gerencia Home Page</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/home/buttons');?>">
								<span class="title">Gerencia buttons</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/home/slider');?>">
								<span class="title">Gerencia Slider</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/home/about_us');?>">
								<span class="title">Gerenciar Sobre nós</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/home/people');?>">
								<span class="title">Gerneciar sobre nós</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/home/whyPage');?>">
								<span class="title">Gerenciar página Por Que Participar</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/home/why');?>">
								<span class="title">Gerenciar conteúdo de Porque 
								Participar</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/home/help');?>">
								<span class="title">Gerenciar Help Desk</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/home/how');?>">
								<span class="title">Gerenciar How Works</span>
							</a>
						</li>	
						<li>
							<a href="<?=base_url(ADMIN.'/home/tos');?>">
								<span class="title">Terms and Conditions</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/home/pp');?>">
								<span class="title">Privacy Policy</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/home/contact');?>">						
								<span class="title">Website Contact</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/admin/website');?>">						
								<span class="title">Website Settings</span>
							</a>
						</li>
						
					</ul>
				</li>
				<li class="has-sub">
					<a href="#">
						<i class="entypo-layout"></i>
						<span class="title">Gerenciar Payment</span>
					</a>
					<ul>
						<li>
							<a href="<?=base_url(ADMIN.'/api');?>">
								<span class="title">Update API Keys</span>
							</a>
						</li>
						
					</ul>
				</li>
				<li class="has-sub <?=isset($location_active)?'opened':'';?>">
					<a href="#">
						<i class="entypo-layout "></i>
						<span class="title">Gerenciar Localização</span>
					</a>
					<ul>
						<li>
							<a href="<?=base_url(ADMIN.'/location/category');?>">
								<span class="title">Add Região</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/location');?>">
								<span class="title">Add Localização</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub <?=isset($surfboard_active)?'opened':'';?>">
					<a href="#">
						<i class="entypo-layout"></i>
						<span class="title">Gerenciar SURFBOARDS</span>
					</a>
					<ul>
						<li>
							<a href="<?=base_url(ADMIN.'/surfboards/view_type');?>">
								<span class="title">Add/Edit Tipos</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/surfboards/view_brand');?>">
								<span class="title">Add/Edit Marcas</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/surfboards');?>">
								<span class="title">View Surfboards</span>
							</a>
						</li>						
						<li>
							<a href="<?=base_url(ADMIN.'/surfboards/add');?>">
								<span class="title">Add Surfboards</span>
							</a>
						</li>
						
					</ul>
				</li>
				
				<li class="has-sub <?=isset($packages_active)?'opened':'';?>">
					<a href="#">
						<i class="entypo-layout"></i>
						<span class="title">Gerenciar Pacotes</span>
					</a>
					<ul>

						<li>
							<a href="<?=base_url(ADMIN.'/packages');?>">
								<span class="title">Pacotes</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/packages/add');?>">
								<span class="title">Add Pacotes</span>
							</a>
						</li>
						
					</ul>
				</li>
				<li class="has-sub <?=isset($subscription_active)?'opened':'';?>">
					<a href="#">
						<i class="entypo-layout"></i>
						<span class="title">Gerenciar Assinatura</span>
					</a>
					<ul>

						<li>
							<a href="<?=base_url(ADMIN.'/subscription');?>">
								<span class="title">Inscrições</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/subscription/add');?>">
								<span class="title">Adicionar Incrição</span>
							</a>
						</li>
						
					</ul>
				</li>
				<li class="has-sub <?=isset($members_active)?'opened':'';?>">
					<a href="#">
						<i class="entypo-layout"></i>
						<span class="title">Membros</span>
					</a>
					<ul>

						<li>
							<a href="<?=base_url(ADMIN.'/members');?>">
								<span class="title">Membros Ativos</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/members/inactive');?>">
								<span class="title">Membros Inativos</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/members/premium');?>">
								<span class="title">Gerenciar pagamento de membros </span>
							</a>
						</li>
						
					</ul>
				</li>	
			<? endif; ?>				
				<li class="has-sub <?=isset($reservations_active)?'opened':'';?> <?=(!$this->myadmin->is_admin())?'opened':'';?>">
					<a href="#">
						<i class="entypo-layout"></i>
						<span class="title">Gerenciar Reservas</span>
					</a>
					<ul>
						<li>
							<a href="<?=base_url(ADMIN.'/orders/detalhe');?>">
								<span class="title">Todas as Reservas</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/orders/today');?>">
								<span class="title">Reservas para hoje</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/orders/issued');?>">
								<span class="title">Reservas para emitir hoje</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/orders/completed');?>">
								<span class="title">Reservas concluídas</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/orders/canceled');?>">
								<span class="title">Reservas canceladas</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/orders/delayedSurf');?>">
								<span class="title">Pranchas de surf atrasadas</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/orders');?>">
								<span class="title">(Legado) Todas as reservas</span>
							</a>
						</li>
					</ul>
				</li>
				<? if($this->myadmin->is_admin()): ?>
				<li class="has-sub <?=isset($users_active)?'opened':'';?>">
					<a href="#">
						<i class="entypo-user"></i>
						<span class="title">Gerenciar o administrador de local</span>
					</a>
					<ul>

						<li>
							<a href="<?=base_url(ADMIN.'/users');?>">
								<span class="title">Ver administrador de localização</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/users/add');?>">
								<span class="title">Adicionar administrador de local</span>
							</a>
						</li>

						
					</ul>
				</li>
				<li class="has-sub <?=isset($coupon_active)?'opened':'';?>">
					<a href="#">
						<i class="entypo-user"></i>
						<span class="title">Gerenciar Coupon</span>
					</a>
					<ul>

						<li>
							<a href="<?=base_url(ADMIN.'/coupon');?>">
								<span class="title">Coupon</span>
							</a>
						</li>
						<li>
							<a href="<?=base_url(ADMIN.'/coupon/add');?>">
								<span class="title">Add Coupon</span>
							</a>
						</li>

						
					</ul>
				</li>
				
				<? endif; ?>
				
					
			</ul>
			
		</div>

	</div>
