<!doctype html>
<html>
<head>
<title>Minha conta – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php require_once('includes/header-logged.php'); ?>
<main>


<section id="dash">
    <?php require_once('includes/topbar.php'); ?>
    <div class="contain">
        <div class="mainBlk" id="profileSet">
            <div class="flexRow flex">
                <div class="col col1">
                    <div class="blk">
                        <div class="_header">
                            <h3>Minha conta</h3>
						</div>
						<!--Show Message Success/error-->
				<? if($this->session->flashdata('message_success')): ?>
					<div class="alert alert-success">
						<strong>Sucesso.</strong> <?=$this->session->flashdata('message_success');?>
					</div>

				<? endif; ?>
				<? if($this->session->flashdata('message_error')): ?>
					<div class="alert alert-danger">
						<strong>Erro.</strong> <?=$this->session->flashdata('message_error');?>
					</div>

				<? endif; ?>
				<? if($this->website_m->checkAccounMissing()): ?>
					<div class="alert alert-info">
						<strong>Atenção:</strong> por favor preencha o perfil abaixo para seguir com a reserva da sua prancha.
					</div>

				<? endif; ?>
				<!--End Show Message Success/error-->
				<? if(validation_errors()): ?>
					<div class="alert alert-danger">
						<strong>Erro.</strong> <?=validation_errors();?>
					</div>

				<? endif; ?>
                        
                        <div class="mainSetting">
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="row formRow">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xx-12 txtGrp">
                                        <div class="proDp ico">
											<? if(isset($user->mem_pic)): ?>
												<? if(!empty($user->mem_pic)): ?>
													<img src="<?=base_url(UPLOAD_AVATAR).$user->mem_pic?>" id="upDP" alt="">
												<? endif; ?>
											<? endif; ?>
                                            <div class="icoMask uploadImg" id="uploadDp" data-image-src="dp">
                                                <i class="fi-camera"></i> escolher <br> imagem
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                                        <input type="text" required name="mem_first" value="<?=@$user->mem_first;?>"  class="txtBox" placeholder="Primeiro nome">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                                        <input type="text" required name="mem_last"  value="<?=@$user->mem_last;?>" class="txtBox" placeholder="Último nome">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                                        <input type="text" name="mem_email" value="<?=@$user->mem_email;?>" <?=!($this->website_m->checkEmail())?"readonly":"";?>  class="txtBox" placeholder="Endereço de e-mail">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                                        <input type="text"  name="mem_phone" value="<?=@$user->mem_phone;?>"   class="txtBox" placeholder="Número de telefone">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp ">
                                        <select name="mem_country"  class="txtBox selectpicker selectCountry" data-live-search="true">
                                            <option value="">País</option>
                                            <?php
											$countries = $this->website_m->get_countries();
											foreach($countries->result() as $country): ?>
												<option value="<?=$country->c_id;?>" <?=@$user->mem_country==$country->c_id?'selected':'';?> ><?=$country->c_title;?></option>
											
											<? endforeach; ?>
                                        </select>
                                    </div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp ">
                                        <input type="text"  value="<?=@$user->mem_passport;?>" name="mem_passport" <?php
											if(isset($user->mem_country)){
												if($user->mem_country==31){
													echo 'pattern="[0-9]{3}[\.]{1}[0-9]{3}[\.]{1}[0-9]{3}[\-]{1}[0-9]{2}"';
												}
											}
?>										class="txtBox passport" placeholder="Passaporte">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                                        <input type="text"  value="<?=@$user->mem_city;?>" name="mem_city"  class="txtBox" placeholder="Cidade ou estado">
                                    </div>
									
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                                        <input type="text"  value="<?=@$user->mem_zip;?>" name="mem_zip"  class="txtBox" placeholder="Código postal">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                                        <input type="text"  value="<?=@$user->mem_address;?>" name="mem_address"  class="txtBox" placeholder="Endereço">
                                    </div>
                                </div>
                                <input type="file" id="uploadFile" name="profile_pic" class="uploadFile" data-file="">
                                <div class="bTn text-center">
                                    <button type="reset" class="webBtn cnclBtn">Cancelar</button>
                                    <button type="submit" class="webBtn colorBtn">Salvar</button>
                                </div>
                            </form>
							<script>
							$(function(){
								$(".selectCountry").change(function(){
									if($(this).val()==31){
										$(".passport").attr("pattern","[0-9]{3}[\.]{1}[0-9]{3}[\.]{1}[0-9]{3}[\-]{1}[0-9]{2}");
										$(".passport").attr("placeholder","XXX.XXX.XXX-XX");
										console.log('brazil');
									}else{
										$(".passport").removeAttr("pattern");
										$(".passport").attr("placeholder","Passaporte");
									}
								});
							});
							
							</script>
                        </div>
                    </div>
                </div>
                <div class="col col2">
                    <div class="blk">
                        <div class="_header">
                            <h3>AlterarSenha</h3>
                        </div>
                        <div class="changePass">
                            <form action="<?=base_url('account/changepaswd');?>" method="post">
								<!--Show Message Success/error-->
								<? if($this->session->flashdata('message_success_pwd')): ?>
									<div class="alert alert-success">
										<strong>Sucesso.</strong> <?=$this->session->flashdata('message_success_pwd');?>
									</div>

								<? endif; ?>
								<? if($this->session->flashdata('message_error_pwd')): ?>
									<div class="alert alert-danger">
										<strong>Erro.</strong> <?=$this->session->flashdata('message_error_pwd');?>
									</div>

								<? endif; ?>
								<!--End Show Message Success/error-->
                                <div class="content">
                                    <p>Sua senha deve conter o seguinte:</p>
                                    <ol class="_list2">
                                        <li>Pelo menos 8 caracteres de comprimento (uma senha forte tem pelo menos 14 caracteres)</li>
                                        <li>Pelo menos 1 letra e pelo menos 1 número ou símbolo</li>
                                    </ol>
                                </div>
                                <div class="row formRow">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-xx-4 txtGrp">
                                        <input type="password" name="old_password"  class="txtBox" placeholder="Current Password">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-xx-4 txtGrp">
                                        <input type="password" name="new_password"  class="txtBox" placeholder="New Password">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-xx-4 txtGrp">
                                        <input type="password" name="renew_password"  class="txtBox" placeholder="Confirm new Password">
                                    </div>
                                </div>
                              
                                <div class="bTn text-center">
                                    <button type="reset" class="webBtn cnclBtn">Cancelar</button>
                                    <button type="submit" class="webBtn colorBtn">Alterar Senha</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- dash -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>