<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends CI_Controller {
	public $data;
	
	public function __construct(){		
		parent::__construct();
		$this->myadmin->is_login();
		if(!$this->myadmin->is_admin()){
			redirect(ADMIN);
		}
		$this->data['location_active'] = true;
	}
	/*Show Login Page*/
	public function index()
	{
		$this->data['row'] = $this->db
							->select('*')
							->from('location')
							->join('location_category','location.loc_id = location_category.loc_id')
							->get();
		$this->data['page'] = 'location/location';
		
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	public function add($id=''){
		if(!empty($id)){
			$where = ['lcc_id'=>$id];
			$this->data['row'] = $this->db->get_where("location",$where)->row();
			
			
		}
		if($this->input->post()){
			
			$data = $this->input->post();
			$data['lcc_status'] = isset($data['lcc_status'])?'active':'inactive';
			
			if(!empty($id)){
				$this->db->where($where);
				$this->db->set($data);
				$this->db->update("location");
				$this->myadmin->success("Location Successfully Updated");
				redirect(ADMIN.'/location/');
			}
			if($this->db->insert("location",$data)){
				$this->myadmin->success("Location Successfully Saved");
				redirect(ADMIN.'/location/');
			}else{
				$this->myadmin->success("Error While Saving");
			}
		}
		
		$this->data['get_category'] = $this->db->get("location_category");
		$this->data['page'] = 'location/add-location';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	public function delete($id)
	{
		if(!empty($id)){
			$this->db->where('lcc_id',$id);
			if($this->db->delete("location")){
				$this->myadmin->success("Successfully Deleted");
				redirect(ADMIN.'/location/');
			}
		}
		$this->myadmin->error("Error While Deleting");
		redirect(ADMIN.'/location/');
	}
	public function deleteCategory($id)
	{
		if(!empty($id)){
			$this->db->where('loc_id',$id);
			$image = $this->db->get("location_category")->row()->loc_image;
			@unlink(UPLOAD_PATH."website/".$image);
			$this->db->where('loc_id',$id);
			if($this->db->delete("location_category")){
				$this->myadmin->success("location category Deleted");
				redirect(ADMIN.'/location/category');
			}
		}
		$this->myadmin->error("Error While Deleting");
		redirect(ADMIN.'/location/');
	}
	public function category()
	{
		$this->data['row'] = $this->db->get("location_category");
		$this->data['page'] = 'location/category';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function addCategory($id=''){
		if(!empty($id)){
			$where = ['loc_id'=>$id];
			$this->data['row'] = $this->db->get_where("location_category",$where)->row();
			$this->db->where($where);
		}
		if($this->input->post()){
			$data = $this->input->post();
			$data['loc_status'] = isset($data['loc_status'])?'active':'inactive';
			if($upload = $this->myadmin->do_upload('image')){
				if(!empty($upload)){
					$data['loc_image'] = $upload; //UPLOAD_PATH."website/"
				}
			}
			if(!empty($id)){
				$this->db->set($data);
				$this->db->update("location_category");
				$this->myadmin->success("Location Category Successfully Updated");
				redirect(ADMIN.'/location/category');
			}
			if($this->db->insert("location_category",$data)){
				$this->myadmin->success("Location Category Successfully Saved");
				redirect(ADMIN.'/location/category');
			}else{
				$this->myadmin->success("Error While Saving");
			}
		}
		$this->data['page'] = 'location/add-category';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	//public function add_
	
}
