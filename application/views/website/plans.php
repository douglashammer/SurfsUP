<!doctype html>
<html>
<head>
<title>Nossos Planos – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}	
?>
<main>
<? if($showSteps): ?>
<div class="txtGrp text-center steps">
	<p><span>Passo 2 de 3:</span> Escolha um dos nossos planos e realize o pagamento para prosseguir com a reserva da prancha. Se preferir, pode pular essa etapa</p>
</div>
	
<div class="txtGrp text-right steps">
	<p><a class="webBtn colorBtn smBtn" href="<?=base_url("browse-surfboards");?>">Pular etapa &raquo;</a></p>
</div>
<? endif; ?>
<section id="plan" class="planPg">		
    <div class="contain text-center">
		<? if($this->session->flashdata('select_plan')): ?>
			<div class="alert alert-danger">
				<strong>Erro.</strong> <?=$this->session->flashdata('select_plan');?>
			</div>

		<? endif; ?>
		<? if($this->session->flashdata("error_order")):?>
			<div class="alert alert-danger">
				<strong>Aviso </strong><?=$this->session->flashdata("error_order")?>
			</div>
		<? endif; ?>
        <h1 class="secHeading">Assinaturas</h1>
        <ul class="lst flex">
		<? foreach($subscription->result() as $perSub): 
				$pre = '';
				if( strtolower($perSub->pkg_duration)=='year'){
					$pre = '12x';
					
				}
				if(strtolower($perSub->pkg_duration)=='month' || strtolower($perSub->pkg_duration)=='year'){
					$perSub->pkg_duration = 'mês';
					
				} else if(strtolower($perPkg->pkg_duration)=='day'){
				   $perPkg->pkg_duration = '&nbsp;dia';
			         }
				
		?>
			<li>
				<div class="planBlk">
					<!-- <h3>Prime</h3> -->
					<div class="icon"><i class="fi-diamond"></i></div>
					<div class="price"><?=$perSub->pkg_title;?></div>
					<small><?=$pre;?>R$ <?=$perSub->pkg_price;?> /por <?=$perSub->pkg_duration;?></small>
					<ul class="list">
					<? foreach(@$this->website_m->get_properties_by_id($perSub->pkg_id)->result() as $property): ?>
						<li><?=$property->tpp_property;?> <em> <?=$property->tpp_value;?></em></li>
					<? endforeach; ?>
					</ul>
					<div class="bTn"><?=$this->website_m->showButton($perSub->pkg_id);?></div>
				</div>
			</li>
		<? endforeach; ?>
		
          
        </ul>
        <hr>
        <h1 class="secHeading">Pacotes</h1>
        <ul class="lst lst2 flex">
           <? foreach($packages->result() as $perPkg):
				$pre = '';
				if( strtolower($perPkg->pkg_duration)=='year'){
					$pre = '12x';
					
				}		   
			if(strtolower($perPkg->pkg_duration)=='month' || strtolower($perPkg->pkg_duration)=='year'){
					$perPkg->pkg_duration = 'mês';
				} else if(strtolower($perPkg->pkg_duration)=='day'){
				$perPkg->pkg_duration = '&nbsp;dia';
			}
				
		   ?>
			<li>
				<div class="planBlk">
					<!-- <h3>Flex</h3> -->
					<div class="icon"><i class="fi-glass"></i></div>
					<div class="price"><?=$perPkg->pkg_title;?></div>
					<small><?=$pre;?>R$<?=$perPkg->pkg_price;?> /por <?=$perPkg->pkg_time.$perPkg->pkg_duration;?></small>
					<ul class="list">
					<? foreach($this->website_m->get_properties_by_id($perPkg->pkg_id)->result() as $property): ?>
						<li><?=$property->tpp_property;?> <em> <?=$property->tpp_value;?></em></li>
					<? endforeach; ?>
					</ul>
					<div class="bTn"><?=$this->website_m->showButton($perPkg->pkg_id);?></div>
				</div>
			</li>
		<? endforeach; ?>
            
        </ul>
    </div>
</section>
<!-- plan -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>