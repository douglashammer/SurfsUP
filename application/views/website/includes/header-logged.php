<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMNR93G" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<?php
$this->website_m->future_resrvation_header();
?>
<header class="ease fix">
    <div class="contain">
        <div class="logo ease">
            <a href="<?=base_url();?>"><img src="<?=base_url(CLIENT_ASSETS);?>images/logo.svg" alt=""></a>
        </div>
        <div class="toggle"><span></span></div>
        <div class="proIco dropDown">
            <div class="inside dropBtn">
				<a href="<?=base_url();?>reservation"><?=(isset($header)?"<strong>Reservas ({$header})</strong>":"<strong>Reservas (0)</strong>");?></a>
                <div class="ico">
                    <img src="<?=base_url(UPLOAD_AVATAR).($this->website_m->get_member_image()?$this->website_m->get_member_image():'avatar.png');?>" alt="">
                </div>
            </div>
            <ul class="proDrop dropCnt">
                <li><a href="<?=base_url();?>minha-conta"><i class="fi-webpage"></i>Minha Conta</a></li>
                <li><a href="<?=base_url();?>packages"><i class="fi-list"></i>Pacotes</a></li>
                <li><a href="<?=base_url();?>help"><i class="fi-question-circle"></i>Ajuda</a></li>
                <li><a href="<?=base_url();?>signout"><i class="fi-power-switch"></i>Sair</a></li>
            </ul>
        </div>
        <nav class="semi ease">
            <ul id="nav">
                <li class="<?php if($page=="home"){echo 'active';} ?>">
                    <a href="<?=base_url();?>"><?=$this->website_m->CMS('buttons')->my_home_button?></a>
                </li>
                <li class="<?php if($page=="surfs-up" || $page=="sobre-nos" || $page=="como-funciona" || $page=="por-que-ser-socio" ){echo 'active';} ?> drop">
                    <a href="javascript:void(0)"><?=$this->website_m->CMS('buttons')->my_Surfs_up_button?></a>
                    <ul class="sub">
                        <li class="<?php if($page=="sobre-nos"){echo 'active';} ?>">
                            <a href="<?=base_url();?>sobre-nos"><?=$this->website_m->CMS('buttons')->my_Surfs_up_1stbutton?></a>
                        </li>
                        <li class="<?php if($page=="como-funciona"){echo 'active';} ?>">
                            <a href="<?=base_url();?>como-funciona"><?=$this->website_m->CMS('buttons')->my_Surfs_up_2ndbutton?></a>
                        </li>
                        <li class="<?php if($page=="por-que-ser-socio"){echo 'active';} ?>">
                            <a href="<?=base_url();?>por-que-ser-socio"><?=$this->website_m->CMS('buttons')->my_Surfs_up_3rddbutton?></a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if($page=="planos" || $page=="pranchas" || $page=="contato"){echo 'active';} ?>">
                    <a href="<?=base_url();?>planos"><?=$this->website_m->CMS('buttons')->my_plan_button?></a>
                </li>
                <li class="<?php if($page=="pranchas"){echo 'active';} ?>">
                    <a href="<?=base_url();?>pranchas"><?=$this->website_m->CMS('buttons')->my_browse_surfboard_button?></a>
                </li>
                <li class="<?php if($page=="contato"){echo 'active';} ?>">
                    <a href="<?=base_url();?>contato"><?=$this->website_m->CMS('buttons')->my_contact_button?></a>
                </li>
            </ul>
        </nav>
    </div>
</header>
<!-- header -->


<div class="upperlay"></div>
<!-- <div id="pageloader">
    <span class="loader"></span>
</div> -->
