<!doctype html>
<html>
<head>
<title>Detalhes do Produto – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}	
?>
<main>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<section id="sBanner" style="background-image: url('<?=base_url(UPLOAD_PATH."website/".$this->website_m->CMS('home')->surf_board_detail_banner);?>');">
    <div class="contain">
        <div class="content">
            <h1>Detalhes da Prancha</h1>
            <ul>
                <li><a href="<?=base_url();?>">Casa</a></li>
                <li><a href="<?=base_url('browse-surfboards');?>">Detalhes da Prancha</a></li>
                <li>Detalhes da Prancha</li>
            </ul>
        </div>
    </div>
</section>
<!-- sBanner -->


<section id="proDetail">
    <div class="contain">
				<? if($this->website_m->checkAccounMissing()): ?>
					<div class="alert alert-info">
						<strong>warning.</strong> Por favor preencha o perfil para processar com a reserva. <a href="<?=base_url("account")?>" target="_blank">Clique aqui</a>
					</div>
				<? endif;?>
				<? if($this->session->flashdata("error_order")):?>
					<div class="alert alert-danger">
						<strong>Aviso </strong><?=$this->session->flashdata("error_order")?>
					</div>
				<? endif; ?>
				<!--Show Message Success/error-->
				<? if($this->session->flashdata('message_success')): ?>
					<div class="alert alert-success">
						<strong>Sucesso.</strong> <?=$this->session->flashdata('message_success');?>
					</div>

				<? endif; ?>
				
				<? if($this->session->flashdata('message_error')): ?>
					<div class="alert alert-danger">
						<strong>Erro.</strong> <?=$this->session->flashdata('message_error');?>
					</div>

				<? endif; ?>
        <div class="flexRow flex">
		
				<!--End Show Message Success/error-->
            <div class="col col1">
                <div class="miniSlider">
                    <ul id="lightSlider">
                        
                        <? foreach($webImages->result() as $images): ?>
							<li data-src="<?=base_url(UPLOAD_PATH.$images->sui_images);?>" data-thumb="<?=base_url(UPLOAD_PATH.$images->sui_images);?>">
                            <img src="<?=base_url(UPLOAD_PATH.$images->sui_images);?>" alt="">
							</li>
						<? endforeach; ?>
						</ul>
                </div>
            </div>
            <div class="col col2">
                <div class="content ckEditor">
                    <h2><?=$board->sur_title;?></h2>
					
                    <hr>
                    <div class="location semi">Localização: <em class="color regular"><?=$board->lcc_title;?></em></div>
                    <hr>
                    <!--<h5>Condição: <em class="color"><?=$board->sur_condition;?></em></h5>-->
                    <p><?=$board->sur_description;?></p>
                    <ul>
						<li><strong>Tipo:</strong> <?=$board->sur_type;?></li>						
						<li><strong>Marca:</strong> <?=$board->sur_brand;?></li>
						<li><strong>Volume:</strong> <?=$board->sur_volume;?> L</li>
						<li><strong>Tamanho:</strong> <?=$board->sur_size;?></li>
					<? foreach($properties->result() as $details): ?>
                        <li><strong><?=$details->sbp_property?>:</strong> <?=$details->sbp_value;?></li>
                    <? endforeach; ?>
                    </ul>
                    <hr>
                </div>
                <?php if($inmanutencao==1){ ?>
                            <span class="webBtn lgBtn btn-default">Reservado</span> 

                <?php }else{ ?>

                <form action="<?=base_url("reservation/reserveNow");?>" method="post">
                    <ul class="btnLst">
                        <!-- <li><h6>Qty:</h6></li> -->
                        <li>
                            <input type="text" readonly name="selectedDate" id="" class="txtBox datepicker" placeholder="Escolha o período">
                        </li>
                        <li>
                            <button type="submit" class="webBtn lgBtn colorBtn"><?=$buttons->Reserve_now_button_detail?></button>
                        </li>
                    </ul>
                </form>
                <?php }?>

            </div>
        </div>
    </div>
</section>
<!-- proDetail 
<section id="surfboards">
    <div class="contain text-center">
        <h1 class="secHeading">Surfboards</h1>    
        </div>
        <div class="bTn"><a href="<?=base_url();?>browse-surfboards" class="webBtn lgBtn colorBtn">See All</a></div>
    </div>
</section>

-->


<section id="similar">
    <div class="contain text-center">
        <h1 class="secHeading">Pranchas semelhantes</h1>
        <div id="owl-surfs" class="owl-carousel owl-theme">
           
           <? foreach($surfboards->result() as $board ): ?>
		   <div class="inner">
                <div class="iTm">
                    <div class="image">
                        <a href="<?=base_url();?>product-detail/<?=$board->sur_id?>/<?=str_replace(" ","-",$board->sur_title);?>"><img src="<?=base_url().UPLOAD_PATH.@$this->website_m->get_surfboard_image(['sur_id'=>$board->sur_id],1)->row()->sui_images;?>" alt=""></a>
                    </div>
                    <div class="txt">
                        <h4><a href="<?=base_url();?>product-detail/<?=$board->sur_id?>/<?=str_replace(" ","-",$board->sur_title);?>"><?=$board->sur_title; ?></a></h4>
                        <div class="specs"><?=$board->sur_size; ?></div>
                        <div class="ltr"><?=$board->sur_volume; ?></div>
                    </div>
                </div>
            </div>
			<? endforeach; ?>
        </div>
    </div>
</section>
<!-- similar -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>