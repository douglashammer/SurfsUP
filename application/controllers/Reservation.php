<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation extends CI_Controller {
	public $data;
	public $planId;
	public $memberId;
	public $surfboardId;
	public $trx_id;
	public $today;
	public $pkg_type;
  public $api;
	
	public function __construct(){		
		parent::__construct();
		if(!$this->website_m->is_login('header')){
			$this->session->set_userdata("postData",$this->input->post());
		}
		$this->website_m->is_login();		
		$this->memberId = $this->website_m->get_member_id();
		
		if($this->session->has_userdata("plan")){
			$this->planId = $this->session->userdata("plan")->pkg_id;
			$this->trx_id = $this->session->userdata("plan")->trx_id;
			$this->pkg_type = @$this->session->userdata("plan")->pkg_type;
		}else{
			$this->session->set_userdata("postData",$this->input->post());
			$this->session->set_flashdata('select_plan','Você ainda não tem nenhum plano ativo. Contrate um de nossos planos para prosseguir com a reserva de pranchas.');
			redirect("plans");
		}
		$this->today = strtotime(date("Y-m-d"));
		$this->data['website_pp'] = $this->website_m->CMS('pp');
    $this->api = $this->db->get("api")->row();
		
	}
	private function presentView(){		
		//present
		$this->db->select("* , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("surfboards as s");
		$this->db->join("location as l","s.sur_location = l.lcc_id");
		$this->db->join("location_category as lc","lc.loc_id = l.loc_id");
		$this->db->join("order as o","s.sur_id=o.sur_id");
		$this->db->where("o.mem_id",$this->memberId);
		$this->db->where("o.ord_surfboard_date ",strtotime(date("Y-m-d")));
		$this->db->order_by('o.ord_surfboard_date','ASC');
		$this->db->group_by('o.ord_number');
		//var_dump(strtotime(date("Y-m-d")));
		return $this->db->get();
	}
	private function pastView(){		
		$this->db->select("* , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("surfboards as s");
		$this->db->join("location as l","s.sur_location = l.lcc_id");
		$this->db->join("location_category as lc","lc.loc_id = l.loc_id");
		$this->db->join("order as o","s.sur_id=o.sur_id");
		$this->db->where("o.mem_id",$this->memberId);
		$this->db->where("o.ord_end <",$this->today);
		$this->db->order_by('o.ord_surfboard_date','ASC');
		$this->db->group_by('o.ord_number');
		return $this->db->get();
	}
	private function futureView(){
		$this->db->select("* , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("surfboards as s");
		$this->db->join("location as l","s.sur_location = l.lcc_id");
		$this->db->join("location_category as lc","lc.loc_id = l.loc_id");
		$this->db->join("order as o","s.sur_id=o.sur_id");
		$this->db->where("o.mem_id",$this->memberId);
		$this->db->where("o.ord_start > ",$this->today);
		$this->db->order_by('o.ord_surfboard_date','ASC');
		$this->db->group_by('o.ord_number');		
		$data = $this->db->get();
		return $data;
	}
	
	public function index()
	{
		if($this->session->has_userdata("plan")){
			$data3 = $this->db->get_where("packages",["pkg_id"=>$this->planId])->row();
			$this->data['pkg_type'] = $data3->pkg_type;
		}
		if($this->input->post('confirmNow')){
			if($this->session->has_userdata('reserve')){
				$data = $this->session->userdata('reserve');
				foreach($data as $arrayReserva =>$value){
          $str_ord_surfboard_date .= ','.$value['ord_surfboard_date'];
        }
        $str_ord_surfboard_date = ( $str_ord_surfboard_date != '' ? substr( $str_ord_surfboard_date, 1) : "");
				unset($_SESSION['reserve']);
				if($this->website_m->OtherCredits()>0){
					$this->website_m->OtherCredits($data); //Update Status
				}
				if($this->session->has_userdata("previousDate")){
				
				  $this->db->select("*");
          $this->db->where("mem_id",$this->memberId);
          $this->db->where("ord_status != 'canceled'");
          $this->db->where(" ord_surfboard_date in (".$str_ord_surfboard_date.")");


          $return_array = $this->db->get('order');
          $return =$return_array->result_array();
          foreach ($return as $key=>$value){
            $this->cancel($value['ord_number'],true);
          }
				}
				
				$this->db->insert_batch('order', $data);
				$this->session->set_flashdata('success_order','Sua reserva foi feita com sucesso.');
				redirect("reservation");
			}
		}
		$this->data['row'] = $this->presentView();//get it
		$this->data['past'] = $this->pastView(); //get it past
		$this->data['future'] = $this->futureView(); //get it future
		///////////////Set COLOR////////////////////
		$this->data['order'] = [
			'pending'=>'yellow',
			'canceled'=>'red',
			'completed'=>'green',
			'issued'=>'blue'
		];
		$this->data['lang'] = [
			'pending'=>'pendente',
			'canceled'=>'cancelado',
			'completed'=>'finalizado',
			'issued'=>'emitido'
		];
		
		////////////////////////////
		if(empty($this->presentView())){
			$this->data['message'] = 'Não há reserva hoje';
		}
		$this->data['page'] = 'reservations';
		$this->load->view('website/reservation',$this->data);

	}
	public function cancel($id,$option=false){
		$package = $this->db->where('pkg_id',$this->planId)->get("packages")->row();
		
		$today = (int)time();
		//die($t);
		
		$order = 
				$this->db->where('mem_id',$this->memberId)
						 ->where('pkg_id',$this->planId)
						 ->where('trx_id',$this->trx_id)
						 ->where('ord_number',$id)
						 ->get("order") 
						 ->result_array();
			
		$start_date = $order[0]['ord_start'];
		$end_date = $order[0]['ord_end'];
		
		$creditComsused = ($end_date-$today)/60/60;
		
		if($start_date<=$today OR $today<=$end_date){ //start or end date should be in future
			
			$differenceHours = (($start_date-$today)/60/60);
			
			if($differenceHours>=24 ){
				$this->db->where('ord_number',$id);
				if($this->db->delete("order")){
					if(!$option){
						$this->session->set_flashdata('success_order','Seu pedido foi cancelado com sucesso e todos os seus créditos são resgatados');
						$this->data['order'] = [
							'pending'=>'yellow',
							'canceled'=>'red',
							'completed'=>'green',
							'issued'=>'blue'
						];
						$this->data['lang'] = [
							'pending'=>'pendente',
							'canceled'=>'cancelado',
							'completed'=>'finalizado',
							'issued'=>'emitido'
						];
						redirect("reservation#Future");
					}
				}
			}elseif($differenceHours<=24){
				$creditComsused = ceil(($end_date-$today)/60/60/24);
				$totalCredits = count($order);
				
				$left = 0;//$totalCredits-$creditComsused;
				
				
				/////disable other orders/////
				$this->db->set('ord_status','canceled');
				$this->db->where('ord_number',$id);
				if($this->db->update("order")){
					if(!$option){
						$this->session->set_flashdata('success_order','Sua solicitação foi concluída com sucesso.');
					}
				}
				if($left>0){
					$this->db->where('ord_number',$id);
					$this->db->limit(1);
					$this->db->delete("order");
				}
				if(!$option){
					$this->data['row'] = $this->presentView();//get it
					$this->data['past'] = $this->pastView(); //get it past
					$this->data['future'] = $this->futureView(); //get it future
			
					redirect("reservation#Present");
				}
			}
			
			
		}else{
			if(!$option){
				$this->session->set_flashdata('error_order','Sua data de término do pedido deve estar no futuro para cancelar.');
				redirect("reservation#Past");
			}
		}
		if($option){
			return true;
		}
						
		/*	}
			
		}
		$this->session->set_flashdata('error_order','Seu pedido não pode ser cancelado. ');
		redirect("reservation#Future");
		*/
	}
	public function performCalculation(){
		
		
		
		$return = [];
		$this->db->order_by("trx_id","DESC");
		$planId = $this->db->get_where("transaction",[
			"mem_id"=>$this->website_m->get_member_id(),
			"trx_status"=>"active"
		])->result_array();
		
		
		$return['pkg_id'] = $planId[0]['pkg_id'];
		$return['mem_id'] = $this->website_m->get_member_id();
		
		
		$pkg3 = array();
		$sn=0;
		foreach($planId as $pp){
			$this->db->where('pkg_id',$pp['pkg_id']);
			$pkg3[] = json_decode(json_encode($this->db->get("packages")->row()),true);
			$sn++;
		}
		
		
		$package = $pkg3;
		
    $this->db->where('mem_id',$this->website_m->get_member_id());
		foreach($planId as $pp){
			$this->db->or_where('pkg_id',$pp['pkg_id']);
			$this->db->or_where('trx_id',$pp['trx_id']);
		}
		$order= $this->db->get("order")->result_array();
		
		$order2 = 
				$this->db->where('mem_id',$this->website_m->get_member_id())
						 ->where('trx_id',$this->trx_id)
						 ->where('ord_status','issued')
						 ->get("order") 
						 ->result_array();
		
		$order_dates = array_column($order,"ord_surfboard_date","ord_id");
		$order_dates2 = array_column($order2,"ord_surfboard_date","ord_id");
		
		$alreadyBooked = array_count_values($order_dates);
		$currentDate = strtotime(date("Y-m-d"));
		
		$book_per_day = array_sum(array_column($package,'pkg_booking_per_day'));
		$isTodayExists = in_array($currentDate,$order_dates);
		
		
		$return['allow_per_day'] = $book_per_day;
		$return['alreadyBooked'] = $alreadyBooked;
		$return['type'] = $package[0]['pkg_type'];
		$return['credits'] = $this->website_m->checkCreditsLeft();
		$return['used'] = array_sum($alreadyBooked);
		
		
		$ordertime = $planId[0];
		
		
    if($ordertime['trx_is_renewed'] == 1){
			$return['expired'] = 'no';
		}else{
			$return['expired'] = 'yes';
		}
		
    $return['previousDates'] = $order_dates;
		$return['issuedifany'] = $order_dates2;
		
    return $return;
	
  }
	
	public function cancelPlanNow(){
		if(!$this->website_m->checkPlanExists()){
			
			$this->session->set_flashdata('select_plan','Por favor, selecione seu plano primeiro.');
			redirect("packages");
		}
		
	
    //Cancelamento na API do pagar.me
    $this->db->select('trx_refer_id');
    $this->db->where(["mem_id" => $this->memberId, "trx_status"=> 'active']);
    $query = $this->db->get('tbl_transaction');
    
    $subscriptionToCancel  = $query->result();
    
    $cancelPayment = new pagar($this->api->api_public);
    $cancel = $cancelPayment->cancelSubscription($subscriptionToCancel[0]->trx_refer_id);

    if($cancel["error"]=="no"){
      //Cancelamento na API do pagar.me
      
      $this->db->set("trx_status","inactive");
		  $this->db->where("mem_id",$this->memberId);
		  $this->db->update("transaction");
      
      $this->session->set_flashdata('success_order','Seu plano foi cancelado com sucesso.');
		  redirect("packages");
    }else{
      $this->session->set_flashdata('message_error',$charge["message"]);
      redirect("packages");
    }
	}

  public function dateEmMysql($dateSql){
    $ano = substr($dateSql, 6, 4);
    $mes= substr($dateSql, 3, 2);
    $dia= substr($dateSql, 0, 2);
    return $ano."-".$mes."-".$dia;
}
	
	public function reserveNow(){
		
    
		if(!$this->website_m->checkPlanExists()){	
			$this->session->set_userdata("postData",$this->input->post());
			$this->session->set_flashdata('select_plan','Você ainda não tem nenhum plano ativo. Contrate um de nossos planos para prosseguir com a reserva de pranchas.');
			redirect("plans");
		}
		if($this->input->post()){
			$this->session->set_userdata("postData",$this->input->post());
		}
		
    
    
		if(!$this->session->has_userdata("product_id")){
			redirect("browse-surfboards");
		}
		
		$surboardLink =  $this->session->userdata("product_id");
		$this->surfboardId = $surboardLink->sur_id;
		unset($_SESSION['product_id']);
		
		
    $datasReserva = explode('-', $this->input->post("selectedDate"));
    
    $data_inicial = $this->dateEmMysql(trim($datasReserva[0]));
    $data_final = $this->dateEmMysql(trim($datasReserva[1]));
    
    $diferenca = strtotime($data_final) - strtotime($data_inicial);
    //Calcula a diferença em dias
    $dias = floor($diferenca / (60 * 60 * 24));
    // busca o tipo do plano
    $planPkg = $_SESSION['plan']->pkg_id;
    $checkPlan = $this->db->get_where('tbl_packages', array('pkg_id' => $planPkg))->row();

    if($dias > 6 && $checkPlan->pkg_type == 'SUBSCRIPTIONS'){
      $this->session->set_flashdata('error_order','Não é possível fazer a reserva por mais de 7 dias.');
			redirect("product-detail/{$surboardLink->sur_id}/".str_replace(" ","-",$surboardLink->sur_title));  
    }

    unset($_SESSION['planid']);

		if($this->session->has_userdata("postData")){
			$da = $this->session->userdata("postData");
			$dates = $da['selectedDate'];
      unset($_SESSION['postData']);
		}else{
			$dates = $this->input->post("selectedDate");
		}
		if($dates){
			$start_end = explode("-",$dates);
			list($startDate,$endDate) = $start_end;
			$startDate = $this->dateReverse($startDate,'/');
			$endDate = $this->dateReverse($endDate,'/');

      if($startDate<$endDate){
				for($j=$startDate;$j<=$endDate;$j+=(1*24*60*60)){ //days*hours*minutes*seconds = days in seconds
					$mydate[] = $j;
				}
			}else{
				$mydate = [$endDate];
			}
			
			if($previousData = $this->performCalculation($mydate)){
				
				$today = strtotime(date("Y-m-d"));
				
				$disabledDates = $this->db
									->where("sur_id",$this->surfboardId)
									->where(" ord_status in ( 'pending', 'issued') ")
									->where("ord_surfboard_date >= ",$today)
									->get("order")->result_array();
				$mydatez = array_column($disabledDates,"ord_surfboard_date");
				$dateDisabled = $mydatez;
				$selectedDisabled = [];
				foreach($mydate as $dt){
					if(!empty($dateDisabled)){
						if(is_array($dateDisabled)){
							if(in_array($dt,$dateDisabled)){
								$selectedDisabled[] = date("d-M-Y",$dt);
							}
						}
					}
				}
				
				if(!empty($selectedDisabled)){
					$this->session->set_flashdata('error_order','Prancha de surf já reservada para essas datas '.implode(",",$selectedDisabled));
					redirect("product-detail/{$surboardLink->sur_id}/".str_replace(" ","-",$surboardLink->sur_title));
				}
			
      	if($previousData['type']=='SUBSCRIPTIONS'){
					
					if($previousData['expired']=='yes'){
						$this->session->set_flashdata('error_order','Assinatura expirada.');
						redirect("browse-surfboards");						
					}
					if($previousData['allow_per_day']==0){
							$booking = true;
					}else{
						foreach($mydate as $dt){
							
							foreach($previousData['alreadyBooked'] as $booking_date=>$no_of_booking){
								if($booking_date==$dt){
									if($no_of_booking==$previousData['allow_per_day']){
										$this->session->set_flashdata('error_order','Sua reserva não está disponível para '.date("M,d Y",$dt));
										redirect("browse-surfboards");
									}
								}
							}
						}
					}	
				}else{ //package
				
					$previousData2 = $this->website_m->performCalculation();
					if($previousData2['credits']== $previousData2['used']){
						$this->session->set_userdata("postData",$this->input->post());
						$this->session->set_userdata("product_id",$surboardLink );
						$this->session->set_flashdata('error_order','Sua reserva excede os seus créditos restantes. Por favor contrate um plano novo para seguir com a reserva.');
						redirect("plans");
					}else{
						if(count($mydate)<=($previousData2['credits']-$previousData2['used'])){
							goto A;
						}else{
							$this->session->set_userdata("postData",$this->input->post());
							$this->session->set_userdata("product_id",$surboardLink );
							$this->session->set_flashdata('error_order','Você consumiu todos os seus créditos ou os dias selecionados são maiores que seus créditos');
							redirect("packages");
						}
					}
				}
			}
			A:
			$randOrdId = rand(111,999).date("ymdhi").$this->memberId;
			$session = array();
			if(is_array($mydate)){
				$ord_start = $mydate[0];
				$ord_end = end($mydate);
				foreach($mydate as $date){
					$session[] = [
						"sur_id"=>$this->surfboardId,
						"mem_id"=>$this->memberId,
						"pkg_id"=>$this->planId,
						"trx_id"=>$this->trx_id,
						"ord_type"=>$previousData['type'],
						"ord_number"=>$randOrdId,
						"ord_surfboard_date"=>$date,
						"ord_time"=>time(),
						"ord_start"=>$ord_start,
						"ord_end" =>$ord_end
					];
				}
			}
			
      /**Here we go with the popup**/
			$this->data['row'] = $this->presentView();//get it
			$this->data['past'] = $this->pastView(); //get it past
			$this->data['future'] = $this->futureView(); //get it future		
			if(empty($this->presentView())){
				$this->data['message'] = 'Não há reserva hoje';
			}
			
			$chkCancel = $sameDates = [];
			foreach($previousData['previousDates'] as $key=>$sd){
				if(in_array($sd,$mydate)){
					$sameDates[$key] = $sd;
					$dtaChk = $this->db->get_where("order",["ord_id"=>$key,"ord_status"=>"pending"])->row();
					if(!empty($dtaChk)){
						$chkCancel[] =	$dtaChk->ord_status;
					}
				}
			}
			//die(print_r($sameDates));
			if(count($sameDates)>0 AND count($chkCancel)>0){
				
				$this->session->set_userdata("previousDate",$previousData['previousDates']);
				$this->data['warning'] = 'Você já tem uma reserva agendada para a data selecionada. Se você prosseguir com esta reserva, a reserva agendada para a data coincidente será automaticamente cancelada';
			}
			$IssuedSameDates = array_intersect($previousData['issuedifany'],$mydate);
			if(count($IssuedSameDates)>0){
				$this->data['error_issue'] = 'Você já tem uma prancha em uso na data selecionada. Para prosseguir com esta reserva, você deve devolver a prancha em uso no seu estabelecimento de origem e depois realizar uma nova reserva.';
			}
			$this->data['selectedDates'] = $mydate;
			//get this board
			$this->data['show'] = $this->website_m->get_surfboard(['sur_id'=>$this->surfboardId])->row();
			$this->data['properties'] = $this->website_m->get_sur_properties($this->surfboardId);
			
			$this->session->set_userdata("reserve",$session);
			$this->data['popup'] = true;
			$this->data['page'] = 'reservations';
			
			$this->load->view('website/reservation',$this->data);
			
		}else{
			$this->session->set_flashdata('error_order','Algo errado ao fazer seu pedido.');
			redirect("browse-surfboards");
		}
	}
	public static function dateReverse($date,$seprater="-"){

		$exp = explode($seprater,$date);
		$exp2 = array_map('trim',$exp);
		list($day,$month,$year) = $exp2;
		return strtotime("{$year}-{$month}-{$day}"); //date in unix timstamp format
	}

}
