
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Location</a>
		</li>
		<li class="active">
			<strong>Add/Edit Category</strong>
		</li>
	</ol>
	
	<h3>Add/Edit Coupon</h3>
	<form role="form" action="" method="post" class="form-horizontal form-groups-bordered" enctype="multipart/form-data">

			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Title</label>
				
				<div class="col-sm-5">
					<input type="text" name="cop_title" value="<?=@$row->cop_title;?>" class="form-control" id="field-1" placeholder="50% Discount Coupon">
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Coupon Code</label>
				
				<div class="col-sm-5">
					<input type="text" name="cop_code" value="<?=@$row->cop_code;?>" class="form-control" id="field-1" placeholder="SUR50">
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Discount Percentage</label>
				
				<div class="col-sm-5">
					<input type="number" min="0" max="100" step="0.1" name="cop_discount" value="<?=@$row->cop_discount;?>" class="form-control" id="field-1" placeholder="eg: 50%">
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Coupun apply to</label>
				
				

					<div class="col-sm-5">
						<select name="pkg_id" required class="form-control">
							<option value="">--Select--</option>
							<? foreach($couponType as $key=>$pkg): ?>
							<option <?=@$row->pkg_id==$key?'selected':'';?> value="<?=$key;?>"><?=$pkg;?></option>	
							<? endforeach; ?>
							
						<!--
						<? foreach($packges->result() as $pkg): ?>
							<option <?=@$row->pkg_id==@$pkg->pkg_id?'selected':'';?> value="<?=$pkg->pkg_id;?>"><?=$pkg->pkg_type;?>-><?=$pkg->pkg_title;?></option>
						<? endforeach; ?>
						--->
						</select>
					</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" name="cop_status" <?=(@$row->cop_status=='active')?'checked':'';?> />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-8">
					<input type="submit"  class="btn btn-success btn-lg" value="Save">
				</div>
			</div>
	</form>
