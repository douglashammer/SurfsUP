<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {
	public $data;
	public function __construct(){		
		parent::__construct();
		if($this->website_m->is_login('header')){
			redirect("packages");
		}
	}
	public function index()
	{
		$this->form_validation->set_rules($this->website_m->formValidation("signin"));
		if($this->form_validation->run()){
					
			if($data = $this->input->post()){
				if($this->website_m->do_login($data)){
					if($this->website_m->is_login('header')){
						setcookie("first","done",time()+12*60*60);
					}
					if($this->session->has_userdata("postData")){
						redirect("reservation/reserveNow");
					}
					if($this->website_m->checkAccounMissing()){
						
							redirect("account");
						
					}
					redirect("browse-surfboards");
				}
			}
		}
		$this->data['page'] = 'signin';
		$this->load->view('website/signin',$this->data);
	}
	public function signout(){
		unset($_SESSION['mem_login']);
		redirect(base_url());
	}
	public function facebook_login() {

	   include_once APPPATH . "libraries/Facebook/autoload.php";

		$fb = new Facebook\Facebook(array(
			'app_id' => FB_APP_ID, // Replace {app-id} with your app id
			'app_secret' => FB_SECRET,
			'default_graph_version' => 'v2.9'
		));

            $helper = $fb->getRedirectLoginHelper();
        $permissions = array('email'); // Optional permissions
        $loginUrl = $helper->getLoginUrl(base_url('ajax/fb_callback'), $permissions);
        $fb_login_url = ($loginUrl);
        redirect($fb_login_url, 'refresh');
        exit;
    }

    public function google_login() {

        include_once APPPATH . "libraries/Google/autoload.php";

        $client_id = GOOGLE_CLIENT_ID;
        $client_secret = GOOGLE_SECRET;
        $redirect_uri = base_url('ajax/google_callback');

        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");
        $authUrl = $client->createAuthUrl();

        redirect(urldecode($authUrl), 'refresh');
    }

    
}