
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Location</a>
		</li>
		<li class="active">
			<strong>Add/Edit Location</strong>
		</li>
	</ol>
	
	<h3>Add/Edit Location</h3>
	<form role="form" action="" method="post" class="form-horizontal form-groups-bordered">
			
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Title</label>
				
				<div class="col-sm-5">
					<input type="text" name="lcc_title" value="<?=@$row->lcc_title;?>" class="form-control" id="field-1" placeholder="Title">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Select Category</label>
				
				<div class="col-sm-5">
					<select name="loc_id" required class="form-control">
						<option value="">--Select--</option>
					<? foreach($get_category->result() as $location): ?>
						<option <?=$location->loc_id==@$row->loc_id?'selected':'';?> value="<?=$location->loc_id;?>"><?=$location->loc_title;?></option>
					<? endforeach; ?>
					</select>
				</div>
			</div>
			<!--
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Opening Time</label>
				
				<div class="col-sm-5">
					<input type="time" name="lcc_start_time" value="<?=@$row->lcc_start_time;?>" class="form-control" id="field-1" >
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Closing Time</label>
				
				<div class="col-sm-5">
					<input type="time" name="lcc_end_time" value="<?=@$row->lcc_end_time;?>" class="form-control" id="field-1">
				</div>
			</div>
			-->
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Location Description</label>
				
				<div class="col-sm-5">
					<textarea  name="lcc_description" class="form-control" id="field-1" ><?=@$row->lcc_description;?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" name="lcc_status" <?=(@$row->lcc_status=='active')?'checked':'';?> />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-8">
					<input type="submit"  class="btn btn-success btn-lg" value="Save">
				</div>
			</div>
	</form>
