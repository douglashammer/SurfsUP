
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="#"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Members</a>
		</li>
		<li class="active">
			<strong><?=$heading;?></strong>
		</li>
	</ol>
	
	<h3>View <?=$heading;?></h3>
		<!--
		<div class="text-right">
			<a href="<?=base_url(ADMIN.'/surfboards/add');?>" class="btn btn-info btn-lg btn-icon icon-left "> <i class="entypo-plus"></i> Add Surfboard</a>
		</div>
		-->
		<br><br>
		<script type="text/javascript">
					$(function() {
				var $table4 = jQuery( "#table-4" );
				$table4.DataTable({	
					dom: 'Bfrtip',
					buttons:
					[{
						extend: 'excel',
						text: 'Exportar para Excel',
						exportOptions: {
							//columns: 'th:not(:last-child)'
						}
					}],
					rowReorder: {
						selector: '.myreorder'
					},
					reorder:true,

					columnDefs: 
					[
						{ orderable: true, className: 'reorder', targets: 0 },
						{ orderable: true, className: 'reorder', targets: 1 },
						{ orderable: true, className: 'reorder', targets: 2 },
						{ orderable: true, className: 'reorder', targets: 3 },
						{ orderable: true, className: 'reorder', targets: 4 },
						{ orderable: true, className: 'reorder', targets: 5 },
						{ orderable: true, className: 'reorder', targets: 6 },
						{ orderable: true, className: 'reorder', targets: 7 },
						{ orderable: false, targets: '_all' }
					],
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": true,
					"bInfo": false,
					"bAutoWidth": false,
					"language": {
							"sEmptyTable":   	"Nenhum dado na tabela",
							"sInfo":         	"_START_ bis _END_ von _TOTAL_ Einträgen",
							"sInfoEmpty":    	"0 bis 0 von 0 Einträgen",
							"sInfoFiltered": 	"(gefiltert von _MAX_ Einträgen)",
							"sInfoPostFix":  	"",
							"sInfoThousands":  	".",
							"sLengthMenu":   	"_MENU_ Einträge anzeigen",
							"sLoadingRecords": 	"Carregando...",
							"sProcessing":   	"Aguarde...",
							"sSearch":       	"Pesquisar",
							"sZeroRecords":  	"Nenhuma entrada disponível.",
							"oPaginate": {
								"sFirst":    	"Primeiro",
								"sPrevious": 	"Voltar",
								"sNext":     	"Próximo",
								"sLast":     	"Último"
							},
							"oAria": {
								"sSortAscending":  ": Ativar a ordfenação da coluna crescente",
								"sSortDescending": ": /ativar a ordenação da coluna decrescente"
						}
					}
					});
			});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>SN</th>
					<th>Nome completo</th>
					<th>Email</th>					
					<th>Telefone</th>
					<th>Quant. Paga</th>
					<th>Pacote/Título da Assinatura</th>
					<th>Tipo de Pacote</th>
					<th>Duração do Pacote</th>
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $col): ?>
				<tr>
					<td><?=$sn++;?></td>
					<td><?=$col->mem_first;?> <?=$col->mem_last;?></td>
					<td><?=$col->mem_email;?></td>
					<td><?=$col->mem_phone;?> <?=$col->c_code;?></td>
					<td>$<?=$col->trx_amount;?></td>
					<td><?=$col->pkg_title;?></td>
					<td><?=$col->pkg_type;?></td>					
					<td><?=$col->pkg_time;?> <?=$col->pkg_duration;?></td>					
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		
