
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Location</a>
		</li>
		<li class="active">
			<strong>Add/Edit Category</strong>
		</li>
	</ol>
	
	<h3>Add/Edit Surfboars</h3>
	<form role="form" action="" method="post" class="form-horizontal form-groups-bordered" enctype="multipart/form-data">
			
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Title</label>
				
				<div class="col-sm-5">
					<input type="text" name="loc_title" value="<?=@$row->loc_title;?>" class="form-control" id="field-1" placeholder="Title">
				</div>
			</div>
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Image <span class="symbol required"></span></label>
					<? if(isset($row->loc_image)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->loc_image;?>" width="150">
					<? endif; ?>
                     <input type="file" name="image"  class="form-control" >
                </div>
            </div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" name="loc_status" <?=(@$row->loc_status=='active')?'checked':'';?> />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-8">
					<input type="submit"  class="btn btn-success btn-lg" value="Save">
				</div>
			</div>
	</form>
