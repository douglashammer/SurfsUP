<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
<div class="row col-md-12">
    
        <div class="col-md-12">
            <h3><i class="fa fa-bars"></i>Main Heading</h3>
            <hr class="hr-short">
           

                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label"> Main Heading <span class="symbol required"></span></label>
                        <input type="text" name="top_main_heading" value="<?=@$row->top_main_heading?>" class="form-control" required></div>
                    </div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Main Sub Heading  <span class="symbol required"></span></label>
						<textarea name="top_main_sub_heading" class="form-control" required><?=@$row->top_main_sub_heading;?></textarea>
						
					</div>
				</div>
			
			
			
               
                    <div class="clearfix"></div>
                    <div class="col-md-12"><hr class="hr-short">
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-green btn-lg" value="Update Settings">
                            </div>
                        </div>
                    </div>
                    <br><br>
                </form>
            </div>            <p>&nbsp;</p>
            <div class="clearfix"></div>
