<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends CI_Controller {
	public $data;
	public $memberId;
	public function __construct(){		
		parent::__construct();
		$this->website_m->is_login();
		$this->memberId = $this->website_m->get_member_id();
	}
	public function index()
	{
		//$this->db->order_by("pkg_id","DESC");
		$this->db->order_by("trx_id","DESC");
		$this->data['selected'] = $this->db->where([
											'mem_id'=>$this->memberId,
											'trx_status'=>'active'
												])->get("transaction")->row();
		
		$this->data['packages'] = $this->website_m->get_packages(['pkg_type'=>'PACKAGES','pkg_status'=>'active']);
		$this->data['subscription'] = $this->website_m->get_packages(['pkg_type'=>'SUBSCRIPTIONS','pkg_status'=>'active']);
		$this->data['page'] = 'packages';
		$this->load->view('website/packages',$this->data);
	}

}