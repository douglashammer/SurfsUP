
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Location</a>
		</li>
		<li class="active">
			<strong>Add/Edit Location</strong>
		</li>
	</ol>
	
	<h3>Add/Edit Help Desk</h3>
	<form role="form" action="" method="post" enctype="multipart/form-data" class="form-horizontal form-groups-bordered">
			
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Question</label>

				<div class="col-sm-5">
					<textarea type="text" name="help_question" class="form-control" id="field-1" placeholder=""><?=@$row->help_question;?></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Answer</label>
				
				<div class="col-sm-5">
					<textarea type="text" name="help_answer" class="form-control" id="field-1" placeholder=""><?=@$row->help_answer;?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" name="help_status" <?=(@$row->help_status=='active')?'checked':'';?> />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-8">
					<center>
						<input type="submit"  class="btn btn-success btn-lg" value="Save">
					</center>
				</div>
			</div>
	</form>
