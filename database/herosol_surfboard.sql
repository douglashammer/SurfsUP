-- phpMyAdmin SQL Dump
-- version 
-- http://www.phpmyadmin.net
--
-- Host: mysql1003.mochahost.com
-- Generation Time: May 24, 2019 at 04:08 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.30

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `herosol_surfboard`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_api`
--

CREATE TABLE IF NOT EXISTS `tbl_api` (
  `api_public` text NOT NULL,
  `api_private` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_api`
--

INSERT INTO `tbl_api` (`api_public`, `api_private`) VALUES
('ek_test_h3wz0E5J1lpBA9kfzCE03TnDtmW6wC', 'ak_test_wMNQsOBOoIGAnKrbFudQ7nqrezmwWi');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `cnt_id` int(11) NOT NULL AUTO_INCREMENT,
  `cnt_name` varchar(100) NOT NULL,
  `cnt_phone` varchar(50) NOT NULL,
  `cnt_subject` varchar(200) NOT NULL,
  `cnt_email` varchar(100) NOT NULL,
  `cnt_message` text NOT NULL,
  PRIMARY KEY (`cnt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`cnt_id`, `cnt_name`, `cnt_phone`, `cnt_subject`, `cnt_email`, `cnt_message`) VALUES
(1, 'Muhammad Sarmad', '3458341704', 'ssa', 'sarmad711@gmail.com', 'dffadsfd'),
(2, 'Muhammad Sarmad', '3458341704', 'testing', 'sarmad711@gmail.com', 'testing saramd'),
(3, 'Daniel', '11999814191', 'Funciona?', 'danielcappellette@gmail.com', 'Estou mandando esta mensagem para ver se funciona');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE IF NOT EXISTS `tbl_countries` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_title` varchar(50) DEFAULT NULL,
  `c_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=248 ;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`c_id`, `c_title`, `c_code`) VALUES
(1, 'Afghanistan', 'AF'),
(2, 'Aland Islands', 'AX'),
(3, 'Albania', 'AL'),
(4, 'Algeria', 'DZ'),
(5, 'American Samoa', 'AS'),
(6, 'Andorra', 'AD'),
(7, 'Angola', 'AO'),
(8, 'Anguilla', 'AI'),
(9, 'Antarctica', 'AQ'),
(10, 'Antigua and Barbuda', 'AG'),
(11, 'Argentina', 'AR'),
(12, 'Armenia', 'AM'),
(13, 'Aruba', 'AW'),
(14, 'Australia', 'AU'),
(15, 'Austria', 'AT'),
(16, 'Azerbaijan', 'AZ'),
(17, 'Bahamas', 'BS'),
(18, 'Bahrain', 'BH'),
(19, 'Bangladesh', 'BD'),
(20, 'Barbados', 'BB'),
(21, 'Belarus', 'BY'),
(22, 'Belgium', 'BE'),
(23, 'Belize', 'BZ'),
(24, 'Benin', 'BJ'),
(25, 'Bermuda', 'BM'),
(26, 'Bhutan', 'BT'),
(27, 'Bolivia', 'BO'),
(28, 'Bosnia and Herzegovina', 'BA'),
(29, 'Botswana', 'BW'),
(30, 'Bouvet Island', 'BV'),
(31, 'Brazil', 'BR'),
(32, 'British Virgin Islands', 'VG'),
(33, 'British Indian Ocean Territory', 'IO'),
(34, 'Brunei Darussalam', 'BN'),
(35, 'Bulgaria', 'BG'),
(36, 'Burkina Faso', 'BF'),
(37, 'Burundi', 'BI'),
(38, 'Cambodia', 'KH'),
(39, 'Cameroon', 'CM'),
(40, 'Canada', 'CA'),
(41, 'Cape Verde', 'CV'),
(42, 'Cayman Islands', 'KY'),
(43, 'Central African Republic', 'CF'),
(44, 'Chad', 'TD'),
(45, 'Chile', 'CL'),
(46, 'China', 'CN'),
(47, 'Hong Kong, SAR China', 'HK'),
(48, 'Macao, SAR China', 'MO'),
(49, 'Christmas Island', 'CX'),
(50, 'Cocos (Keeling) Islands', 'CC'),
(51, 'Colombia', 'CO'),
(52, 'Comoros', 'KM'),
(53, 'Congo (Brazzaville)', 'CG'),
(54, 'Congo, (Kinshasa)', 'CD'),
(55, 'Cook Islands', 'CK'),
(56, 'Costa Rica', 'CR'),
(57, 'Côte d''Ivoire', 'CI'),
(58, 'Croatia', 'HR'),
(59, 'Cuba', 'CU'),
(60, 'Cyprus', 'CY'),
(61, 'Czech Republic', 'CZ'),
(62, 'Denmark', 'DK'),
(63, 'Djibouti', 'DJ'),
(64, 'Dominica', 'DM'),
(65, 'Dominican Republic', 'DO'),
(66, 'Ecuador', 'EC'),
(67, 'Egypt', 'EG'),
(68, 'El Salvador', 'SV'),
(69, 'Equatorial Guinea', 'GQ'),
(70, 'Eritrea', 'ER'),
(71, 'Estonia', 'EE'),
(72, 'Ethiopia', 'ET'),
(73, 'Falkland Islands (Malvinas)', 'FK'),
(74, 'Faroe Islands', 'FO'),
(75, 'Fiji', 'FJ'),
(76, 'Finland', 'FI'),
(77, 'France', 'FR'),
(78, 'French Guiana', 'GF'),
(79, 'French Polynesia', 'PF'),
(80, 'French Southern Territories', 'TF'),
(81, 'Gabon', 'GA'),
(82, 'Gambia', 'GM'),
(83, 'Georgia', 'GE'),
(84, 'Germany', 'DE'),
(85, 'Ghana', 'GH'),
(86, 'Gibraltar', 'GI'),
(87, 'Greece', 'GR'),
(88, 'Greenland', 'GL'),
(89, 'Grenada', 'GD'),
(90, 'Guadeloupe', 'GP'),
(91, 'Guam', 'GU'),
(92, 'Guatemala', 'GT'),
(93, 'Guernsey', 'GG'),
(94, 'Guinea', 'GN'),
(95, 'Guinea-Bissau', 'GW'),
(96, 'Guyana', 'GY'),
(97, 'Haiti', 'HT'),
(98, 'Heard and Mcdonald Islands', 'HM'),
(99, 'Holy See (Vatican City State)', 'VA'),
(100, 'Honduras', 'HN'),
(101, 'Hungary', 'HU'),
(102, 'Iceland', 'IS'),
(103, 'India', 'IN'),
(104, 'Indonesia', 'ID'),
(105, 'Iran, Islamic Republic of', 'IR'),
(106, 'Iraq', 'IQ'),
(107, 'Ireland', 'IE'),
(108, 'Isle of Man', 'IM'),
(109, 'Israel', 'IL'),
(110, 'Italy', 'IT'),
(111, 'Jamaica', 'JM'),
(112, 'Japan', 'JP'),
(113, 'Jersey', 'JE'),
(114, 'Jordan', 'JO'),
(115, 'Kazakhstan', 'KZ'),
(116, 'Kenya', 'KE'),
(117, 'Kiribati', 'KI'),
(118, 'Korea (North)', 'KP'),
(119, 'Korea (South)', 'KR'),
(120, 'Kuwait', 'KW'),
(121, 'Kyrgyzstan', 'KG'),
(122, 'Lao PDR', 'LA'),
(123, 'Latvia', 'LV'),
(124, 'Lebanon', 'LB'),
(125, 'Lesotho', 'LS'),
(126, 'Liberia', 'LR'),
(127, 'Libya', 'LY'),
(128, 'Liechtenstein', 'LI'),
(129, 'Lithuania', 'LT'),
(130, 'Luxembourg', 'LU'),
(131, 'Macedonia, Republic of', 'MK'),
(132, 'Madagascar', 'MG'),
(133, 'Malawi', 'MW'),
(134, 'Malaysia', 'MY'),
(135, 'Maldives', 'MV'),
(136, 'Mali', 'ML'),
(137, 'Malta', 'MT'),
(138, 'Marshall Islands', 'MH'),
(139, 'Martinique', 'MQ'),
(140, 'Mauritania', 'MR'),
(141, 'Mauritius', 'MU'),
(142, 'Mayotte', 'YT'),
(143, 'Mexico', 'MX'),
(144, 'Micronesia, Federated States of', 'FM'),
(145, 'Moldova', 'MD'),
(146, 'Monaco', 'MC'),
(147, 'Mongolia', 'MN'),
(148, 'Montenegro', 'ME'),
(149, 'Montserrat', 'MS'),
(150, 'Morocco', 'MA'),
(151, 'Mozambique', 'MZ'),
(152, 'Myanmar', 'MM'),
(153, 'Namibia', 'NA'),
(154, 'Nauru', 'NR'),
(155, 'Nepal', 'NP'),
(156, 'Netherlands', 'NL'),
(157, 'Netherlands Antilles', 'AN'),
(158, 'New Caledonia', 'NC'),
(159, 'New Zealand', 'NZ'),
(160, 'Nicaragua', 'NI'),
(161, 'Niger', 'NE'),
(162, 'Nigeria', 'NG'),
(163, 'Niue', 'NU'),
(164, 'Norfolk Island', 'NF'),
(165, 'Northern Mariana Islands', 'MP'),
(166, 'Norway', 'NO'),
(167, 'Oman', 'OM'),
(168, 'Pakistan', 'PK'),
(169, 'Palau', 'PW'),
(170, 'Palestinian Territory', 'PS'),
(171, 'Panama', 'PA'),
(172, 'Papua New Guinea', 'PG'),
(173, 'Paraguay', 'PY'),
(174, 'Peru', 'PE'),
(175, 'Philippines', 'PH'),
(176, 'Pitcairn', 'PN'),
(177, 'Poland', 'PL'),
(178, 'Portugal', 'PT'),
(179, 'Puerto Rico', 'PR'),
(180, 'Qatar', 'QA'),
(181, 'Réunion', 'RE'),
(182, 'Romania', 'RO'),
(183, 'Russian Federation', 'RU'),
(184, 'Rwanda', 'RW'),
(185, 'Saint-Barthélemy', 'BL'),
(186, 'Saint Helena', 'SH'),
(187, 'Saint Kitts and Nevis', 'KN'),
(188, 'Saint Lucia', 'LC'),
(189, 'Saint-Martin (French part)', 'MF'),
(190, 'Saint Pierre and Miquelon', 'PM'),
(191, 'Saint Vincent and Grenadines', 'VC'),
(192, 'Samoa', 'WS'),
(193, 'San Marino', 'SM'),
(194, 'Sao Tome and Principe', 'ST'),
(195, 'Saudi Arabia', 'SA'),
(196, 'Senegal', 'SN'),
(197, 'Serbia', 'RS'),
(198, 'Seychelles', 'SC'),
(199, 'Sierra Leone', 'SL'),
(200, 'Singapore', 'SG'),
(201, 'Slovakia', 'SK'),
(202, 'Slovenia', 'SI'),
(203, 'Solomon Islands', 'SB'),
(204, 'Somalia', 'SO'),
(205, 'South Africa', 'ZA'),
(206, 'South Georgia and the South Sandwich Islands', 'GS'),
(207, 'South Sudan', 'SS'),
(208, 'Spain', 'ES'),
(209, 'Sri Lanka', 'LK'),
(210, 'Sudan', 'SD'),
(211, 'Suriname', 'SR'),
(212, 'Svalbard and Jan Mayen Islands', 'SJ'),
(213, 'Swaziland', 'SZ'),
(214, 'Sweden', 'SE'),
(215, 'Switzerland', 'CH'),
(216, 'Syrian Arab Republic (Syria)', 'SY'),
(217, 'Taiwan, Republic of China', 'TW'),
(218, 'Tajikistan', 'TJ'),
(219, 'Tanzania, United Republic of', 'TZ'),
(220, 'Thailand', 'TH'),
(221, 'Timor-Leste', 'TL'),
(222, 'Togo', 'TG'),
(223, 'Tokelau', 'TK'),
(224, 'Tonga', 'TO'),
(225, 'Trinidad and Tobago', 'TT'),
(226, 'Tunisia', 'TN'),
(227, 'Turkey', 'TR'),
(228, 'Turkmenistan', 'TM'),
(229, 'Turks and Caicos Islands', 'TC'),
(230, 'Tuvalu', 'TV'),
(231, 'Uganda', 'UG'),
(232, 'Ukraine', 'UA'),
(233, 'United Arab Emirates', 'AE'),
(234, 'United Kingdom', 'GB'),
(235, 'United States of America', 'US'),
(236, 'US Minor Outlying Islands', 'UM'),
(237, 'Uruguay', 'UY'),
(238, 'Uzbekistan', 'UZ'),
(239, 'Vanuatu', 'VU'),
(240, 'Venezuela (Bolivarian Republic)', 'VE'),
(241, 'Viet Nam', 'VN'),
(242, 'Virgin Islands, US', 'VI'),
(243, 'Wallis and Futuna Islands', 'WF'),
(244, 'Western Sahara', 'EH'),
(245, 'Yemen', 'YE'),
(246, 'Zambia', 'ZM'),
(247, 'Zimbabwe', 'ZW');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coupon`
--

CREATE TABLE IF NOT EXISTS `tbl_coupon` (
  `cop_id` int(11) NOT NULL AUTO_INCREMENT,
  `cop_title` varchar(255) NOT NULL,
  `cop_code` varchar(255) NOT NULL,
  `cop_discount` float NOT NULL,
  `cop_status` varchar(20) NOT NULL,
  PRIMARY KEY (`cop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_coupon`
--

INSERT INTO `tbl_coupon` (`cop_id`, `cop_title`, `cop_code`, `cop_discount`, `cop_status`) VALUES
(2, '100% Discount', 'SUR100', 100, 'inactive'),
(3, '50%', 'surf50', 50, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_help`
--

CREATE TABLE IF NOT EXISTS `tbl_help` (
  `help_id` int(11) NOT NULL AUTO_INCREMENT,
  `help_question` text NOT NULL,
  `help_answer` text NOT NULL,
  `help_status` varchar(20) NOT NULL,
  PRIMARY KEY (`help_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tbl_help`
--

INSERT INTO `tbl_help` (`help_id`, `help_question`, `help_answer`, `help_status`) VALUES
(2, 'O Que é o Surf’s Up Club?', 'O Surf''s Up Club é o primeiro clube de assinatura de pranchas do Brasil. Através de parcerias com grandes shapers e estabelecimentos localizados nos principais picos de surfe, provemos liberdade para que o surfista tenha novas experiências no mar, surfando com diferentes modelos, tamanhos, marcas e tecnologias de pranchas de surfe por um preço único', 'active'),
(3, 'Como funciona?', 'O surfista, ao se cadastrar em nosso clube, tem acesso ilimitado a todas as pranchas do clube (ou seja, pode surfar com qualquer prancha disponível em qualquer unidade do Surf''s Up Club);  seguro incluso para danos ocorridos na prática do surfe; e possibilidade de trocas ilimitadas (pode surfar com uma prancha, devolver, reservar e surfar com qualquer outra prancha, quantas vezes quiser!).', 'active'),
(4, 'Como me associo ao clube?', 'Todo processo operacional do Surf''s Up funciona online, através da nossa plataforma digital.É só seguir o nosso passo a passo: 1- Faça seu cadastro no nosso site com suas informações pessoais ou login com facebook/google. 2- Escolha seu plano - Encontre e assine o plano que mais combina com você. Atualmente temos planos de assinatura mensal e anual e também temos pacotes avulsos de 1, 3, 5 e 10 dias. 3- Reserve sua prancha online através da nossa plataforma digiral, filtrando por localização ou filtros avançados 4- GO SURF NOW! Vá ao estabelecimento parceiro escolhido, retire sua prancha com seu id de reserva e vá surfar.', 'active'),
(5, 'Qual plano é o ideal para mim?', 'Todos nossos planos garantem acesso irrestrito a qualquer prancha do clube. Nossos associados podem desfrutar de todos os equipamentos com direito a trocas ilimitadas!Atualmente temos planos de assinatura mensal e anual, e também temos pacotes avulsos, de 1, 3, 5 e 10 dias. Os planos avulsos funcionam no modo pré-pago, em que o cliente compra uma quantidade de dias de uso do clube, e pode usar quando e como quiser, podendo ficar com a prancha até pelo limite de dias contratados. Já os planos de assinatura, funcionam no modelo de assinatura recorrente, podendo o cliente usar pranchas todos os dias, porém com o limite máximo até 7 dias consecutivos com a mesma prancha. Para todos os planos, temos seguro incluso para danos no equipamento ocorridos na prática do surfe.', 'active'),
(6, 'Quais pranchas o Surf’s Up Club possui?', 'Temos pranchas dos mais variados estilos, tamanhos e modelos. Cardápio completo para o cliente desfrutar como preferir. Atualmente nosso quiver conta com pranchas de performance, longboard, funboard, fish model, single fin, alternativas, mini long, alaia, softboard, SUP e prancha de equilíbrio.', 'active'),
(7, 'Quais são as marcas parceiras do Surf’s Up Club?', 'Atualmente são Flap Company, Zabo Surfboards, Adriano Nunes, Arenque Surfboards, Tropical Brasil, Concept Surfboards, Attack Surfboarfs, Equilibra Boards, Leon Ades e Wet Dreams.', 'active'),
(8, 'Como reservo uma prancha?', 'Com o cadastro efetuado, após a confirmação do pagamento, o sócio será direcionado direto ao nosso sistema de reservas. Basta o cliente filtrar a prancha por localização e/ou filtros avançados, escolher a data desejada e confirmar a reserva. Após isso, basta o cliente comparecer no estabelecimento parceiro escolhido e retirar a prancha com o ID de reserva recebido', 'inactive'),
(9, 'Onde eu posso retirar as pranchas?', 'Atualmente, temos 3 pontos disponíveis para retirada e devolução de pranchas. Os locais disponíveis são Maresias, Barra do Sahy e São Paulo Capital', 'active'),
(10, 'Quantas vezes eu posso trocar de prancha?', 'Quantas você quiser! Nós incentivamos nossos clientes a experimentar o maior número de pranchas possível para a melhor experiência no surfe.', 'active'),
(11, 'O que acontece se eu quebrar uma prancha/quilha/equipamento?', 'O Surf’s Up Club inclui seguro para danos nos equipamentos ocorridos na prática do surfe. No entanto, precisamos que você nos retorne a prancha, leash, quilha ou capa danificada. Mas lembre-se: O Surf’s Up Club se responsabiliza APENAS para danos ocorridos na PRÁTICA DO SURFE. Caso seja comprovado que a prancha foi danificada por negligência do usuário fora da prática do surfe, o cliente será responsável por repor o equipamento danificado para o Surf''s Up Club.', 'active'),
(12, 'Quanto tempo posso ficar com a prancha?', 'Se você possui um dos planos de assinatura, o tempo máximo que você pode ficar consecutivamente com uma prancha é de 7 dias. Caso você tenha alguns dos pacotes avulsos, o limite é o número de dias contratado no seu plano', 'active'),
(13, 'O que acontece caso alguém roube a prancha, ou se eu perder?', 'Não queremos que isso aconteça de jeito nenhum, mas, caso aconteça, o cliente será responsável por repor outra do mesmo modelo de acordo com o valor de mercado disponibilizado por cada um dos nossos shapers (mantenha o olho na prancha).', 'inactive'),
(14, 'Eu posso pegar mais de uma prancha por vez?', 'Atualmente o cliente tem direito a retirar apenas uma prancha por vez. No entanto, estamos trabalhando em novos planos que incluem mais itens simultaneamente.', 'active'),
(15, 'Como sei qual prancha está disponível para reserva?', 'No painel de controle do cliente é possível filtrar as prancha disponíveis por data em filtros avançados', 'active'),
(16, 'É possível cancelar a reserva da prancha?', 'Sim, caso você tenha um dos pacotes avulsos, você pode cancelar a reserva da sua prancha com até 24hrs de antecedência no nosso sistema de reservas online. Se você cancelar a sua reserva com essa antecedência, não será descontado nenhum crédito de dia de uso do nosso quiver.', 'active'),
(17, 'E se eu não conseguir entregar a prancha no dia da devolução?', 'Em caso de atraso no dia da devolução da prancha o assinante será penalizado por uma multa no valor de R$90,00/dia.', 'active'),
(18, 'Por que existe a multa?', 'Nós odiamos qualquer cobrança extra dos nosso clientes e não desejamos fazer isto, porém, precisamos implementar esta penalidade para que as pranchas sejam devolvidas nas datas estabelecidadas, já que a mesma prancha em uso pode ter sido reservada por outro cliente, prejudicando assim a experiência de outro. Portanto, devolva sua prancha dentro do prazo determinado! =)', 'active'),
(19, 'Posso comprar as pranchas que estão disponíveis no quiver?', 'No momento, as pranchas disponíveis no clube não estão sendo comercializadas. Se deseja adquirir alguma delas, recomendamos que você entre em contato diretamente com o shaper parceiro utilizando o nosso código de desconto especial para clientes do clube.', 'active'),
(20, 'Posso devolver a prancha antes do final do prazo?', 'Sim, você pode e esperamos que você escolha outra prancha no lugar da prancha devolvida.', 'active'),
(21, 'Não consigo fazer o login na minha conta e meu e-mail e senha estão corretos, o que eu faço?', 'Entre em contato com contato@surfsupclub.com que resolveremos o seu problema o quanto antes.', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_location`
--

CREATE TABLE IF NOT EXISTS `tbl_location` (
  `lcc_id` int(11) NOT NULL AUTO_INCREMENT,
  `loc_id` int(11) NOT NULL,
  `lcc_title` varchar(50) NOT NULL,
  `lcc_start_time` varchar(10) NOT NULL,
  `lcc_end_time` varchar(10) NOT NULL,
  `lcc_status` varchar(20) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`lcc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `tbl_location`
--

INSERT INTO `tbl_location` (`lcc_id`, `loc_id`, `lcc_title`, `lcc_start_time`, `lcc_end_time`, `lcc_status`) VALUES
(6, 6, 'Hotel Maui', '', '', 'active'),
(15, 7, 'Drifter Hostel', '', '', 'active'),
(23, 9, 'Goiás', '', '', 'active'),
(24, 9, 'Mato Grosso', '', '', 'active'),
(25, 9, 'Mato Grosso do Sul', '', '', 'active'),
(26, 9, 'Distrito Federal', '', '', 'active'),
(28, 10, 'Espírito Santo', '', '', 'active'),
(29, 10, 'Minas Gerais', '', '', 'active'),
(30, 10, 'Rio de Janeiro', '', '', 'active'),
(31, 10, 'São Paulo', '', '', 'active'),
(32, 11, 'Paraná', '', '', 'active'),
(33, 11, 'Rio Grande do Sul', '', '', 'active'),
(34, 11, 'Santa Catarina', '', '', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_location_category`
--

CREATE TABLE IF NOT EXISTS `tbl_location_category` (
  `loc_id` int(11) NOT NULL AUTO_INCREMENT,
  `loc_title` varchar(50) NOT NULL,
  `loc_image` varchar(255) NOT NULL,
  `loc_status` varchar(20) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`loc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_location_category`
--

INSERT INTO `tbl_location_category` (`loc_id`, `loc_title`, `loc_image`, `loc_status`) VALUES
(6, 'Maresias', '9f4ce67eaf43fae2c15a4fa4422b4f79.jpg', 'active'),
(7, 'Barra do Sahy', '68f2f025328a23d58edb2d1d7dfb5f80.jpg', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_members`
--

CREATE TABLE IF NOT EXISTS `tbl_members` (
  `mem_id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_first` varchar(50) NOT NULL,
  `mem_last` varchar(50) NOT NULL,
  `mem_email` varchar(60) NOT NULL,
  `mem_password` varchar(200) NOT NULL,
  `mem_phone` varchar(30) NOT NULL,
  `mem_country` varchar(30) NOT NULL,
  `mem_city` varchar(50) NOT NULL,
  `mem_zip` varchar(10) NOT NULL,
  `mem_address` varchar(200) NOT NULL,
  `mem_pic` varchar(100) NOT NULL,
  `mem_status` varchar(20) NOT NULL DEFAULT 'active',
  `mem_verify` int(11) NOT NULL DEFAULT '0',
  `mem_passport` varchar(50) NOT NULL,
  PRIMARY KEY (`mem_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=117 ;

--
-- Dumping data for table `tbl_members`
--

INSERT INTO `tbl_members` (`mem_id`, `mem_first`, `mem_last`, `mem_email`, `mem_password`, `mem_phone`, `mem_country`, `mem_city`, `mem_zip`, `mem_address`, `mem_pic`, `mem_status`, `mem_verify`, `mem_passport`) VALUES
(5, 'Muhammad', 'Sarmad', 'sarmad711@gmail.com', 'e5cd253cba454363ed4e56783eb1515b782848ce', '3458341704', '31', 'Sargodha', '41000000', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 1, '426.169.158-20'),
(6, 'jahanzaib', 'khalid', 'iamgenious.1@gmail.com', '0dfe6fed0c80397e167d7f580725fc7033cea47b', '3227602530', '168', 'sargodha', '40100', 'block no 33 house no 133', '', 'active', 1, ''),
(7, 'Lucas', 'Gonçalves', 'contatosurfsup@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '', '', '', '', '', '', 'active', 0, ''),
(8, 'Lucas', 'Gonçalves Sinisgalli', 'contatosurfsup1@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '1193938', '31', 'São Paulo', '01426000', 'Oscar Freire 112', 'bb159a3035bc9219c81b07879046cf7b.jpeg', 'active', 0, ''),
(9, 'Muhammad', 'Sarmad', 'testing@live.com', 'd545481c46ebe5d1e958b7dea80d954044269f7b', '', '', '', '', '', '', 'active', 0, ''),
(10, 'jahanzaib', 'khalid', 'herosolutions.tk@gmail.com', '0dfe6fed0c80397e167d7f580725fc7033cea47b', '03227602530', '1', 'sargodha', '40100', 'block no 33 house no 133', '', 'active', 0, ''),
(11, 'Nathalia', 'Goto', 'nath.goto@gmail.com', '94793f1838e9751efb080b947ad76a106564120a', '11972574576', '31', 'Sao Sebastiao', '11618246', 'Rua Manganes,868 casa 1', '', 'active', 0, ''),
(12, 'Lucas', 'zika', 'lucas123@hotmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '119', '10', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(13, 'Lucas', 'xei', 'lucas1234@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '119', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(14, 'Thiago', 'Guimarães', 'tgoguimaraes@gmail.com', 'd5e75c39e0b923655a86d9c6bce1bad9977ca2de', '11973388148', '31', 'são paulo', '04614020', 'Rua Augusto Ribeiro Filho, 40', '', 'active', 0, ''),
(15, 'Lucas', 'Sinisgalli', 'lucas12345@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '123', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(16, 'Fique por dentro', 'tubo', 'daniel@gmail.com', '69fa8766dd162a1d5d6d01671b70ef2a23b82184', '11 9999-9999', '31', 'São Sebastião', '11622-124', 'Rua Izidro Jorge, 500 - Barra do Sahy', '', 'active', 0, ''),
(17, 'Lucas', 'goncalves', 'lucas123456@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(18, 'sarim', 'Khan', 'sarim@gmail.com', 'ec5a7f6b53c89e3478f37c4858e279020e64589f', '+123789456', '235', 'california', '60100', 'gfhjfghjfgh', '', 'active', 0, ''),
(19, 'Andre', 'vieira', 'andrevmoreira1@gmail.com', '69fa8766dd162a1d5d6d01671b70ef2a23b82184', '11987198533', '31', 'Sao paulo', '04789010', 'Av sylvio sciumbata 355', '34f610f79e34e76fc8bbf7449efa89fe.jpeg', 'active', 0, ''),
(20, 'lucas', 'guedes', 'lucascdg@al.insper', '5489218533b547c86228bb57219e5d5ac42c3b11', '0451850507', '31', 'Freshwater', '2096', '3/37 MOORE ROAD', '', 'active', 0, ''),
(21, 'Lucas', 'Gonçalves', 'lopsquintas@hotmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(22, 'Lucas', 'sinisgalli', 'lucas123456@hotmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(23, 'Lucas', 'Zika', 'lucaszika@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(24, 'Lucas', 'Gonçalves Sinisgalli', '123456@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, '426.169.158-20'),
(25, 'Muhammad', 'Sarmad', 'sarmad711@gmail.coms', 'e5cd253cba454363ed4e56783eb1515b782848ce', '3458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, ''),
(26, 'Lucas', 'G', 'lucass@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '1119', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(27, 'Lucas', 'Gonçalves Sinisgalli', 'lgs@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, ''),
(28, 'jahanzaib', 'khalid', 'iamgenious.2@gmail.com', '0dfe6fed0c80397e167d7f580725fc7033cea47b', '03227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(29, 'Muhammad', 'Sarmad', 'anyemail@gmail.com', 'e5cd253cba454363ed4e56783eb1515b782848ce', '+923458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, ''),
(30, 'Muhammad', 'Sarmad', 'alihaider@live.com', 'd545481c46ebe5d1e958b7dea80d954044269f7b', '03458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', 'acb534f7b1475705a8399b2dacd5258d.gif', 'active', 0, ''),
(31, 'Muhammad', 'Sarmad', 'developer@testing.com', '7f278e831fdbcf6d1c75d3a6a2534e8f4bdbe43c', '03458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, ''),
(32, 'Muhammad', 'Sarmad', 'developer2@testing.com', '36c03b9f8989849c9ddefcf4b01bc1e6d0cc9531', '03458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, ''),
(33, 'Lucas', 'Gonçalves Sinisgalli', '123@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, ''),
(34, 'Muhammad', 'Sarmad', 'developer3@account.com', '7f278e831fdbcf6d1c75d3a6a2534e8f4bdbe43c', '03458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, ''),
(35, 'jahanzaib', 'khalid', 'iamgenious.5@gmail.com', '0dfe6fed0c80397e167d7f580725fc7033cea47b', '03227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(36, 'Lucas', 'Gonçalves Sinisgalli', '12@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(37, 'Muhammad', 'Sarmad', 'developer4@gmail.com', '7f278e831fdbcf6d1c75d3a6a2534e8f4bdbe43c', '03458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, ''),
(38, 'Lucas', 'Gonçalves Sinisgalli', '1@gmail.com', 'b6f753cf6a46eafba046f08b48ca02a04923cf5d', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, ''),
(39, 'jahanzaib', 'khalid', 'admin545@gmail.com', '0dfe6fed0c80397e167d7f580725fc7033cea47b', '03227602530', '168', 'sargodha', '40100', 'block no 33 house no 133', '', 'active', 0, ''),
(40, 'jahanzaib', 'khalid', '545454admin@gmail.com', '0dfe6fed0c80397e167d7f580725fc7033cea47b', '03227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(41, 'jahanzaib', 'khalid', 'adm78787n@gmail.com', '0dfe6fed0c80397e167d7f580725fc7033cea47b', '03227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(42, 'Muhammad', 'Sarmad', 'sarmad711@gmail.comzs', 'e5cd253cba454363ed4e56783eb1515b782848ce', '03458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, ''),
(43, 'Daniel', 'Cappellette', 'danielcappellette@gmail.com', 'cfdd320e112eb508430d5e6e8feba67d7e58bb40', '11999814191', '31', 'SÃO PAULO', '11618246', 'Rua Manganês, 868, Casa 1', '', 'active', 0, ''),
(44, 'Xeik', 'Vida Lok', '0@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '119', '31', 'São Paulo', '01426000', 'Rua Oscar Freire, 112', '', 'active', 0, ''),
(45, 'Lucas', 'Gonçalves', '01@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '119', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(46, 'Lucas', 'Z', '12456@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '', '', '', '', '', '', 'active', 0, ''),
(47, 'ali', 'khan', 'ali@gmail.com', 'ec5a7f6b53c89e3478f37c4858e279020e64589f', '0123456789', '2', 'dfgfdgfdg', 'dfdfgfdg', 'fdgfdg', '', 'active', 0, ''),
(48, 'jahanzaib', 'khalid', 'admin111@gmail.com', '046c8d403665f97d978403dd06c89a4642e405fc', '03227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(49, 'jahanzaib', 'khalid', 'admin54545@gmail.com', '046c8d403665f97d978403dd06c89a4642e405fc', '03227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(50, 'jahanzaib', 'khalid', 'admin2222@gmail.com', '046c8d403665f97d978403dd06c89a4642e405fc', '03227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(51, 'Muhammad', 'Sarmad', 'anyname@gmail.comz', 'e5cd253cba454363ed4e56783eb1515b782848ce', '+923458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, ''),
(52, 'Muhammad', 'Sarmad', 'testing@gmail.co', '0454d7eced72d093e4c7565eecba1579f1920fda', '+923458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, ''),
(53, 'Muhammad', 'Sarmad', 'sam@live.com', 'd545481c46ebe5d1e958b7dea80d954044269f7b', '3458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, ''),
(54, 'Mazhar', 'Hussain', 'any@one.com', 'd545481c46ebe5d1e958b7dea80d954044269f7b', '3027712782', '168', 'Vehari', '61100', 'Vehari', '', 'active', 0, ''),
(55, 'Muhammad', 'Sarmad', 'samn@gam.com', '2b563deefebc0d467bcff6b33d9121f3d23aa779', '3458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, ''),
(56, 'jahanzaib', 'khalid', 'admin9023409@gmail.com', '3f9d82f7e279e4839cb502b67ca273a3bde3e159', '3227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(57, 'jahanzaib', 'khalid', 'iamgenious.55665561@gmail.com', '73fc947aa005820847e541f4deae870d8cd91f88', '3227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(58, 'jahanzaib', 'khalid', 'iamgenious1545.1@gmail.com', '046c8d403665f97d978403dd06c89a4642e405fc', '3227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(59, 'jahanzaib', 'khalid', 'asdasdiamgenious.1@gmail.com', '046c8d403665f97d978403dd06c89a4642e405fc', '3227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(60, 'Lucas', 'Sinisgalli', '012@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '119348', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(61, 'Lucas', 'Gonçalves Sinisgalli', '30@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, ''),
(62, 'Ilan', 'Lebl', 'ilan_jkl@mac.com', '046c8d403665f97d978403dd06c89a4642e405fc', '', '', '', '', '', '', 'active', 0, ''),
(63, 'Lucas', 'Sinisgalli', 'l1@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Rua Oscar Freire, 112 ap 62', 'd34eb3984e637662ce2ec7819e8b4ff8.jpeg', 'active', 0, ''),
(64, 'Lucas', 'Zica', '1212@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(65, 'Drama surf', 'Foto', 'dramasurffoto@gmail.com', '69fa8766dd162a1d5d6d01671b70ef2a23b82184', '11939387311', '31', 'São Sebastião', '11622-124', 'Rua Izidro Jorge, 500 - Barra do Sahy', '', 'active', 0, ''),
(66, 'Joao', 'Pinto', 'joaopinto@gmail.com', 'e6df37903f62d81f1179235f84d8f4aaa42f4bcf', '11999999989', '31', 'SÃO PAULO', '02404010', 'RUA PONTINS, 65, APTO 81', '', 'active', 0, ''),
(67, 'Xeik', 'drone', 'xeikdrone@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '119', '31', 'Sao paulo', '01426000', 'Rua Oscar Freire 112', '768fe40edce7f05302786dacdba0cbc0.jpeg', 'active', 0, ''),
(68, 'Lucas', 'xeik', '121@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(69, 'Muhammad', 'Sarmad', 'developer33@gmail.com', '7f278e831fdbcf6d1c75d3a6a2534e8f4bdbe43c', '3458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, ''),
(70, 'Lucas', 'Gonçalves Sinisgalli', 'lucassinisgalli12@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, ''),
(71, 'Lucas', 'Gonçalves Sinisgalli', 'lucassinisgalli1@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, ''),
(72, 'Daniel', 'Cappellette', 'danielcmf@al.insper.edu.br', '5489218533b547c86228bb57219e5d5ac42c3b11', '1199981-4191', '31', 'SÃO PAULO', '02404010', 'RUA PONTINS, 65, APTO 81', '', 'active', 0, ''),
(73, 'Lucas', '!@#', '011@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(74, 'Lucas', 'Gonçalves', '121212@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, ''),
(75, 'Lucas', 'Gonçalves', '12312@gmail.com', 'c0f2652a30bd8f0312bf33d2a82980dd2e504d88', '', '', '', '', '', '', 'active', 0, ''),
(76, 'Muhammad', 'Sarmad', 'dev@yahoo.com', 'e5cd253cba454363ed4e56783eb1515b782848ce', '3458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, ''),
(77, 'Muhammad', 'Sarmad', 'dev@testing.com', 'e5cd253cba454363ed4e56783eb1515b782848ce', '3458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, ''),
(78, 'Muhammad', 'Sarmad', 'hello@world.com', 'e5cd253cba454363ed4e56783eb1515b782848ce', '3458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, ''),
(79, 'Muhammad', 'Sarmad', 'asad@testing.com', 'e5cd253cba454363ed4e56783eb1515b782848ce', '3458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, ''),
(80, 'Lucas', 'Gonçalves Sinisgalli', 'silviammgon@hotmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, ''),
(81, 'jahanzaib', 'khalid', 'alkjsad@gmail.com', '046c8d403665f97d978403dd06c89a4642e405fc', '3227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(82, 'jahanzaib', 'khalid', 'adminasdasd@gmail.com', '0dfe6fed0c80397e167d7f580725fc7033cea47b', '3227602530', '168', 'sargodha', '40100', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, ''),
(83, 'Muhammad', 'Sarmad', 'iamtesting@gmail.com', 'e5cd253cba454363ed4e56783eb1515b782848ce', '3458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, ''),
(84, 'Felipe', 'Cappellette', 'felipe.cmf@gmail.com', '0dfe6fed0c80397e167d7f580725fc7033cea47b', '', '', '', '', '', '', 'active', 0, ''),
(85, 'Muhammad', 'Sarmad', 'testingsam@gmailc.om', 'e5cd253cba454363ed4e56783eb1515b782848ce', '3458341704', '31', 'Sargodha', '40100000', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, '426.169.158-20'),
(86, 'Lucas', 'Xeik', 'lucasxeik@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire, 112 ap 62', '', 'active', 0, '426.169.158-20'),
(87, 'Joao', 'da', 'joniesilva@gmail.com', '145cf3da290176e664d37350f1373cb4bdc90375', '', '', '', '', '', '', 'active', 0, ''),
(88, 'Lucas', 'Gonçalves Sinisgalli', '12345678@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, '426.169.158-20'),
(89, 'Lucas', 'Gonçalves Sinisgalli', '123456789@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, '426.169.158-20'),
(90, 'rodrigo', 'monteiro', 'oke.rodrigo@gmail.com', '3d1dff819136dc7b0a2b80a52968ae8ef8ae75c5', '11996696555', '31', 'Sao Paulo', '05612000', 'rua olegario mariano, 810', '', 'active', 0, '105.003.737-52'),
(91, 'Kelly', 'Slater', 'k.slater@gmail.com', '64bb5ec991a1216fb7526bb286920d1adb8a8d96', '1111111111', '31', 'São Sebastião', '11618246', 'Rua Manganês, 868, casa 01', '', 'active', 0, '999.999.999-99'),
(92, 'Lucas', 'Gonçalve', 'lucas420@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Rua Oscar Freire, 112', '', 'active', 0, '426.169.158-20'),
(93, 'Muhammad', 'Sarmad', 'samabc@gmail.com', '0fdeed2c453114dc65849a0d79ef1255eecca98c', '3458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, '544000194568678'),
(94, 'Lucas', 'Gonçalves Sinisgalli', '11@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, '426.169.158-20'),
(95, 'Lucas', 'Gonçalves Sinisgalli', '6@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, '426.169.158-20'),
(96, 'Muhammad', 'Sarmad', 'newuser@gmail.com', '895e50a66def85d3ea8a3a3b01247bde10b342b8', '3458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, '5440001245688873'),
(97, 'Muhammad', 'Sarmad', 'newuser2@gmail.com', '03aad02ba517322a5484d2fd9b41e09c87d0f6e4', '3458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, '123456789000475'),
(98, 'surfsup', 'club', 'contato@surfsupclub.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Rua Oscar Freire, 112', '', 'active', 0, '426.169.158-20'),
(99, 'Lucas', 'Xeik', '12121212@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112', '', 'active', 0, '426.169.158-20'),
(100, 'Lucas', 'Gonçalves Sinisgalli', 'surf123@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, '426.169.158-20'),
(101, 'Muhammad', 'Sarmad', 'newnew@gmail.com', '895e50a66def85d3ea8a3a3b01247bde10b342b8', '3458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, '46789456123645'),
(102, 'Lucas', 'Gonçalves Sinisgalli', '12123456@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, '426.169.158-20'),
(103, 'Lucas', 'Gonçalves Sinisgalli', '123123@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, '426.169.158-20'),
(104, 'Mazhar', 'Hussain', 'sarmad711@gmail.comsfds', 'e5cd253cba454363ed4e56783eb1515b782848ce', '3027712782', '168', 'Vehari', '61100', 'Vehari', '', 'active', 0, '3424324325435435'),
(105, 'Muhammad', 'Sarmad', 'newuser@testing.sam', '0454d7eced72d093e4c7565eecba1579f1920fda', '3458341704', '168', 'Sargodha', '40100', 'N.S.T , Sargodha', '', 'active', 0, '4578945412156'),
(106, 'jahanzaib', 'khalid', '565656@gmail.com', '046c8d403665f97d978403dd06c89a4642e405fc', '3227602530', '31', 'sao paulo', '01426000', 'block no 33 house no 133, block no 33 house no 133', '', 'active', 0, '426.169.158-20'),
(107, 'Lucas', 'G', 'l1l2@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'S', '01426000', 'Oscar Freire 112', '', 'active', 0, '426.169.158-20'),
(108, 'Muhammad', 'Sarmad', 'anyusertesting@gmail.com', 'b4a8542ac582864febc373f888d4853462856613', '3458341704', '31', 'sao paulo', '01426000', 'N.S.T , Sargodha', '', 'active', 0, '426.169.158-20'),
(109, 'jahanzaib', 'khalid', 'testtest123@gmail.com', '046c8d403665f97d978403dd06c89a4642e405fc', '5656565656565', '31', 'sao paulo', '01426000', 'asdasd', '', 'active', 0, '426.169.158-20'),
(110, 'Muhammad', 'Sarmad', 'testing.sarmad@live.com', 'e5cd253cba454363ed4e56783eb1515b782848ce', '3458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, '63435435435435'),
(111, 'Lucas', 'Gonçalves Sinisgalli', '11111@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, '426.169.158-20'),
(112, 'Muhammad', 'Sarmad', 'useruertesting@gmail.coms', '8af15e5bedc98cb10bf322f8a39505b118672644', '3458341704', '168', 'Sargodha', '40100', 'House # 21 Street #38 Block Z new Satlitetown', '', 'active', 0, '4324436546546545654'),
(113, 'Lucas', 'Gonçalves Sinisgalli', '1236@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', '11939387311', '31', 'São Paulo', '01426000', 'Oscar Freire 112, 62', '', 'active', 0, '426.169.158-20'),
(114, 'AHSAN', 'KAZI', 'adminhajaj@gmail.com', '046c8d403665f97d978403dd06c89a4642e405fc', '1612055557', '31', 'Manchester', 'm44fg', '116 Cheetham Hill Road, First Floor, Manchester M4 4FG', '', 'active', 0, '426.169.158-20'),
(115, 'AHSAN', 'khaid', 'iamgenious.10022@gmail.com', '046c8d403665f97d978403dd06c89a4642e405fc', '545454', '31', 'Manchester', 'm44fg', '116 Cheetham Hill Road, First Floor, Manchester M4 4FG', '', 'active', 0, '426.169.158-20'),
(116, 'Raphael', 'Wright', 'raphaelwright@gmail.com', '975f8898a0d983b9938ba2dafa496b828b0eda83', '11981811428', '31', 'São Paulo ', '05458001', 'Av Diogenes Ribeiro de Lima, 2361', '', 'active', 0, '310.278.528-07');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE IF NOT EXISTS `tbl_order` (
  `ord_id` int(11) NOT NULL AUTO_INCREMENT,
  `sur_id` int(11) NOT NULL,
  `ord_surfboard_date` int(11) NOT NULL,
  `mem_id` int(11) NOT NULL,
  `pkg_id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `ord_time` int(11) NOT NULL,
  `ord_status` varchar(30) NOT NULL DEFAULT 'pending',
  `ord_number` varchar(255) NOT NULL,
  `ord_start` int(11) NOT NULL,
  `ord_end` int(11) NOT NULL,
  PRIMARY KEY (`ord_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`ord_id`, `sur_id`, `ord_surfboard_date`, `mem_id`, `pkg_id`, `trx_id`, `ord_time`, `ord_status`, `ord_number`, `ord_start`, `ord_end`) VALUES
(2, 10, 1558033200, 105, 5, 2, 1557954982, 'canceled', '7941905160216105', 1557946800, 1558119600),
(3, 10, 1558119600, 105, 5, 2, 1557954982, 'canceled', '7941905160216105', 1557946800, 1558119600),
(4, 10, 1558378800, 105, 5, 2, 1557955000, 'canceled', '4551905160216105', 1558378800, 1558378800),
(9, 9, 1559070000, 105, 5, 2, 1557955073, 'canceled', '3961905160217105', 1559070000, 1559156400),
(10, 9, 1559156400, 105, 5, 2, 1557955073, 'canceled', '3961905160217105', 1559070000, 1559156400),
(26, 18, 1557946800, 110, 5, 9, 1558010579, 'canceled', '4421905160542110', 1557946800, 1557946800),
(27, 18, 1558033200, 110, 5, 9, 1558010635, 'canceled', '9501905160543110', 1558033200, 1558033200),
(28, 10, 1557946800, 111, 4, 12, 1558017338, 'canceled', '5341905160735111', 1557946800, 1557946800),
(29, 10, 1558033200, 111, 4, 12, 1558017399, 'canceled', '4121905160736111', 1558033200, 1558033200),
(31, 10, 1557946800, 112, 5, 13, 1558032928, 'canceled', '8411905161155112', 1557946800, 1558119600),
(32, 10, 1558033200, 112, 5, 13, 1558032928, 'canceled', '8411905161155112', 1557946800, 1558119600),
(33, 10, 1558119600, 112, 5, 13, 1558032928, 'canceled', '8411905161155112', 1557946800, 1558119600),
(34, 10, 1558119600, 112, 5, 13, 1558033247, 'canceled', '1981905171200112', 1558119600, 1558119600),
(38, 10, 1558033200, 113, 5, 15, 1558086667, 'canceled', '8741905170251113', 1558033200, 1558033200),
(39, 10, 1558119600, 113, 5, 15, 1558086845, 'canceled', '9021905170254113', 1558119600, 1558119600),
(42, 10, 1558033200, 24, 1, 17, 1558123177, 'canceled', '131190518125924', 1558033200, 1558033200),
(43, 18, 1559156400, 92, 1, 19, 1558482267, 'canceled', '708190522044492', 1559156400, 1559242800),
(44, 18, 1559242800, 92, 1, 19, 1558482267, 'canceled', '708190522044492', 1559156400, 1559242800),
(45, 18, 1558465200, 24, 1, 17, 1558551793, 'canceled', '648190523120324', 1558465200, 1558551600),
(46, 18, 1558551600, 24, 1, 17, 1558551793, 'canceled', '648190523120324', 1558465200, 1558551600),
(47, 9, 1558465200, 24, 1, 17, 1558551847, 'pending', '240190523120424', 1558465200, 1558465200),
(48, 9, 1559415600, 5, 6, 1, 1558614404, 'pending', '95119052305265', 1559415600, 1559847600),
(49, 9, 1559502000, 5, 6, 1, 1558614404, 'pending', '95119052305265', 1559415600, 1559847600),
(50, 9, 1559588400, 5, 6, 1, 1558614404, 'pending', '95119052305265', 1559415600, 1559847600),
(51, 9, 1559674800, 5, 6, 1, 1558614404, 'pending', '95119052305265', 1559415600, 1559847600),
(52, 9, 1559761200, 5, 6, 1, 1558614404, 'pending', '95119052305265', 1559415600, 1559847600),
(53, 9, 1559847600, 5, 6, 1, 1558614404, 'pending', '95119052305265', 1559415600, 1559847600),
(54, 10, 1558638000, 116, 1, 21, 1558719907, 'pending', '6921905241045116', 1558638000, 1558724400),
(55, 10, 1558724400, 116, 1, 21, 1558719907, 'pending', '6921905241045116', 1558638000, 1558724400);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_status`
--

CREATE TABLE IF NOT EXISTS `tbl_order_status` (
  `ors_id` int(11) NOT NULL AUTO_INCREMENT,
  `ord_id` int(11) NOT NULL,
  `ors_status` varchar(50) NOT NULL,
  PRIMARY KEY (`ors_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_order_status`
--

INSERT INTO `tbl_order_status` (`ors_id`, `ord_id`, `ors_status`) VALUES
(1, 2147483647, 'canceled'),
(2, 2147483647, 'issued'),
(3, 2147483647, 'completed'),
(4, 2147483647, 'issued'),
(5, 2147483647, 'issued'),
(6, 2147483647, 'issued'),
(7, 2147483647, 'completed'),
(8, 2147483647, 'issued'),
(9, 2147483647, 'issued');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_packages`
--

CREATE TABLE IF NOT EXISTS `tbl_packages` (
  `pkg_id` int(11) NOT NULL AUTO_INCREMENT,
  `pkg_title` varchar(200) NOT NULL,
  `pkg_type` varchar(50) NOT NULL,
  `pkg_price` float NOT NULL,
  `pkg_time` int(11) NOT NULL,
  `pkg_duration` varchar(20) NOT NULL,
  `pkg_booking_per_day` int(11) NOT NULL,
  `pkg_cancel_hours` int(11) NOT NULL,
  `pkg_status` varchar(20) NOT NULL,
  PRIMARY KEY (`pkg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_packages`
--

INSERT INTO `tbl_packages` (`pkg_id`, `pkg_title`, `pkg_type`, `pkg_price`, `pkg_time`, `pkg_duration`, `pkg_booking_per_day`, `pkg_cancel_hours`, `pkg_status`) VALUES
(1, 'Yearly', 'SUBSCRIPTIONS', 26.5, 1, 'year', 0, 0, 'active'),
(2, 'Plano Mensal', 'SUBSCRIPTIONS', 149, 0, 'month', 3, 0, 'active'),
(3, 'Bate-volta', 'PACKAGES', 1, 1, 'day', 0, 0, 'active'),
(4, 'Floater', 'PACKAGES', 129, 3, 'day', 0, 0, 'active'),
(5, 'Layback', 'PACKAGES', 189, 5, 'day', 0, 0, 'active'),
(6, 'Surftrip', 'PACKAGES', 259, 10, 'day', 0, 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_packages_properties`
--

CREATE TABLE IF NOT EXISTS `tbl_packages_properties` (
  `tpp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpp_property` varchar(50) NOT NULL,
  `tpp_value` varchar(500) NOT NULL,
  `tpp_status` varchar(20) NOT NULL,
  `pkg_id` int(11) NOT NULL,
  PRIMARY KEY (`tpp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `tbl_packages_properties`
--

INSERT INTO `tbl_packages_properties` (`tpp_id`, `tpp_property`, `tpp_value`, `tpp_status`, `pkg_id`) VALUES
(18, 'Book multiple', 'products at once', '', 1),
(19, 'All damages', 'except loss/stolen covered by Surf''s up', '', 1),
(20, 'Prebook 6 months', 'in advance', '', 1),
(21, 'Free cancellation', '24 hrs before booking', '', 1),
(22, 'Discount', 'off select daily bookings', '', 1),
(43, 'drama ', 'wave', '', 3),
(44, 'Minor damages', 'covered by Surf''s up', '', 3),
(45, 'Prebook 6 months', 'in advance', '', 3),
(46, 'Free cancellation 48 hrs before', 'booking', '', 3),
(51, 'Book multiple', 'products at once', '', 2),
(52, 'Minor damages', 'covered by Surf''s up', '', 2),
(53, 'Prebook 6 months', 'in advance', '', 2),
(54, 'Free cancellation 48 hrs before', 'booking', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_people`
--

CREATE TABLE IF NOT EXISTS `tbl_people` (
  `people_id` int(11) NOT NULL AUTO_INCREMENT,
  `people_title` varchar(200) NOT NULL,
  `people_name` varchar(100) NOT NULL,
  `people_image` varchar(500) NOT NULL,
  `people_description` text NOT NULL,
  `people_status` varchar(20) NOT NULL,
  PRIMARY KEY (`people_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_people`
--

INSERT INTO `tbl_people` (`people_id`, `people_title`, `people_name`, `people_image`, `people_description`, `people_status`) VALUES
(1, 'CEO', 'Xeik', '95e611c5e4c4108c74c9e6e43e2300a7.jpg', 'Sapatada do xeik', 'active'),
(2, 'CMO', 'Drama', 'b7f0db3c5f17173681748cc49ef26480.jpg', 'Do surf', 'active'),
(3, 'CFO', 'Espirro', '9207cfc4fa539f3ac5b91863899e641e.jpeg', 'Zica do Pântano', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reset`
--

CREATE TABLE IF NOT EXISTS `tbl_reset` (
  `reset_id` int(11) NOT NULL AUTO_INCREMENT,
  `reset_hash` varchar(500) NOT NULL,
  `mem_id` varchar(80) NOT NULL,
  `reset_status` varchar(20) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `tbl_reset`
--

INSERT INTO `tbl_reset` (`reset_id`, `reset_hash`, `mem_id`, `reset_status`) VALUES
(1, 'baf9c55431965f73bf52f31c34b5f944252e0475', '3', 'active'),
(2, '803533d0618eb178ea1c8b3920bf9d9b770ee244', '3', 'active'),
(3, '775f0aa6be2ca67ce6d893127c0068ce2c86a7da', '3', 'active'),
(4, 'fc2d1c75a5645b7b696723dfb88792434f2efb01', '0', 'active'),
(5, '928c925cd4cdf3a55e91d52bde7ce5b06f04b7d3', '0', 'active'),
(6, 'e87be94b4090ec2875aede3484a063f19c02ab24', '0', 'active'),
(7, '038720d0d219540ecaadd2897d48a4250441d22e', '0', 'active'),
(8, '5d106f98cd5ab37bb4d6c3d75c8ee8040a91f082', '3', 'active'),
(9, 'f2cc73d7f3d871bf92cc1ab601c746db5623fe27', '2', 'active'),
(10, '74b1bec87ff43ca1c5dbc1c88aa823b1df0f1f76', '2', 'active'),
(11, 'a6fabe9baf7e07da7d2edf519ab2def42e660117', '3', 'active'),
(12, '9d7872ac53f254cd77135c549d2e76e8eb5efbcf', '3', 'inactive'),
(13, 'bc77db41f6addbd21fe94bd4699b73e0229aec05', '3', 'inactive'),
(14, 'f124e68d1ec70519c6dd9f7a6561e62f3bdf65d4', '3', 'inactive'),
(15, '5c9a0bdffcf0e3a92f3cba8d82694938f3311772', '6', 'inactive'),
(16, '043b9c7458d1236cfe13089e4510a5fd43e18900', '7', 'active'),
(17, '0fe5c72ed899cbfa0190cb476fdb946547f4773c', '8', 'active'),
(18, '9e2eb8b65321f21332010b17fa44c0224f83fa77', '9', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_title` varchar(100) NOT NULL,
  `slider_image` varchar(200) NOT NULL,
  `slider_status` varchar(20) NOT NULL,
  PRIMARY KEY (`slider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`slider_id`, `slider_title`, `slider_image`, `slider_status`) VALUES
(6, 'pranchas arenque', 'e469f6ccc80d6e4ddef8f93e45fdde54.jpg', 'active'),
(10, 'Spirro indoo', '9eda401f6d33bff3de9de963a50770d0.jpeg', 'active'),
(11, 'Spirro indoo2', '8504c651f63b7f7fcc4e396fb06e74f8.jpeg', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surfboards`
--

CREATE TABLE IF NOT EXISTS `tbl_surfboards` (
  `sur_id` int(11) NOT NULL AUTO_INCREMENT,
  `sur_title` varchar(200) NOT NULL,
  `sur_location` int(11) NOT NULL,
  `sur_volume` float NOT NULL,
  `sur_type` varchar(100) NOT NULL,
  `sur_brand` varchar(100) NOT NULL,
  `sur_length` float NOT NULL,
  `sur_size` varchar(50) NOT NULL,
  `sur_condition` varchar(20) NOT NULL,
  `sur_quantity` int(11) NOT NULL,
  `sur_description` text NOT NULL,
  `sur_status` varchar(20) NOT NULL,
  `o_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`sur_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tbl_surfboards`
--

INSERT INTO `tbl_surfboards` (`sur_id`, `sur_title`, `sur_location`, `sur_volume`, `sur_type`, `sur_brand`, `sur_length`, `sur_size`, `sur_condition`, `sur_quantity`, `sur_description`, `sur_status`, `o_by`) VALUES
(3, 'Adriano Nunes Eccentryc 6 0 Frente', 7, 0, '', '', 0, '5''4 xurie erq', 'New product', 10, 'The Plunder is a fuller-plane shape board designed to bring versatility and fun to small waves without forgetting about that superior performance spark, too. The unique surface area in the outline allows the board to trim with surprising speed in the weakest of conditions, while the soft diamond tail paired with the kick in the rail-line rocker toward the back third, gives the Plunder an ability to respond and turn when a good section lines up.', 'active', NULL),
(8, 'Attack Surfácil Amarela', 7, 2.7, '', '', 0, '5''4 ', 'Good', 15, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 'active', NULL),
(9, 'Attack Surfácil Azul', 15, 2.7, '', 'hello', 0, '5''4 "x 19.75 x 2.38', 'Good', 15, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 'active', 2),
(10, 'Minitank', 15, 40, 'Iniciante', 'FLAP', 6.6, '6''6', 'Good', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 'active', 1),
(11, 'Powerlight Fish Performance', 13, 0, '', 'jhanzaib', 0, '5''4 ', 'Good', 15, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 'active', NULL),
(12, 'Attack Surfácil Amarela', 5, 2.7, '', 'asad', 0, '5''4 "x 19.75 x 2.38', 'Good', 15, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 'active', NULL),
(13, 'Adriano Nunes Eccentryc 6 0 Frente', 12, 2.7, '', '', 0, '5''4 "x 19.75 x 2.38', 'New product', 10, 'The Plunder is a fuller-plane shape board designed to bring versatility and fun to small waves without forgetting about that superior performance spark, too. The unique surface area in the outline allows the board to trim with surprising speed in the weakest of conditions, while the soft diamond tail paired with the kick in the rail-line rocker toward the back third, gives the Plunder an ability to respond and turn when a good section lines up.', 'active', NULL),
(14, 'Zabo Total Flex', 14, 0, '', 'sam', 0, '5''4 x 78945 x 554', 'New product', 10, 'The Plunder is a fuller-plane shape board designed to bring versatility and fun to small waves without forgetting about that superior performance spark, too. The unique surface area in the outline allows the board to trim with surprising speed in the weakest of conditions, while the soft diamond tail paired with the kick in the rail-line rocker toward the back third, gives the Plunder an ability to respond and turn when a good section lines up.', 'active', NULL),
(15, 'Adriano Nunes Eccentryc 6 0 Frente', 23, 4.6, 'Plunder  plunder is a full', 'Plunder ', 3.1, '5''4 ', 'New product', 1, 'The Plunder is a fuller-plane shape board designed to bring versatility and fun to small waves without forgetting about that superior performance spark, too. The unique surface area in the outline allows the board to trim with surprising speed in the weakest of conditions, while the soft diamond tail paired with the kick in the rail-line rocker toward the back third, gives the Plunder an ability to respond and turn when a good section lines up.', 'active', NULL),
(16, 'ZABO Torped', 32, 2.7, 'Types', 'Brands', 3.8, '5''4 ', 'New product', 1, 'The Plunder is a fuller-plane shape board designed to bring versatility and fun to small waves without forgetting about that superior performance spark, too. The unique surface area in the outline allows the board to trim with surprising speed in the weakest of conditions, while the soft diamond tail paired with the kick in the rail-line rocker toward the back third, gives the Plunder an ability to respond and turn when a good section lines up.', 'active', NULL),
(17, 'Attack Surfácil Amarela', 24, 3.7, 'Sur Type', 'Sam''s', 5.2, '5''4 ', 'Good', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 'active', NULL),
(18, 'Anarchy', 6, 27.4, 'Maroleira', 'Arenque', 5.4, '5''4', 'Perfeito', 1, 'Com rocker de entrada baixo e volume generoso no bico, essa prancha gera velocidade em condições mínimas de surf, trazendo igualdade de condições para surfistas de qualquer nível de habilidade fazerem a cabeça no line up. \r\n\r\nDo meio para trás, em referência a radicalidade associada ao movimento anarquista, uma rabeta Bump Squash de performance, kick tail e um fundo single to double concave finalizando com v bottom, proporciona muita maleabilidade e manobras fortes.', 'active', 0),
(19, 'Torped', 15, 0, '', '', 5.8, '', '', 1, '', 'inactive', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surfboards_properties`
--

CREATE TABLE IF NOT EXISTS `tbl_surfboards_properties` (
  `sbp_id` int(11) NOT NULL AUTO_INCREMENT,
  `sbp_property` varchar(50) NOT NULL,
  `sbp_value` varchar(500) NOT NULL,
  `sbp_status` varchar(20) NOT NULL,
  `sur_id` int(11) NOT NULL,
  PRIMARY KEY (`sbp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=402 ;

--
-- Dumping data for table `tbl_surfboards_properties`
--

INSERT INTO `tbl_surfboards_properties` (`sbp_id`, `sbp_property`, `sbp_value`, `sbp_status`, `sur_id`) VALUES
(40, 'Roteta', 'Squash', 'active', 4),
(41, 'Legs', '3', 'active', 4),
(42, 'Material', 'Polyurethane', 'active', 4),
(49, 'Roteta', 'Squash', 'active', 5),
(50, 'Legs', '3', 'active', 5),
(51, 'Material', 'Polyurethane', 'active', 5),
(247, 'kola', 'Kala', 'active', 6),
(321, 'boards', '3', 'active', 12),
(322, 'working', 'fine', 'active', 12),
(323, 'do', 'any', 'active', 12),
(345, 'boards', '3', 'active', 9),
(346, 'working', 'fine', 'active', 9),
(347, 'do', 'any', 'active', 9),
(348, 'Roteta', 'Squash', 'active', 16),
(349, 'Legs', '3', 'active', 16),
(350, 'Material', 'Polyurethane', 'active', 16),
(360, 'Roteta', 'Squash', 'active', 13),
(361, 'Legs', '3', 'active', 13),
(362, 'Material', 'Polyurethane', 'active', 13),
(366, 'Roteta', 'Squash', 'active', 15),
(367, 'Legs', '3', 'active', 15),
(368, 'Material', 'Polyurethane', 'active', 15),
(369, 'boards', '3', 'active', 11),
(370, 'working', 'fine', 'active', 11),
(371, 'do', 'any', 'active', 11),
(375, 'Roteta', 'Squash', 'active', 3),
(376, 'Legs', '3', 'active', 3),
(377, 'Material', 'Polyurethane', 'active', 3),
(378, 'Roteta', 'Squash', 'active', 14),
(379, 'Legs', '3', 'active', 14),
(380, 'Material', 'Polyurethane', 'active', 14),
(381, 'boards', '3', 'active', 8),
(382, 'working', 'fine', 'active', 8),
(383, 'do', 'any', 'active', 8),
(393, 'boards', '3', 'active', 17),
(394, 'working', 'fine', 'active', 17),
(395, 'do', 'any', 'active', 17),
(399, 'boards', '3', 'active', 10),
(400, 'working', 'fine', 'active', 10),
(401, 'do', 'any', 'active', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sur_images`
--

CREATE TABLE IF NOT EXISTS `tbl_sur_images` (
  `sui_id` int(11) NOT NULL AUTO_INCREMENT,
  `sui_images` varchar(500) NOT NULL,
  `sur_id` int(11) NOT NULL,
  PRIMARY KEY (`sui_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `tbl_sur_images`
--

INSERT INTO `tbl_sur_images` (`sui_id`, `sui_images`, `sur_id`) VALUES
(25, '1551193774_powerlight-tp-plus-frente1.jpg', 3),
(28, '1551194111_Attack-SUP-Amarelo-Frente.jpg', 8),
(29, '1551194111_Attack-SUP-Amarelo-Frente.jpg', 9),
(31, '1551194219_FLAP-Minitank-6_6-frente.jpg', 9),
(33, '1551194270_FLAP-Kanguru-frente.jpg', 11),
(34, '1551194282_ZABO-Shark-Bomb-Frente2.jpg', 12),
(36, '1551361466_FLAP-Kanguru-frente.jpg', 16),
(39, '1551362396_Arenque-New-Dora-5_11-Frente.jpg', 13),
(40, '1551439686_alaia.jpg', 14),
(41, '1551439706_Powerlight-Triple-Wing-Frente2.jpg', 15),
(42, '1551439752_powerlight-tp-plus-frente1.jpg', 17),
(43, '1551449125_powerlight-tp-fast-frente.jpg', 8),
(44, '1551449125_powerlight-tp-plus-frente1.jpg', 8),
(45, '1551449125_Powerlight-Triple-Wing-Frente2.jpg', 8),
(47, '1551453682_FLAP-Minitank-6_6-frente.jpg', 10),
(49, '1554909493_surfboard shortboard arenque.jpg', 10),
(50, '1554909677_QUIVERDEPRANCHAS_MARESIAS-0251.jpg', 18),
(51, '1555548384_surf''sup_bds-1993.jpg', 19),
(52, '1555548384_surf''sup_bds-1998.jpg', 19),
(53, '1555548384_surf''sup_bds-2000.jpg', 19);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE IF NOT EXISTS `tbl_transaction` (
  `trx_id` int(11) NOT NULL AUTO_INCREMENT,
  `trx_amount` float NOT NULL,
  `trx_discount` float NOT NULL,
  `trx_coupon` varchar(50) NOT NULL,
  `trx_api_id` varchar(50) NOT NULL,
  `mem_id` int(11) NOT NULL,
  `pkg_id` int(11) NOT NULL,
  `trx_time` int(11) NOT NULL,
  `trx_card_name` varchar(500) NOT NULL,
  `trx_status` varchar(10) NOT NULL DEFAULT 'active',
  `trx_response` text NOT NULL,
  PRIMARY KEY (`trx_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`trx_id`, `trx_amount`, `trx_discount`, `trx_coupon`, `trx_api_id`, `mem_id`, `pkg_id`, `trx_time`, `trx_card_name`, `trx_status`, `trx_response`) VALUES
(1, 259, 0, 'na', '6356597', 5, 6, 1557954809, 'testing sarmad', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"962229","soft_descriptor":null,"tid":6356597,"nsu":6356597,"date_created":"2019-05-15T21:13:29.299Z","date_updated":"2019-05-15T21:13:29.663Z","amount":25900,"authorized_amount":25900,"paid_amount":25900,"refunded_amount":0,"installments":1,"id":6356597,"cost":50,"card_holder_name":"testing sarmad","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2068620,"external_id":"#surf1557954808boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"testing sarmad","email":"sarmad711@gmail.com","phone_numbers":["+3458341704"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-15T21:13:29.037Z","documents":[{"object":"document","id":"doc_cjvpq2g9j0coa1u6eh88ib0m2","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":987123,"name":"testing sarmad","address":{"object":"address","street":"House # 21 Street #38 Block Z new Satlitetown","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Sargodha","state":"41000000","zipcode":"41000000","country":"br","id":2266376}},"shipping":{"object":"shipping","id":867321,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266377}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvpq2gg70cob1u6egr4npkd1","date_created":"2019-05-15T21:13:29.288Z","date_updated":"2019-05-15T21:13:29.727Z","brand":"visa","holder_name":"testing sarmad","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"1223"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(2, 189, 0, 'na', '6356635', 105, 5, 1557954978, 'my plan', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"114300","soft_descriptor":null,"tid":6356635,"nsu":6356635,"date_created":"2019-05-15T21:16:17.697Z","date_updated":"2019-05-15T21:16:18.163Z","amount":18900,"authorized_amount":18900,"paid_amount":18900,"refunded_amount":0,"installments":1,"id":6356635,"cost":50,"card_holder_name":"my plan","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2068652,"external_id":"#surf1557954977boards","type":"individual","country":"pk","document_number":null,"document_type":"cpf","name":"my plan","email":"newuser@testing.sam","phone_numbers":["+3458341704"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-15T21:16:17.483Z","documents":[{"object":"document","id":"doc_cjvpq628r0d72cs6dd4z6vfmc","type":"passport","number":"4578945412156"}]},"billing":{"object":"billing","id":987125,"name":"my plan","address":{"object":"address","street":"N.S.T , Sargodha","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Sargodha","state":"40100","zipcode":"40100","country":"pk","id":2266381}},"shipping":{"object":"shipping","id":867322,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266382}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvpq62dd0d73cs6dp4g7nwvz","date_created":"2019-05-15T21:16:17.666Z","date_updated":"2019-05-15T21:16:18.250Z","brand":"visa","holder_name":"my plan","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"1022"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(3, 189, 0, 'na', '6356640', 105, 5, 1557955045, 'sam genius', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"355134","soft_descriptor":null,"tid":6356640,"nsu":6356640,"date_created":"2019-05-15T21:17:24.745Z","date_updated":"2019-05-15T21:17:25.247Z","amount":18900,"authorized_amount":18900,"paid_amount":18900,"refunded_amount":0,"installments":1,"id":6356640,"cost":50,"card_holder_name":"sam genius","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2068656,"external_id":"#surf1557955044boards","type":"individual","country":"pk","document_number":null,"document_type":"cpf","name":"sam genius","email":"newuser@testing.sam","phone_numbers":["+3458341704"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-15T21:17:24.682Z","documents":[{"object":"document","id":"doc_cjvpq7i370cpp1u6ekfy4abn0","type":"passport","number":"4578945412156"}]},"billing":{"object":"billing","id":987127,"name":"sam genius","address":{"object":"address","street":"N.S.T , Sargodha","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Sargodha","state":"40100","zipcode":"40100","country":"pk","id":2266385}},"shipping":{"object":"shipping","id":867323,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266386}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvpq7i4e0cpq1u6erm4rb7z9","date_created":"2019-05-15T21:17:24.735Z","date_updated":"2019-05-15T21:17:25.310Z","brand":"visa","holder_name":"sam genius","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0422"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(4, 189, 0, 'na', '6357997', 107, 5, 1558006172, 'Testing Card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"948855","soft_descriptor":null,"tid":6357997,"nsu":6357997,"date_created":"2019-05-16T11:29:32.493Z","date_updated":"2019-05-16T11:29:32.850Z","amount":18900,"authorized_amount":18900,"paid_amount":18900,"refunded_amount":0,"installments":1,"id":6357997,"cost":50,"card_holder_name":"Testing Card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2068933,"external_id":"#surf1558006172boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"Testing Card","email":"l1l2@gmail.com","phone_numbers":["+11939387311"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-16T11:29:32.209Z","documents":[{"object":"document","id":"doc_cjvqknccr0f2mcs6dg58nwrwb","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":987225,"name":"Testing Card","address":{"object":"address","street":"Oscar Freire 112","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"S","state":"01426000","zipcode":"01426000","country":"br","id":2266550}},"shipping":{"object":"shipping","id":867362,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266551}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvqkncjg0f2ncs6d1rmlm3wx","date_created":"2019-05-16T11:29:32.477Z","date_updated":"2019-05-16T11:29:32.930Z","brand":"visa","holder_name":"Testing Card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0422"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(5, 189, 0, 'na', '6357999', 106, 5, 1558006269, 'jah', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"720259","soft_descriptor":null,"tid":6357999,"nsu":6357999,"date_created":"2019-05-16T11:31:08.621Z","date_updated":"2019-05-16T11:31:09.046Z","amount":18900,"authorized_amount":18900,"paid_amount":18900,"refunded_amount":0,"installments":1,"id":6357999,"cost":50,"card_holder_name":"jah","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2068944,"external_id":"#surf1558006268boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"jah","email":"565656@gmail.com","phone_numbers":["+3227602530"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-16T11:31:08.427Z","documents":[{"object":"document","id":"doc_cjvqkpel60esf1u6e7757drfz","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":987226,"name":"jah","address":{"object":"address","street":"block no 33 house no 133, block no 33 house no 133","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"sao paulo","state":"01426000","zipcode":"01426000","country":"br","id":2266552}},"shipping":{"object":"shipping","id":867363,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266553}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvqkpepn0esg1u6eq1x5b6tc","date_created":"2019-05-16T11:31:08.604Z","date_updated":"2019-05-16T11:31:09.122Z","brand":"visa","holder_name":"jah","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0523"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(6, 189, 0, 'na', '6358009', 108, 5, 1558006472, 'testing card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"824462","soft_descriptor":null,"tid":6358009,"nsu":6358009,"date_created":"2019-05-16T11:34:31.994Z","date_updated":"2019-05-16T11:34:32.363Z","amount":18900,"authorized_amount":18900,"paid_amount":18900,"refunded_amount":0,"installments":1,"id":6358009,"cost":50,"card_holder_name":"testing card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2068967,"external_id":"#surf1558006471boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"testing card","email":"anyusertesting@gmail.com","phone_numbers":["+3458341704"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-16T11:34:31.855Z","documents":[{"object":"document","id":"doc_cjvqktrjv0et81u6el2s4pm6b","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":987228,"name":"testing card","address":{"object":"address","street":"N.S.T , Sargodha","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"sao paulo","state":"01426000","zipcode":"01426000","country":"br","id":2266555}},"shipping":{"object":"shipping","id":867364,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266556}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvqktrla0et91u6ebef45equ","date_created":"2019-05-16T11:34:31.919Z","date_updated":"2019-05-16T11:34:32.427Z","brand":"visa","holder_name":"testing card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0423"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(8, 259, 0, 'na', '6358031', 109, 6, 1558007255, 'jahanzaib khalid', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"757589","soft_descriptor":null,"tid":6358031,"nsu":6358031,"date_created":"2019-05-16T11:47:34.346Z","date_updated":"2019-05-16T11:47:34.777Z","amount":25900,"authorized_amount":25900,"paid_amount":25900,"refunded_amount":0,"installments":1,"id":6358031,"cost":50,"card_holder_name":"jahanzaib khalid","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2069043,"external_id":"#surf1558007253boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"jahanzaib khalid","email":"testtest123@gmail.com","phone_numbers":["+5656565656565"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-16T11:47:34.128Z","documents":[{"object":"document","id":"doc_cjvqlaj650f5bcy6d3so55n16","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":987232,"name":"jahanzaib khalid","address":{"object":"address","street":"asdasd","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"sao paulo","state":"01426000","zipcode":"01426000","country":"br","id":2266563}},"shipping":{"object":"shipping","id":867367,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266564}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvqlajat0f5ccy6dtvboq0h5","date_created":"2019-05-16T11:47:34.326Z","date_updated":"2019-05-16T11:47:34.845Z","brand":"visa","holder_name":"jahanzaib khalid","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0523"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(9, 189, 0, 'na', '6358053', 110, 5, 1558008106, 'testing sarmad', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"579329","soft_descriptor":null,"tid":6358053,"nsu":6358053,"date_created":"2019-05-16T12:01:45.665Z","date_updated":"2019-05-16T12:01:46.090Z","amount":18900,"authorized_amount":18900,"paid_amount":18900,"refunded_amount":0,"installments":1,"id":6358053,"cost":50,"card_holder_name":"testing sarmad","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2069110,"external_id":"#surf1558008105boards","type":"individual","country":"pk","document_number":null,"document_type":"cpf","name":"testing sarmad","email":"testing.sarmad@live.com","phone_numbers":["+3458341704"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-16T12:01:45.515Z","documents":[{"object":"document","id":"doc_cjvqlss3c0f4a206eg4ra11yx","type":"passport","number":"63435435435435"}]},"billing":{"object":"billing","id":987234,"name":"testing sarmad","address":{"object":"address","street":"House # 21 Street #38 Block Z new Satlitetown","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Sargodha","state":"40100","zipcode":"40100","country":"pk","id":2266567}},"shipping":{"object":"shipping","id":867368,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266568}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvqlss6t0f4b206evecko0x8","date_created":"2019-05-16T12:01:45.654Z","date_updated":"2019-05-16T12:01:46.157Z","brand":"visa","holder_name":"testing sarmad","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0428"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(10, 189, 0, 'na', '6358055', 110, 5, 1558008140, 'testing card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"212098","soft_descriptor":null,"tid":6358055,"nsu":6358055,"date_created":"2019-05-16T12:02:19.931Z","date_updated":"2019-05-16T12:02:20.377Z","amount":18900,"authorized_amount":18900,"paid_amount":18900,"refunded_amount":0,"installments":1,"id":6358055,"cost":50,"card_holder_name":"testing card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2069111,"external_id":"#surf1558008139boards","type":"individual","country":"pk","document_number":null,"document_type":"cpf","name":"testing card","email":"testing.sarmad@live.com","phone_numbers":["+3458341704"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-16T12:02:19.783Z","documents":[{"object":"document","id":"doc_cjvqltij90ewc1u6etk9maptf","type":"passport","number":"63435435435435"}]},"billing":{"object":"billing","id":987235,"name":"testing card","address":{"object":"address","street":"House # 21 Street #38 Block Z new Satlitetown","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Sargodha","state":"40100","zipcode":"40100","country":"pk","id":2266569}},"shipping":{"object":"shipping","id":867369,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266570}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvqltimn0ewd1u6e9toxquq6","date_created":"2019-05-16T12:02:19.919Z","date_updated":"2019-05-16T12:02:20.440Z","brand":"visa","holder_name":"testing card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0423"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(11, 259, 0, 'na', '6358056', 107, 6, 1558008148, 'Testing Card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"544929","soft_descriptor":null,"tid":6358056,"nsu":6358056,"date_created":"2019-05-16T12:02:28.312Z","date_updated":"2019-05-16T12:02:28.687Z","amount":25900,"authorized_amount":25900,"paid_amount":25900,"refunded_amount":0,"installments":1,"id":6358056,"cost":50,"card_holder_name":"Testing Card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2069112,"external_id":"#surf1558008147boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"Testing Card","email":"l1l2@gmail.com","phone_numbers":["+11939387311"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-16T12:02:28.097Z","documents":[{"object":"document","id":"doc_cjvqltoyj0f6qcy6d7nqkxaj3","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":987236,"name":"Testing Card","address":{"object":"address","street":"Oscar Freire 112","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"S","state":"01426000","zipcode":"01426000","country":"br","id":2266571}},"shipping":{"object":"shipping","id":867370,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266572}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvqltp3e0f6rcy6ddbqevobt","date_created":"2019-05-16T12:02:28.298Z","date_updated":"2019-05-16T12:02:28.755Z","brand":"visa","holder_name":"Testing Card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0522"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(12, 129, 0, 'na', '6358607', 111, 4, 1558017318, 'Testing Card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"820338","soft_descriptor":null,"tid":6358607,"nsu":6358607,"date_created":"2019-05-16T14:35:17.684Z","date_updated":"2019-05-16T14:35:18.072Z","amount":12900,"authorized_amount":12900,"paid_amount":12900,"refunded_amount":0,"installments":1,"id":6358607,"cost":50,"card_holder_name":"Testing Card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2069468,"external_id":"#surf1558017317boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"Testing Card","email":"11111@gmail.com","phone_numbers":["+11939387311"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-16T14:35:17.490Z","documents":[{"object":"document","id":"doc_cjvqra83k0fr9cy6dftsmc9cw","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":987312,"name":"Testing Card","address":{"object":"address","street":"Oscar Freire 112, 62","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"S\\u00e3o Paulo","state":"01426000","zipcode":"01426000","country":"br","id":2266745}},"shipping":{"object":"shipping","id":867379,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2266746}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvqra8850fracy6d6dr23mcm","date_created":"2019-05-16T14:35:17.670Z","date_updated":"2019-05-16T14:35:18.152Z","brand":"visa","holder_name":"Testing Card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0923"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(13, 189, 0, 'na', '6360135', 112, 5, 1558032918, 'Muhammad Sarmad', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"815463","soft_descriptor":null,"tid":6360135,"nsu":6360135,"date_created":"2019-05-16T18:55:18.090Z","date_updated":"2019-05-16T18:55:18.541Z","amount":18900,"authorized_amount":18900,"paid_amount":18900,"refunded_amount":0,"installments":1,"id":6360135,"cost":50,"card_holder_name":"Muhammad Sarmad","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2069993,"external_id":"#surf1558032917boards","type":"individual","country":"pk","document_number":null,"document_type":"cpf","name":"Muhammad Sarmad","email":"useruertesting@gmail.coms","phone_numbers":["+3458341704"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-16T18:55:17.851Z","documents":[{"object":"document","id":"doc_cjvr0klez0h7acy6dfe169b0w","type":"passport","number":"4324436546546545654"}]},"billing":{"object":"billing","id":987465,"name":"Muhammad Sarmad","address":{"object":"address","street":"House # 21 Street #38 Block Z new Satlitetown","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Sargodha","state":"40100","zipcode":"40100","country":"pk","id":2267124}},"shipping":{"object":"shipping","id":867410,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2267125}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvr0klkn0h7bcy6d32rk86th","date_created":"2019-05-16T18:55:18.072Z","date_updated":"2019-05-16T18:55:18.614Z","brand":"visa","holder_name":"Muhammad Sarmad","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"1224"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(14, 129, 0, 'na', '6360162', 112, 4, 1558033353, 'testing card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"250422","soft_descriptor":null,"tid":6360162,"nsu":6360162,"date_created":"2019-05-16T19:02:33.158Z","date_updated":"2019-05-16T19:02:33.676Z","amount":12900,"authorized_amount":12900,"paid_amount":12900,"refunded_amount":0,"installments":1,"id":6360162,"cost":50,"card_holder_name":"testing card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2070018,"external_id":"#surf1558033352boards","type":"individual","country":"pk","document_number":null,"document_type":"cpf","name":"testing card","email":"useruertesting@gmail.coms","phone_numbers":["+3458341704"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-16T19:02:33.008Z","documents":[{"object":"document","id":"doc_cjvr0tx6l0grccs6dh4wdbtzs","type":"passport","number":"4324436546546545654"}]},"billing":{"object":"billing","id":987475,"name":"testing card","address":{"object":"address","street":"House # 21 Street #38 Block Z new Satlitetown","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Sargodha","state":"40100","zipcode":"40100","country":"pk","id":2267139}},"shipping":{"object":"shipping","id":867413,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2267140}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvr0txa00grdcs6donbpvtp7","date_created":"2019-05-16T19:02:33.145Z","date_updated":"2019-05-16T19:02:33.749Z","brand":"visa","holder_name":"testing card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0422"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(15, 189, 0, 'na', '6362258', 113, 5, 1558086663, 'Testing Card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"931483","soft_descriptor":null,"tid":6362258,"nsu":6362258,"date_created":"2019-05-17T09:51:02.276Z","date_updated":"2019-05-17T09:51:02.804Z","amount":18900,"authorized_amount":18900,"paid_amount":18900,"refunded_amount":0,"installments":1,"id":6362258,"cost":50,"card_holder_name":"Testing Card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2070560,"external_id":"#surf1558086661boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"Testing Card","email":"1236@gmail.com","phone_numbers":["+11939387311"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-17T09:51:02.006Z","documents":[{"object":"document","id":"doc_cjvrwkio40j6k206ewp71tq5k","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":987605,"name":"Testing Card","address":{"object":"address","street":"Oscar Freire 112, 62","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"S\\u00e3o Paulo","state":"01426000","zipcode":"01426000","country":"br","id":2267412}},"shipping":{"object":"shipping","id":867437,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2267413}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvrwkiut0j6l206eq78gi6cd","date_created":"2019-05-17T09:51:02.262Z","date_updated":"2019-05-17T09:51:02.906Z","brand":"visa","holder_name":"Testing Card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0523"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(16, 259, 0, 'na', '6362261', 113, 6, 1558086779, 'Testing Card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"532273","soft_descriptor":null,"tid":6362261,"nsu":6362261,"date_created":"2019-05-17T09:52:58.506Z","date_updated":"2019-05-17T09:52:59.034Z","amount":25900,"authorized_amount":25900,"paid_amount":25900,"refunded_amount":0,"installments":1,"id":6362261,"cost":50,"card_holder_name":"Testing Card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2070561,"external_id":"#surf1558086778boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"Testing Card","email":"1236@gmail.com","phone_numbers":["+11939387311"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-17T09:52:58.332Z","documents":[{"object":"document","id":"doc_cjvrwn0fg0jhucy6doptia6df","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":987606,"name":"Testing Card","address":{"object":"address","street":"Oscar Freire 112, 62","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"S\\u00e3o Paulo","state":"01426000","zipcode":"01426000","country":"br","id":2267414}},"shipping":{"object":"shipping","id":867438,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2267415}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvrwn0jf0jhvcy6dzudlfu6u","date_created":"2019-05-17T09:52:58.491Z","date_updated":"2019-05-17T09:52:59.100Z","brand":"visa","holder_name":"Testing Card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0723"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(17, 318, 0, 'na', '6365644', 24, 1, 1558123152, 'Testing Card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"883911","soft_descriptor":null,"tid":6365644,"nsu":6365644,"date_created":"2019-05-17T19:59:11.672Z","date_updated":"2019-05-17T19:59:12.141Z","amount":31800,"authorized_amount":31800,"paid_amount":31800,"refunded_amount":0,"installments":1,"id":6365644,"cost":50,"card_holder_name":"Testing Card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2072792,"external_id":"#surf1558123151boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"Testing Card","email":"123456@gmail.com","phone_numbers":["+11939387311"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-17T19:59:11.409Z","documents":[{"object":"document","id":"doc_cjvsiam2r0ly6206e9dtf07pm","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":988229,"name":"Testing Card","address":{"object":"address","street":"Oscar Freire 112, 62","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"S\\u00e3o Paulo","state":"01426000","zipcode":"01426000","country":"br","id":2268474}},"shipping":{"object":"shipping","id":867675,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2268475}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvsiam970ly7206ef1r6fqzm","date_created":"2019-05-17T19:59:11.659Z","date_updated":"2019-05-17T19:59:12.209Z","brand":"visa","holder_name":"Testing Card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0423"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(18, 149, 0, 'na', '6378014', 98, 2, 1558460866, 'Testing card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"399407","soft_descriptor":null,"tid":6378014,"nsu":6378014,"date_created":"2019-05-21T17:47:45.768Z","date_updated":"2019-05-21T17:47:46.376Z","amount":14900,"authorized_amount":14900,"paid_amount":14900,"refunded_amount":0,"installments":1,"id":6378014,"cost":50,"card_holder_name":"Testing card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2076161,"external_id":"#surf1558460865boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"Testing card","email":"contato@surfsupclub.com","phone_numbers":["+11939387311"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-21T17:47:45.675Z","documents":[{"object":"document","id":"doc_cjvy3d02q04ocib6dsuqxfmq3","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":989779,"name":"Testing card","address":{"object":"address","street":"Rua Oscar Freire, 112","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"S\\u00e3o Paulo","state":"01426000","zipcode":"01426000","country":"br","id":2271447}},"shipping":{"object":"shipping","id":868496,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2271448}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvy3d04904odib6dbva7lhdw","date_created":"2019-05-21T17:47:45.753Z","date_updated":"2019-05-21T17:47:46.449Z","brand":"visa","holder_name":"Testing card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0423"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}');
INSERT INTO `tbl_transaction` (`trx_id`, `trx_amount`, `trx_discount`, `trx_coupon`, `trx_api_id`, `mem_id`, `pkg_id`, `trx_time`, `trx_card_name`, `trx_status`, `trx_response`) VALUES
(19, 318, 0, 'na', '6379849', 92, 1, 1558482236, 'Testing card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"335217","soft_descriptor":null,"tid":6379849,"nsu":6379849,"date_created":"2019-05-21T23:43:56.064Z","date_updated":"2019-05-21T23:43:56.491Z","amount":31800,"authorized_amount":31800,"paid_amount":31800,"refunded_amount":0,"installments":1,"id":6379849,"cost":50,"card_holder_name":"Testing card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2076825,"external_id":"#surf1558482235boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"Testing card","email":"lucas420@gmail.com","phone_numbers":["+11939387311"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-21T23:43:55.899Z","documents":[{"object":"document","id":"doc_cjvyg31ft06siic6da3spann4","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":990023,"name":"Testing card","address":{"object":"address","street":"Rua Oscar Freire, 112","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"S\\u00e3o Paulo","state":"01426000","zipcode":"01426000","country":"br","id":2271874}},"shipping":{"object":"shipping","id":868570,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2271875}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvyg31ji06sjic6dqqxmi33w","date_created":"2019-05-21T23:43:56.047Z","date_updated":"2019-05-21T23:43:56.556Z","brand":"visa","holder_name":"Testing card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0323"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(20, 149, 0, 'na', '6382718', 24, 2, 1558551833, 'Testing Card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"953118","soft_descriptor":null,"tid":6382718,"nsu":6382718,"date_created":"2019-05-22T19:03:53.271Z","date_updated":"2019-05-22T19:03:53.786Z","amount":14900,"authorized_amount":14900,"paid_amount":14900,"refunded_amount":0,"installments":1,"id":6382718,"cost":50,"card_holder_name":"Testing Card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2077716,"external_id":"#surf1558551832boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"Testing Card","email":"123456@gmail.com","phone_numbers":["+11939387311"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-22T19:03:53.126Z","documents":[{"object":"document","id":"doc_cjvzlir0d09yaib6dwf90ojd6","type":"cpf","number":"42616915820"}]},"billing":{"object":"billing","id":990513,"name":"Testing Card","address":{"object":"address","street":"Oscar Freire 112, 62","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"S\\u00e3o Paulo","state":"01426000","zipcode":"01426000","country":"br","id":2272712}},"shipping":{"object":"shipping","id":868773,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2272713}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjvzlir3b09ybib6dodze88yx","date_created":"2019-05-22T19:03:53.255Z","date_updated":"2019-05-22T19:03:53.856Z","brand":"visa","holder_name":"Testing Card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0520"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}'),
(21, 318, 0, 'na', '6394242', 116, 1, 1558719896, 'Restinga Card', 'active', '{"object":"transaction","status":"paid","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":"0000","acquirer_name":"pagarme","acquirer_id":"5c9cea5062a07424715753bf","authorization_code":"148257","soft_descriptor":null,"tid":6394242,"nsu":6394242,"date_created":"2019-05-24T17:44:55.753Z","date_updated":"2019-05-24T17:44:56.087Z","amount":31800,"authorized_amount":31800,"paid_amount":31800,"refunded_amount":0,"installments":1,"id":6394242,"cost":50,"card_holder_name":"Restinga Card","card_last_digits":"4242","card_first_digits":"424242","card_brand":"visa","card_pin_mode":null,"card_magstripe_fallback":false,"postback_url":null,"payment_method":"credit_card","capture_method":"ecommerce","antifraud_score":null,"boleto_url":null,"boleto_barcode":null,"boleto_expiration_date":null,"referer":"api_key","ip":"198.38.82.246","subscription_id":null,"phone":null,"address":null,"customer":{"object":"customer","id":2081191,"external_id":"#surf1558719895boards","type":"individual","country":"br","document_number":null,"document_type":"cpf","name":"Restinga Card","email":"raphaelwright@gmail.com","phone_numbers":["+11981811428"],"born_at":null,"birthday":"1970-01-01","gender":null,"date_created":"2019-05-24T17:44:55.667Z","documents":[{"object":"document","id":"doc_cjw2dkww2022dpz6d9yezpjd0","type":"cpf","number":"31027852807"}]},"billing":{"object":"billing","id":991479,"name":"Restinga Card","address":{"object":"address","street":"Av Diogenes Ribeiro de Lima, 2361","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"S\\u00e3o Paulo ","state":"05458001","zipcode":"05458001","country":"br","id":2274965}},"shipping":{"object":"shipping","id":869160,"name":"Neo Reeves","fee":0,"delivery_date":"2000-12-21","expedited":true,"address":{"object":"address","street":"Rua Matrix","complementary":null,"street_number":"9999","neighborhood":"Rio Cotia","city":"Cotia","state":"sp","zipcode":"06714360","country":"br","id":2274966}},"items":[{"object":"item","id":"pkg001","title":"pkgSurfboard","unit_price":0,"quantity":1,"category":null,"tangible":true,"venue":null,"date":null}],"card":{"object":"card","id":"card_cjw2dkwxd022epz6dtnxprwsd","date_created":"2019-05-24T17:44:55.729Z","date_updated":"2019-05-24T17:44:56.153Z","brand":"visa","holder_name":"Restinga Card","first_digits":"424242","last_digits":"4242","country":"UNITED STATES","fingerprint":"cj7t2pcew03tn0l09n98ibta0","valid":true,"expiration_date":"0220"},"split_rules":null,"metadata":{},"antifraud_metadata":{},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":false,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null}');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `usr_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_email` varchar(100) NOT NULL,
  `usr_password` varchar(200) NOT NULL,
  `usr_type` varchar(20) NOT NULL DEFAULT 'user',
  `lcc_id` int(11) NOT NULL,
  `usr_status` varchar(20) NOT NULL,
  PRIMARY KEY (`usr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`usr_id`, `usr_email`, `usr_password`, `usr_type`, `lcc_id`, `usr_status`) VALUES
(0, 'admin@gmail.com', '9976c6a16d7e31b0f9a013356bc128da61d00a74', 'Administrator', 6, 'active'),
(4, 'location@gmail.com', 'e519543997e2b363a8a534abf4c1df3d5b5808c3', 'user', 32, 'active'),
(5, '123456@gmail.com', '5489218533b547c86228bb57219e5d5ac42c3b11', 'user', 6, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_website`
--

CREATE TABLE IF NOT EXISTS `tbl_website` (
  `site_meta_desc` varchar(500) NOT NULL,
  `site_meta_keyword` varchar(500) NOT NULL,
  `site_meta_copyright` varchar(500) NOT NULL,
  `site_meta_author` varchar(500) NOT NULL,
  `site_facebook` varchar(500) NOT NULL,
  `site_twitter` varchar(500) NOT NULL,
  `site_instagram` varchar(500) NOT NULL,
  `site_youtube` varchar(500) NOT NULL,
  `site_domain` varchar(500) NOT NULL,
  `site_name` varchar(500) NOT NULL,
  `site_email` varchar(500) NOT NULL,
  `site_noreply_email` varchar(500) NOT NULL,
  `site_phone` varchar(500) NOT NULL,
  `site_address` varchar(500) NOT NULL,
  `footer_text` text NOT NULL,
  `email_subject` text NOT NULL,
  `email_template` text NOT NULL,
  `web_header` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_website`
--

INSERT INTO `tbl_website` (`site_meta_desc`, `site_meta_keyword`, `site_meta_copyright`, `site_meta_author`, `site_facebook`, `site_twitter`, `site_instagram`, `site_youtube`, `site_domain`, `site_name`, `site_email`, `site_noreply_email`, `site_phone`, `site_address`, `footer_text`, `email_subject`, `email_template`, `web_header`) VALUES
('New Admin', 'HTML, CSS, XML, JavaScript', 'New Admin © 2018 All Rights Reserved.', 'Administration', 'https://www.facebook.com/SURF''S UP CLUB', 'https://twitter.com/SURF''S UP CLUB', 'https://instagram.com/SURF''S UP CLUB', 'https://www.youtube.com/SURF''S UP CLUB', 'surfboard.com', 'SURF''S UP CLUB', 'contato@surfsupclub.com', 'contato@surfsupclub.com', '+55(11)93938-7311', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.0920490782787!2d-45.626935385378374!3d-23.779736275577136!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94d27fbdf760f367%3A0x84346b1d8330908c!2sR.+Maganes%2C+868+-+Praia+de+Boi%C3%A7ucanga%2C+S%C3%A3o+Sebasti%C3%A3o+-+SP%2C+11600-000!5e0!3m2!1spt-BR!2sbr!4v1555429335998!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'The world has become so fast paced that people don''t want to stand by reading a page of information, they would much rather look at a presentation and understand a message. It has come to a point where images.', 'Subject here', '<p>Hi {firstname} {lastname},</p>\r\n\r\n<p>Thanks for signup , Please click the link below to activate your account.</p>\r\n\r\n<p>{link}</p>\r\n\r\n<p>~Surf&#39;s Up Team.</p>\r\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_web_setting`
--

CREATE TABLE IF NOT EXISTS `tbl_web_setting` (
  `web_id` int(11) NOT NULL AUTO_INCREMENT,
  `web_page` varchar(50) NOT NULL,
  `web_setting` text NOT NULL,
  PRIMARY KEY (`web_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_web_setting`
--

INSERT INTO `tbl_web_setting` (`web_id`, `web_page`, `web_setting`) VALUES
(1, 'home', '{"first_image":"4948b2253e83042f270b5895de923a21.svg","second_image":"49f593f913a5c13fae7733563fad90a2.svg","third_image":"83f48f31d96d4cd4ad2bdec33a525dcf.svg","why_image1":"171c936c5c7874edb1399f68c42b903b.svg","why_image2":"362147f874dc329f0e13415a766b1f0b.svg","why_image3":"0dd21130275a023d6dde93045abca6c7.svg","about_us_banner":"b6f26206343add66e918ccd5b640526e.jpg","why_join_banner":"f9517cfc926da4c5e56c7d640a342043.jpg","surf_board_detail_banner":"bb95ba1250a032dabc994763210b8c5c.jpg","headin_top_text":"Fa\\u00e7a parte do primeiro","headin_top_color":"#ffffff","headin_top_size":"","headin_top_family":"","headin_top_letter_space":"0.1","main_heading":"Clube de Pranchas do Brasil","main_color":"#ffffff","main_size":"","main_family":"","main_family_space":"","main_heading_sologan":"e eleve ao m\\u00e1ximo sua experi\\u00eancia no surfe!","heading_sologan_color":"#ffffff","heading_sologan_size":"12","heading_sologan_family":"","main_sologan_space":"","how_heading":"COMO FUNCIONA","how_detail":"Com um dos nossos planos, o cliente tem direito a","how_text1":"Surfar com qualquer prancha dispon\\u00edvel no clube","how_text2":"Ficar com a mesma prancha por v\\u00e1rios dias","how_text3":"Seguro incluso ao bem para danos ocorridos na pr\\u00e1tica do surfe","why_heading":"POR QUE SER S\\u00d3CIO","why_description":"Mais flexibilidade, mais liberdade, mais surf!","why_heading1":"Surf it all","why_text1":"Surfar com qualquer prancha dispon\\u00edvel no clube","why_heading2":"Demo","why_text2":"Teste antes de escolher qual prancha vai comprargfdgfds","why_heading3":"Flexibilidade","why_text3":"Deixe sua prancha em casa e passe no nosso clube","middle_banner_heading":"SURFE ESSA ONDA","middle_banner_description":"Experimente diferentes modelos, tamanhos, marcas e tecnologias de prancha de surfe por um pre\\u00e7o \\u00fanico."}'),
(2, 'about', '{"people_picture2":"9d7c3e85100723818a75786c4e8aab16.jpg","people_picture1":"e9aeb33000a48327a56ec9bcd6e21c56.jpg","sub_picture1":"8144c27fdeca1beb0e21bcbfb5cfee85.svg","sub_picture2":"1399e279377311964c47f9f744648589.svg","sub_picture3":"f870dd05538fe22395118ca5927c850f.svg","people_description1":"Xeik \\u00e9 zica","top_main_heading":"Quem somos","top_main_sub_heading":"Somos o primeiro Clube de Pranchas do Brasil","main_heading":"Nossa miss\\u00e3o","main_heading_sologan":"Provemos liberdade para que o surfista tenha novas experi\\u00eancias no mar, surfando com diferentes modelos, tamanhos, marcas e tecnologias de pranchas de surfe.\\r\\n\\r\\nQueremos que nosso cliente se sinta como uma crian\\u00e7a em uma loja de doces, surfando com a prancha que quiser sem medo de ser feliz.\\r\\nCom a nossa plataforma digital, criamos um enorme laborat\\u00f3rio de pranchas de surfe, o ponto de encontro entre o shaper e o surfista. Enquanto os shapers se encarregam de refinar os seus modelos, experimentar novas f\\u00f3mulas e combina\\u00e7\\u00f5es de tecnologias, os nossos associados se encarregam de surfar e avaliar cada prancha, em cada sess\\u00e3o de surfe.\\r\\n","how_heading":"Nossa Solu\\u00e7\\u00e3o","how_detail":"Atrav\\u00e9s de parcerias com grandes shapers e estabelecimentos localizados nos principais picos de surfe oferecemos","how_sub_heading":"Qualidade","how_sub_detail":"Todas nossas pranchas passam por um processo de sele\\u00e7\\u00e3o rigoroso para que possamos sempre entregar as melhores pranchas do mercado para os nossos clientes","how_sub_heading1":"Variedade","how_sub_detail1":"Temos pranchas de todos os tipos, para todos os surfistas. Do longboard \\u00e0s pranchinhas, da alaia \\u00e0s fishes, do funboard ao SUP. Temos pranchas para todos os gostos, surfistas e condi\\u00e7\\u00f5es de onda","how_sub_heading2":"Acessibilidade","how_sub_detail2":"Estamos localizados nos principais picos de surfe, para que voc\\u00ea tenha a comodidade de buscar a prancha no quiver mais pr\\u00f3ximo a sua pr\\u00f3xima queda","abovePeopleText":"A onda nunca acaba: Se voc\\u00ea vai pegar um swell gigante ou precisa de um pranch\\u00e3o para ensinar um amigo a surfar, temos as melhores pranchas do mercado ideais para quaisquer condi\\u00e7\\u00f5es de onda acess\\u00edveis nos melhores picos de surfe do Brasil.\\r\\n\\r\\n","top_people_heading":"THE PEOPLE BEHIND SURF''S UP","people_name1":"Lucas Sinisgalli","people_designtion1":"CEO & Co-Fundador","people_name2":"Daniel Cappellette","people_designtion2":"CMO & Co-Fundador","people_description2":"Madelynne is the co-founder and General Manager Officer at Surf''s up Club. Madelynne plays a leading role in driving key strategic initiatives across the global business. Previously she oversaw the creation of Surf''s up Club engineering, data science, and performance marketing teams. Madelynne became an entrepreneur in his youth, running a business whilehe was in high school that sold to clients in more than 20 countries. She earned a degree in Computer Science from Harvard University and held several engineering positions before co-founding Surf''s up Club. As a guest, Madelynne has stayed in hundreds of homes using Surf''s up Club and she is also a host in San Francisco, where she lives with his family.","contact_heading":"Fale Conosco","contact_description":"If you have any questions or consultation, please reach us at surfsclub@gmail.com. We will provide the best requested information as soon as possible.\\r\\nSe voc\\u00ea tem qualquer d\\u00favida ou sugest\\u00e3o, contate-nos pelo email contato@surfsupclub.com ou atrav\\u00e9s dos n\\u00fameros: (11) 93938-7311 \\/ (11) 99981-4191. Iremos te atender o mais r\\u00e1pido poss\\u00edvel com nosso atendimento personalizado."}'),
(3, 'how', '{"headin_top_text":"fdsf","main_heading":"dfdsf","main_heading_sologan":"dsfdasfd","how_heading":"sfdsf","how_detail":"dsfdasfd","how_text1":"fdafsf","how_text2":"dasfas","how_text3":"fdsf","why_heading":"fdsf","why_description":"fdafdas","middle_banner_heading":"fdaf","middle_banner_description":"fdsafds","first_image":"04e8fe9adb0a086f58c80a036d304cbb.JPG","second_image":"cefd18def31d0f310bb5b94b5e02e56c.png","third_image":"0eab5772a37b4d98bcbff0155b3a9866.JPG","why_image1":"d27fe0e7a5a4deafe53c65ff2d9aecf0.JPG","why_image2":"b23bc761ed5587ca8ba8285464a6f42e.png","why_image3":"3d188f5f2385c62cb6934f0ffb74e327.png","why_picture1":"977433a455b20f0b230640eccabc6ad9.svg","why_picture2":"8772bb4ec29c08283d8e68f14f940761.svg","picture1":"4ed3670b7525ebc274bd2b5bd2501130.svg","why_picture3":"0a6caab333bc9db9a012c30442f20609.svg","why_picture4":"085a08474e47d282e7d7654110b61550.svg","picture2":"08a2de5ca2f640db9ee79b4f46e86921.svg","picture3":"aab481014e67c72dea7148cff7440ba3.svg","why_text1":"Cadastre-se com email, telefone e senha.","top_main_heading":"COMO FUNCIONA","top_main_sub_heading":"Com um dos nossos planos, o cliente tem direito a:","howItMainHeading1":"Como funciona","text1":"Surfar com qualquer prancha dispon\\u00edvel no clube","text2":"Ficar com a mesma prancha por v\\u00e1rios dias","text3":"Seguro incluso para danos ocorridos na pr\\u00e1tica do surfe","howItMainHeading2":"Como funciona","why_heading1":"Fa\\u00e7a seu cadastro","why_heading2":"Escolha seu plano e realze o pagamento","why_text2":"Escolha o plano que mais combina com o seu perfil e realize o pagamento pelo cart\\u00e3o de cr\\u00e9dito ou boleto.","why_heading3":"Reserve sua prancha online","why_text3":"Escolha e reserve sua prancha em nosso site, filtrando por localiza\\u00e7\\u00e3o e\\/ou filtros avan\\u00e7ados.","why_heading4":"GO SURF NOW!","why_text4":"Com o seu c\\u00f3digo de reserva, retire a prancha no estabelecimento parceiro e GO SURF NOW!!!","contact_heading":"Fale Conosco","contact_description":"D\\u00favidas? Entre em contato conosco para resolver todas as suas d\\u00favidas"}'),
(4, 'tos', '{"tos_heading":"TERMOS DE USO \\u2013 PLATAFORMA SURF\\u2019S UP","tos_detail":"<p>TERMOS DE USO &ndash; PLATAFORMA SURF&rsquo;S UP<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Estes Termos de Uso da Surf&rsquo;s Up (&ldquo;Termos de Uso&rdquo; ou &ldquo;Termos&rdquo;) s&atilde;o celebrados entre voc&ecirc;, o usu&aacute;rio (doravante &ldquo;Usu&aacute;rio&rdquo;, &ldquo;S&oacute;cio&rdquo;, &ldquo;voc&ecirc;&rdquo;, &ldquo;seu&rdquo;, &ldquo;seus&rdquo;, &ldquo;sua&rdquo;, &ldquo;suas&rdquo;), e LUCAS GON&Ccedil;ALVES SINISGALLI 42616915820, empres&aacute;rio individual com sede na Cidade e Estado de S&atilde;o Paulo, na Rua Oscar Freire, n&ordm; 112, comp. 62, no bairro Cerqueira Cesar, CEP 01426-000, devidamente inscrito no CNPJ\\/MF sob o n&ordm; 30.145.560\\/0001-03 (doravante &ldquo;Surf&rsquo;s Up&rdquo;, &ldquo;n&oacute;s&rdquo;, &ldquo;nosso&rdquo;, &ldquo;nossos&rdquo;, &ldquo;nossa&rdquo;, &ldquo;nossas&rdquo;) e neste ato representado na forma de seus atos constitutivos.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>1. INTRODU&Ccedil;&Atilde;O<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>1.1. A Surf&rsquo;s Up &eacute; empresa pioneira no sistema de compartilhamento de quivers no Brasil, disponibilizando aos seus clientes o acesso facilitado a uma linha exclusiva de pranchas de surfe, leashs, decks, quilhas e capas (doravante &ldquo;Produtos&rdquo;).<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>1.2. Os presentes Termos de Uso regem a rela&ccedil;&atilde;o entre o Usu&aacute;rio e a Plataforma Digital de aluguel das pranchas de surfe (&ldquo;Plataforma&rdquo;), de titularidade da Surf&rsquo;s Up, disponibilizada em nosso site: https:\\/\\/surfsupclub.com (doravante &ldquo;Site&rdquo;). Desde j&aacute;, voc&ecirc; reconhece e declara que, previamente &agrave; utiliza&ccedil;&atilde;o da Plataforma, leu, entendeu e concordou com todo o conte&uacute;do deste documento e com a Pol&iacute;tica de Privacidade que integra estes Termos de Uso.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>1.3. As disposi&ccedil;&otilde;es dos Termos de Uso dever&atilde;o ser interpretadas de acordo com as Leis da Rep&uacute;blica Federativa do Brasil.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>1.4. Estes Termos de Uso s&atilde;o aplic&aacute;veis a todos e quaisquer Usu&aacute;rios que utilizarem os servi&ccedil;os providos pela Surf&rsquo;s Up, via Plataforma, por meio do acesso ao Site, em todo o territ&oacute;rio brasileiro.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>1.5. Ainda, o Usu&aacute;rio compreende que a Plataforma se encontra em cont&iacute;nuo aprimoramento, assim como o conte&uacute;do dos presentes Termos, de modo a melhor atender &agrave;s suas necessidades. Dessa forma, a Surf&rsquo;s Up se reserva o direito de, a seu &uacute;nico e exclusivo crit&eacute;rio, alterar as condi&ccedil;&otilde;es previstas nestes Termos de Uso. Quando forem feitas altera&ccedil;&otilde;es nestes Termos, informaremos voc&ecirc; por meio de aviso dentro da Plataforma e\\/ou Site ou por meio de comunica&ccedil;&atilde;o via e-mail. Nestes casos, o uso cont&iacute;nuo de nossos servi&ccedil;os ficar&aacute; condicionado &agrave; aceita&ccedil;&atilde;o da vers&atilde;o mais recente dos presentes Termos. Portanto, &eacute; imprescind&iacute;vel que o Usu&aacute;rio esteja atento a avisos neste sentido.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>1.6. Caso o Usu&aacute;rio n&atilde;o deseje continuar utilizando os servi&ccedil;os da Surf&rsquo;s Up em raz&atilde;o da nova vers&atilde;o destes Termos de Uso ou por qualquer outro motivo, a seu crit&eacute;rio, o mesmo poder&aacute;, desde que n&atilde;o esteja inadimplente com rela&ccedil;&atilde;o &agrave;s obriga&ccedil;&otilde;es aqui previstas, encerrar seu cadastro e\\/ou cancelar os Planos adquiridos por meio do acesso &agrave; Plataforma.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>2. CONCEITOS E DEFINI&Ccedil;&Otilde;ES:<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&middot; Conta: Espa&ccedil;o virtual de acesso exclusivo do S&oacute;cio dentro da Plataforma ap&oacute;s autentica&ccedil;&atilde;o por meio de login e senha, previamente cadastrados no Site;<\\/p>\\r\\n\\r\\n<p>&middot; Esta&ccedil;&atilde;o\\/Clube: Locais em que se encontram os Quivers com as Pranchas e demais equipamentos (leashs, capas e quilhas) para utiliza&ccedil;&atilde;o dos Usu&aacute;rios;<\\/p>\\r\\n\\r\\n<p>&middot; Login: Credencial previamente cadastrada e utilizada para identifica&ccedil;&atilde;o e autentica&ccedil;&atilde;o do Usu&aacute;rio na Plataforma;<\\/p>\\r\\n\\r\\n<p>&middot; Plataforma: &Aacute;rea de acesso disponibilizada no pr&oacute;prio site da Surf&rsquo;s Up com a finalidade de utilizar as funcionalidades a ela inerentes;<\\/p>\\r\\n\\r\\n<p>&middot; Pranchas: Todas as pranchas de surfe integrantes dos Quivers;<\\/p>\\r\\n\\r\\n<p>&middot; Produtos: Pranchas, leashs, decks, quilhas e capas referidos em conjunto;<\\/p>\\r\\n\\r\\n<p>&middot; Plano\\/Assinatura Mensal: Plano adquirido pelo Usu&aacute;rio com prazo de validade com acesso ilimitado aos Produtos, por per&iacute;odo indeterminado e com pagamento fixo mensal, conforme pormenorizadamente descrito no Site;<\\/p>\\r\\n\\r\\n<p>. Plano\\/Assinatura Anual: Plano adquirido pelo Usu&aacute;rio com prazo de validade de 01 (um) ano, com op&ccedil;&otilde;es de parcelamento de at&eacute; 12 (doze) vezes.<\\/p>\\r\\n\\r\\n<p>&middot; Planos Avulsos: Plano adquirido pelo Usu&aacute;rio aos servi&ccedil;os disponibilizados pela Surf&rsquo;s Up, com acesso ilimitado aos Produtos pelo per&iacute;odo de 01 (um), 03 (tr&ecirc;s), 05 (cinco) e 10 (dez) dias, de forma consecutiva ou n&atilde;o, conforme pormenorizadamente descrito no Site.<\\/p>\\r\\n\\r\\n<p>&middot; Planos: Planos Avulsos, Plano Mensal e o Plano Anual referidos em conjunto, indistintamente;<\\/p>\\r\\n\\r\\n<p>&middot; Quiver(s): Conjunto\\/cole&ccedil;&atilde;o de pranchas de surfe disponibilizadas pela Surf&rsquo;s Up;<\\/p>\\r\\n\\r\\n<p>&middot; Site: Endere&ccedil;o eletr&ocirc;nico da Surf&rsquo;s Up em que s&atilde;o disponibilizadas diversas informa&ccedil;&otilde;es para o Usu&aacute;rio e para os Visitantes que o acessam;<\\/p>\\r\\n\\r\\n<p>&middot; Usu&aacute;rio: Pessoas f&iacute;sicas maiores de 18 (dezoito) anos ou que tenham capacidade plena e que concordem com os presentes Termos de Uso e com a Pol&iacute;tica de Privacidade, j&aacute; cadastradas e que realizam o acesso ao Site mediante senha pessoal e identifica&ccedil;&atilde;o (Login). No caso de pessoas f&iacute;sicas absoluta ou relativamente incapazes, estas devem estar devidamente acompanhadas por seus pais, respons&aacute;veis ou representantes legais, em car&aacute;ter de representa&ccedil;&atilde;o ou assist&ecirc;ncia, conforme o caso, a fim de tornar v&aacute;lida a aceita&ccedil;&atilde;o e concord&acirc;ncia dos presentes Termos de Uso e Pol&iacute;tica de Privacidade, nos termos da legisla&ccedil;&atilde;o aplic&aacute;vel;<\\/p>\\r\\n\\r\\n<p>&middot; Visitantes: Pessoas que simplesmente naveguem pelo Site sem possuir Login de Usu&aacute;rio e senha.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>2.1. Os termos definidos acima poder&atilde;o ser grafados no plural ou no singular, de acordo com o contexto em que se encaixarem, sem que de qualquer forma seja alterado o conceito e\\/ou a defini&ccedil;&atilde;o que ora lhes &eacute; conferido.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>3. POL&Iacute;TICA DE PRIVACIDADE<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>3.1. A Pol&iacute;tica de Privacidade da Surf&rsquo;s Up est&aacute; incorporada a estes Termos de Uso e encontra-se dispon&iacute;vel no nosso Site. Ao continuar utilizando nossos servi&ccedil;os, voc&ecirc; concorda em permanecer vinculado a eles.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>4. CADASTRO NA PLATAFORMA<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>4.1. Para que seja poss&iacute;vel a utiliza&ccedil;&atilde;o da Plataforma com o intuito de utiliza&ccedil;&atilde;o dos servi&ccedil;os e\\/ou Produtos da Surf&rsquo;s Up, voc&ecirc; dever&aacute; fornecer os dados solicitados no formul&aacute;rio de cadastramento, de forma verdadeira e completa, bem como apresentar<\\/p>\\r\\n\\r\\n<p>todos os documentos solicitados, responsabilizando-se sempre pela veracidade das informa&ccedil;&otilde;es fornecidas e pela atualiza&ccedil;&atilde;o dos dados de cadastro no Site.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>4.2. Para concluir o procedimento de cadastro, voc&ecirc; dever&aacute; criar uma senha pessoal e intransfer&iacute;vel para futuros acessos &agrave; Plataforma. Como medida de seguran&ccedil;a, voc&ecirc; deve guardar sua senha e mant&ecirc;-la em sigilo, n&atilde;o a compartilhando com terceiros.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>4.3. A Surf&rsquo;s Up se isenta de qualquer responsabilidade decorrente da utiliza&ccedil;&atilde;o n&atilde;o autorizada do seu Login e\\/ou senha por terceiros. O Usu&aacute;rio concorda em comunicar imediatamente a Surf&rsquo;s Up no caso de uso n&atilde;o autorizado de seu Login e senha ou quebra de seguran&ccedil;a a que venha a ter ci&ecirc;ncia.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>4.4. Ap&oacute;s a etapa de cadastramento, o Usu&aacute;rio poder&aacute;, por meio da Plataforma: atualizar seus dados pessoais; adquirir os Planos para utiliza&ccedil;&atilde;o dos Produtos; realizar a reserva dos Produtos; consultar relat&oacute;rio de retiradas de Produtos; consultar a localiza&ccedil;&atilde;o das Esta&ccedil;&otilde;es; consultar o tempo de loca&ccedil;&atilde;o dos Produtos; consultar a disponibilidade de Pranchas em cada uma das Esta&ccedil;&otilde;es; cancelar e\\/ou alterar os Planos contratados, dentre outras funcionalidades.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>4.5. Desde j&aacute;, o Usu&aacute;rio concorda e est&aacute; ciente de que, caso a Surf&rsquo;s Up n&atilde;o consiga fazer a valida&ccedil;&atilde;o dos dados informados no processo de cadastramento na Plataforma, todas as etapas do cadastro estar&atilde;o prejudicadas e o mesmo n&atilde;o poder&aacute; ser conclu&iacute;do.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>5. DA UTILIZA&Ccedil;&Atilde;O DA PLATAFORMA.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>5.1. Para aquisi&ccedil;&atilde;o dos Planos, o Usu&aacute;rio poder&aacute;:<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>a) Adicionar os dados de um cart&atilde;o de cr&eacute;dito de sua titularidade em seu perfil na Plataforma, no qual ser&atilde;o efetuadas as cobran&ccedil;as decorrentes da utiliza&ccedil;&atilde;o dos servi&ccedil;os da Surf&rsquo;s Up. Para isso, voc&ecirc; garante estar informando os dados de um cart&atilde;o de cr&eacute;dito v&aacute;lido e que est&aacute; em seu nome, bem como autoriza que a Surf&rsquo;s Up efetue as cobran&ccedil;as na forma e nos casos aqui previstos, diretamente no referido cart&atilde;o de cr&eacute;dito; ou,<\\/p>\\r\\n\\r\\n<p>b) Gerar e realizar o pagamento do boleto banc&aacute;rio referente ao Plano que desejar adquirir, por meio de sua conta pessoal na Plataforma.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>5.2. O Usu&aacute;rio poder&aacute; retirar e utilizar as Pranchas ap&oacute;s efetuar a reserva via Plataforma, pelo Site, mediante apresenta&ccedil;&atilde;o do c&oacute;digo de reserva diretamente nas Esta&ccedil;&otilde;es.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>5.3. O c&oacute;digo informado para o Usu&aacute;rio no momento da reserva via Plataforma, por meio do Site, &eacute; pessoal e intransfer&iacute;vel. Seu uso &eacute; monitorado e caso seja compartilhado com terceiros ou utilizado de maneira que viole os presentes Termos de Uso, a Surf&rsquo;s Up se reserva o direito de suspender e\\/ou excluir permanentemente o cadastro do Usu&aacute;rio envolvido, sem preju&iacute;zo de outras medidas cab&iacute;veis na esfera extrajudicial ou judicial (c&iacute;vel e\\/ou criminal). Para fins de controle, o c&oacute;digo de reserva s&oacute; ser&aacute; emitido ap&oacute;s confirmada a compensa&ccedil;&atilde;o banc&aacute;ria pelo sistema da Plataforma, independente da forma de pagamento.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>5.4. A Plataforma est&aacute; dispon&iacute;vel para acesso todos os dias da semana, por&eacute;m, os dias e hor&aacute;rios dispon&iacute;veis para voc&ecirc; realizar a retirada e devolu&ccedil;&atilde;o de seus Produtos poder&aacute; variar de acordo com cada Esta&ccedil;&atilde;o. Dessa forma, certifique-se de confirmar os dias e hor&aacute;rios de funcionamento de nossos Clubes parceiros, conforme informado em nosso Site.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>5.4.1. Caso a reserva realizada por voc&ecirc; tenha vencimento em data que n&atilde;o houver funcionamento no Clube respons&aacute;vel pelo recebimento dos Produtos, voc&ecirc; dever&aacute; realizar a devolu&ccedil;&atilde;o no primeiro dia de funcionamento subsequente. Neste caso, a Surf&rsquo;s Up n&atilde;o realizar&aacute; a cobran&ccedil;a de quaisquer valores adicionais.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>5.5. O desempenho da Plataforma poder&aacute; sofrer interrup&ccedil;&otilde;es, varia&ccedil;&otilde;es ou altera&ccedil;&otilde;es em decorr&ecirc;ncia de manuten&ccedil;&atilde;o de Esta&ccedil;&otilde;es, Produtos, servidores ou da infraestrutura de rede ou, ainda, por inconsist&ecirc;ncias causadas por eventos decorrentes de caso fortuito ou for&ccedil;a maior, na forma prevista em lei.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>5.6. Cada Plano adquirido d&aacute; direito &agrave; retirada de uma &uacute;nica Prancha por vez.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>5.7. A n&atilde;o retirada dos Produtos reservados por voc&ecirc; no dia da reserva ocasionar&aacute; no imediato cancelamento da reserva, bem como no c&ocirc;mputo de uma di&aacute;ria de utiliza&ccedil;&atilde;o dos cr&eacute;ditos referentes ao Plano adquirido, sem qualquer direito a ressarcimento.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>6. SUSPENS&Atilde;O OU EXCLUS&Atilde;O DO CADASTRO DO USU&Aacute;RIO DA PLATAFORMA<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>6.1. Os servi&ccedil;os prestados pela Surf&rsquo;s Up visam o compartilhamento de uma ampla variedade de pranchas de surfe de alta qualidade, para diversos surfistas do pa&iacute;s. Dessa forma, a fim de garantir a qualidade nos servi&ccedil;os prestados, a Surf&rsquo;s Up se reserva o direito de suspender ou cancelar, sem qualquer &ocirc;nus ou pr&eacute;vio aviso, o cadastro dos Usu&aacute;rios cuja conduta venha a prejudicar o uso dos demais surfistas aos servi&ccedil;os.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>6.2. Ocorrer&aacute; a suspens&atilde;o do direito de utiliza&ccedil;&atilde;o da Plataforma nas seguintes hip&oacute;teses, cuja relev&acirc;ncia ser&aacute; avaliada a crit&eacute;rio exclusivo da Surf&rsquo;s Up:<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>MOTIVO PRAZO DE SUSPENS&Atilde;O<\\/p>\\r\\n\\r\\n<p>a) Descumprimento de qualquer<\\/p>\\r\\n\\r\\n<p>disposi&ccedil;&atilde;o prevista nos Termos de<\\/p>\\r\\n\\r\\n<p>Uso;<\\/p>\\r\\n\\r\\n<p>01 a 03 meses, conforme a relev&acirc;ncia e eventual reincid&ecirc;ncia da pr&aacute;tica do ato.<\\/p>\\r\\n\\r\\n<p>b) Uso indevido da Plataforma, da<\\/p>\\r\\n\\r\\n<p>Esta&ccedil;&atilde;o e\\/ou dos Produtos.<\\/p>\\r\\n\\r\\n<p>03 a 09 meses ou, ainda, impedimento de utiliza&ccedil;&atilde;o dos servi&ccedil;os Surf&rsquo;s Up por prazo indeterminado, conforme a relev&acirc;ncia e eventual reincid&ecirc;ncia da pr&aacute;tica do ato.<\\/p>\\r\\n\\r\\n<p>c) N&atilde;o pagamento de qualquer valor<\\/p>\\r\\n\\r\\n<p>devido;<\\/p>\\r\\n\\r\\n<p>At&eacute; o efetivo pagamento.<\\/p>\\r\\n\\r\\n<p>d) Por determina&ccedil;&atilde;o legal ou judicial. Pelo per&iacute;odo determinado judicialmente.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>6.2.1. Considerar-se-&aacute; uso inadequado dos Produtos: qualquer conduta que extrapole o uso comum de uma prancha de surfe e seus respectivos acess&oacute;rios. Por isso, voc&ecirc; declara, desde j&aacute;, que leu e concorda com o Manual de Boas Pr&aacute;ticas e Uso dos Produtos, disponibilizado em nosso Site. Eventuais danos ser&atilde;o cobrados, conforme disposto no item 7 abaixo.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>6.2.2. Considera-se uso inadequado da Esta&ccedil;&atilde;o ou da Plataforma: qualquer conduta direcionada &agrave; tentativa de burlar a Plataforma e\\/ou a Esta&ccedil;&atilde;o ou quaisquer condutas incompat&iacute;veis com o uso da Plataforma ou das Esta&ccedil;&otilde;es, como por exemplo, fornecer dados inver&iacute;dicos na etapa de cadastramento, fornecer aos funcion&aacute;rios das esta&ccedil;&otilde;es um c&oacute;digo de reserva falso, etc.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>6.2.3. Cobran&ccedil;a por falta de devolu&ccedil;&atilde;o dos Produtos: caso o Usu&aacute;rio n&atilde;o devolva os Produtos locados ap&oacute;s o uso, dentro dos prazos estabelecidos no Plano adquirido, a<\\/p>\\r\\n\\r\\n<p>Surf&rsquo;s Up se reserva o direito de realizar a cobran&ccedil;a de tarifa adicional, proporcional aos dias excedentes at&eacute; o momento da devolu&ccedil;&atilde;o dos Produtos na Esta&ccedil;&atilde;o de onde foram retirados, salvo nos casos de furto qualificado e\\/ou roubo.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>6.3. OCORRER&Aacute; A EXCLUS&Atilde;O PERMANENTE DO CADASTRO DO USU&Aacute;RIO QUE:<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>a) Reiterar a pr&aacute;tica de qualquer dos atos previstos nos subitens anteriores;<\\/p>\\r\\n\\r\\n<p>b) Abandonar os Produtos em local inapropriado ou devolv&ecirc;-los em desacordo com previsto nestes Termos de Uso;<\\/p>\\r\\n\\r\\n<p>c) Comprovadamente, por ato doloso, causar danos aos Produtos;<\\/p>\\r\\n\\r\\n<p>d) Por determina&ccedil;&atilde;o legal ou judicial.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>7. MULTAS APLIC&Aacute;VEIS<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>7.1. Pela utiliza&ccedil;&atilde;o dos Planos adquiridos via Plataforma, poder&atilde;o ser cobradas as multas mencionadas abaixo, al&eacute;m dos custos inerentes &agrave; aquisi&ccedil;&atilde;o de cada Plano, vejamos:<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>a) Multa por danos causados aos Produtos: &eacute; cobrada sempre que qualquer dos Produtos locados pelo Usu&aacute;rio for devolvido com danos decorrentes de conduta do Usu&aacute;rio, da seguinte maneira:<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&middot; R$ 180,00 (cento e oitenta reais) - Por danos repar&aacute;veis causados aos Produtos;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&middot; R$ 2.500,00 (dois mil e quinhentos reais) - Por danos irrepar&aacute;veis causados &agrave;s Pranchas. Danos irrepar&aacute;veis ser&atilde;o considerados aqueles que acarretem na necessidade de reposi&ccedil;&atilde;o\\/substitui&ccedil;&atilde;o da Prancha, em raz&atilde;o de preju&iacute;zo na performance, quest&otilde;es de seguran&ccedil;a ou outros motivos que comprometam a utiliza&ccedil;&atilde;o da mesma por outros Usu&aacute;rios;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>b) Multa por atraso na devolu&ccedil;&atilde;o dos Produtos: &eacute; cobrada quando o Usu&aacute;rio devolver os Produtos locados fora do prazo estipulado para devolu&ccedil;&atilde;o, indicado no momento da reserva, de acordo com o Plano adquirido:<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&middot; R$ 90,00 (noventa reais) - Por dia de atraso, at&eacute; o efetivo momento da devolu&ccedil;&atilde;o na respectiva Esta&ccedil;&atilde;o.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>c) Multa por falta de devolu&ccedil;&atilde;o dos Produtos: &eacute; cobrada na ocasi&atilde;o em que os Produtos locados n&atilde;o s&atilde;o devolvidos na respectiva Esta&ccedil;&atilde;o ap&oacute;s o prazo de loca&ccedil;&atilde;o estipulado no momento da reserva, de acordo com o Plano, da seguinte forma:<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&middot; R$ 300,00 (trezentos reais) - Quando n&atilde;o ocorrer a devolu&ccedil;&atilde;o dos equipamentos acess&oacute;rios &agrave;s Pranchas, como leashs, capas, quilhas, etc., sendo certo que o jogo de quilhas &eacute; composto de 2 (duas) at&eacute; 5 (cinco) pe&ccedil;as que dever&atilde;o ser devolvidas em sua totalidade, sob pena de aplica&ccedil;&atilde;o da multa aqui estipulada.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&middot; R$ 2.500,00 (doiz mil e quinhentos reais) - Quando n&atilde;o ocorrer a devolu&ccedil;&atilde;o da Prancha.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>8. LIBERA&Ccedil;&Atilde;O E UTILIZA&Ccedil;&Atilde;O DOS PRODUTOS<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>8.1. Para efetuar a libera&ccedil;&atilde;o para utiliza&ccedil;&atilde;o dos Produtos o Usu&aacute;rio dever&aacute; proceder com a reserva do Produto desejado via Plataforma, de acordo com a disponibilidade do respectivo Produto na Esta&ccedil;&atilde;o desejada. Feita a reserva, ser&aacute; gerado um c&oacute;digo de reserva<\\/p>\\r\\n\\r\\n<p>que dever&aacute; ser informado ao funcion&aacute;rio da Esta&ccedil;&atilde;o, que proceder&aacute; com a entrega ao Usu&aacute;rio dos Produtos reservados.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>8.2. Caso o Usu&aacute;rio detecte que o Produto escolhido possui algum tipo de defeito ou problema, dever&aacute; informar imediatamente o funcion&aacute;rio da Esta&ccedil;&atilde;o acerca do problema e n&atilde;o dever&aacute; retirar o Produto da Esta&ccedil;&atilde;o. Neste caso, o Usu&aacute;rio dever&aacute; fazer o cancelamento de sua reserva e realizar uma nova reserva para retirada de outro Produto, sem &ocirc;nus.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>8.3. A reserva dos Produtos poder&aacute; ser feita com, no m&aacute;ximo, 01 (um) m&ecirc;s de anteced&ecirc;ncia.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>8.4. Para os Usu&aacute;rios que adquirirem a Assinatura Mensal ou a Assinatura Anual, o prazo m&aacute;ximo para utiliza&ccedil;&atilde;o de uma reserva de um mesmo Produto ser&aacute; de 07 (sete) dias consecutivos. Ou seja, ap&oacute;s este prazo voc&ecirc; se compromete em realizar a devolu&ccedil;&atilde;o dos Produtos e, se for o caso, realizar uma nova reserva para continuar a utiliza&ccedil;&atilde;o de nossos servi&ccedil;os.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>8.5. Conclu&iacute;dos os 07 (sete) dias consecutivos de uso de um mesmo Produto, caso o Usu&aacute;rio deseje continuar a utiliza&ccedil;&atilde;o deste Produto, ele dever&aacute; realizar a devolu&ccedil;&atilde;o do Produto normalmente e em seguida realizar uma nova reserva deste Produto por meio da Plataforma.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>8.6. A Surf&rsquo;s Up estimula seus Usu&aacute;rios a utilizarem o maior n&uacute;mero poss&iacute;vel de nossos Produtos, por isso, oferecemos a possibilidade do Usu&aacute;rio realizar a substitui&ccedil;&atilde;o dos Produtos a seu crit&eacute;rio. Para isso, voc&ecirc; dever&aacute; realizar a devolu&ccedil;&atilde;o dos Produtos sob sua posse para conseguir reservar outros na mesma data. Caso o Usu&aacute;rio fa&ccedil;a uma nova reserva e n&atilde;o devolva os Produtos que com ele se encontram at&eacute; a data da retirada da nova reserva, esta ser&aacute; automaticamente cancelada.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>8.7. Caso, devido ao atraso na devolu&ccedil;&atilde;o dos Produtos por outro Usu&aacute;rio, os Produtos por voc&ecirc; reservados n&atilde;o se encontrem no Clube, no momento de sua retirada, voc&ecirc; dever&aacute; realizar a reserva de novos Produtos, ali dispon&iacute;veis, ou entrar em contato com a central de atendimento Surf&rsquo;s Up para cancelamento de sua reserva e solicita&ccedil;&atilde;o de estorno do valor pago ou para utiliza&ccedil;&atilde;o deste valor, como cr&eacute;ditos, em reservas futuras.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>9. CANCELAMENTO<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>9.1. Ap&oacute;s a aquisi&ccedil;&atilde;o de Planos Avulsos, da Assinatura Mensal ou da Assinatura Anual, o Usu&aacute;rio ter&aacute; o direito, em at&eacute; 07 (sete) dias cotados da data de pagamento, de realizar o cancelamento de sua compra, desde que o mesmo ainda n&atilde;o tenha utilizado seu Plano ou Assinatura. Para isso, o Usu&aacute;rio dever&aacute; realizar a solicita&ccedil;&atilde;o de cancelamento por meio do endere&ccedil;o eletr&ocirc;nico: contato@surfsupclub.com.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>9.2. T&atilde;o logo seja analisada a solicita&ccedil;&atilde;o de cancelamento do Usu&aacute;rio, a equipe Surf&rsquo;s Up entrar&aacute; em contato para solicitar informa&ccedil;&otilde;es e\\/ou confirmar o cancelamento, bem como informar o prazo para reembolso ao Usu&aacute;rio, que ser&aacute; de no m&aacute;ximo 05 (cinco) dias &uacute;teis, caso a solicita&ccedil;&atilde;o seja aprovada.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>9.3. N&atilde;o ser&atilde;o aprovados os reembolsos de Planos ou Assinaturas que j&aacute; tenham sido utilizados pelo Usu&aacute;rio ou que sejam solicitados fora do prazo disposto no item 9.1 acima.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>10. DEVOLU&Ccedil;&Atilde;O DOS PRODUTOS<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>10.1. No ato da devolu&ccedil;&atilde;o dos Produtos, certifique-se de que os Produtos est&atilde;o sendo entregues no mesmo estado de conserva&ccedil;&atilde;o em que foram retirados, sob pena de arcar com a multa prevista nestes Termos de Uso.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>10.2. Caso a Esta&ccedil;&atilde;o em que voc&ecirc; retirou e deve devolver os Produtos esteja com problemas ou n&atilde;o esteja funcionando dentro do hor&aacute;rio de funcionamento informado, entre em contato com a Central de Atendimento da Surf&rsquo;s Up para ser orientado de como proceder.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>11. FURTO OU ROUBO DOS PRODUTOS<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>11.1. Caso os Produtos locados pelo Usu&aacute;rio sejam furtados ou roubados, o Usu&aacute;rio dever&aacute; informar a Central de Atendimento da Surf&rsquo;s Up o mais r&aacute;pido poss&iacute;vel, para que sejam providenciados o imediato bloqueio do Plano e o reporte &agrave;s autoridades competentes.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>11.2. Na hip&oacute;tese elencada no item anterior, o Usu&aacute;rio dever&aacute; apresentar o respectivo Boletim de Ocorr&ecirc;ncia para evitar a cobran&ccedil;a por falta de devolu&ccedil;&atilde;o dos Produtos;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>11.3. O Boletim de Ocorr&ecirc;ncia dever&aacute; ser enviado para o e-mail contato@surfsupclub.com, no prazo de at&eacute; 05 (cinco) dias &uacute;teis ap&oacute;s o ocorrido e dever&aacute; ser informado o n&uacute;mero do protocolo que Usu&aacute;rio recebeu quando da liga&ccedil;&atilde;o feita &agrave; Central de Atendimento.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>11.4. O Usu&aacute;rio est&aacute; ciente que a falsa comunica&ccedil;&atilde;o de crime estar&aacute; sujeita &agrave;s disposi&ccedil;&otilde;es e penas descritos no Decreto Lei n&ordm; 2.848 de 7 de dezembro de 1940 (C&oacute;digo Penal).<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>12. DICAS E REGRAS DE SEGURAN&Ccedil;A<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>12.1. O Usu&aacute;rio declara que est&aacute; apto a utilizar os Produtos disponibilizados pela Surf&rsquo;s Up e que seu estado de sa&uacute;de &eacute; adequado para a pr&aacute;tica do surfe.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>12.2. A Surf&rsquo;s Up n&atilde;o se responsabiliza pelos danos causados ao pr&oacute;prio Usu&aacute;rio ou a terceiros pelo uso dos Produtos.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>13. DECLARA&Ccedil;&Otilde;ES DO USU&Aacute;RIO<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>13.1. Desde j&aacute;, o Usu&aacute;rio declara estar ciente de que:<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>13.2. O uso inadequado dos Produtos, nos termos descritos acima, isenta a Surf&rsquo;s Up de qualquer responsabilidade civil, penal, administrativa ou em qualquer outra esfera;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>13.3. Os Produtos possuem os certificados de seguran&ccedil;a exigidos pela legisla&ccedil;&atilde;o vigente;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>13.4. N&atilde;o deve estar sob efeito de &aacute;lcool ou outra subst&acirc;ncia que retire ou diminua sua coordena&ccedil;&atilde;o motora, discernimento ou percep&ccedil;&atilde;o no momento de utiliza&ccedil;&atilde;o dos Produtos;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>13.5. N&atilde;o ser&aacute; de responsabilidade da Surf&rsquo;s Up, em hip&oacute;tese alguma, o roubo, perda ou dano aos objetos afixados nas Pranchas ou transportados utilizando-as;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>13.6. Qualquer avaria causada aos Produtos durante o uso dever&aacute; ser comunicada &agrave; Central de Atendimento da Surf&rsquo;s Up, imediatamente.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>13.7. A utiliza&ccedil;&atilde;o dos Produtos poder&aacute; exp&ocirc;-lo a situa&ccedil;&otilde;es de risco, tais como ferimentos f&iacute;sicos, risco de morte e\\/ou danos a propriedade, sendo assim, a Surf&rsquo;s Up n&atilde;o ser&aacute; responsabilizada por qualquer dano f&iacute;sico, material ou moral causado ao Usu&aacute;rio ou a terceiros durante e\\/ou em decorr&ecirc;ncia da utiliza&ccedil;&atilde;o dos Produtos;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>13.8. Caso o Usu&aacute;rio venha a sofrer qualquer dano f&iacute;sico comprovadamente ocasionado por suposta falha dos Produtos, dever&aacute; comunicar a Surf&rsquo;s Up imediatamente por meio da Central de Atendimento e seguir as orienta&ccedil;&otilde;es dadas pelo atendente, para que lhe possa ser prestado o suporte adequado.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>14. USO DO SITE<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>14.1. Em rela&ccedil;&atilde;o ao Site, quando do cadastro do Usu&aacute;rio na Plataforma, este estar&aacute; autorizado a utiliz&aacute;-la de forma pessoal e n&atilde;o comercial. O Usu&aacute;rio n&atilde;o poder&aacute; fazer qualquer altera&ccedil;&atilde;o nem copiar e reproduzir o conte&uacute;do do Site, em nenhuma hip&oacute;tese, sem a autoriza&ccedil;&atilde;o por escrito da Surf&rsquo;s Up, detentora dos direitos incidentes sobre a marca e conte&uacute;dos produzidos.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>15. USO DE M&Iacute;DIAS SOCIAIS<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>15.1. A Surf&rsquo;s Up se utiliza de redes sociais como Facebook, Twitter e Instagram para interagir com seus clientes\\/seguidores. Nas nossas rela&ccedil;&otilde;es nas m&iacute;dias sociais, vamos compartilhar experi&ecirc;ncias e informa&ccedil;&otilde;es e ouvir o que voc&ecirc; tem para nos falar.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>15.2. As p&aacute;ginas da Surf&rsquo;s Up nas referidas redes sociais tamb&eacute;m est&atilde;o sujeitas aos Termos de Uso e se dedicam exclusivamente a tratar de assuntos relacionados aos servi&ccedil;os prestados pela Surf&rsquo;s Up.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>15.3. A Surf&rsquo;s Up preza pelo bom uso das suas redes sociais, de forma a preservar a sua reputa&ccedil;&atilde;o frente aos terceiros com quem mant&eacute;m qualquer tipo de rela&ccedil;&atilde;o comercial. Qualquer conte&uacute;do publicado pelo Usu&aacute;rio nas redes sociais da Surf&rsquo;s Up que seja considerado ofensivo &agrave;s pessoas, ilegal ou que fa&ccedil;a propaganda ou publicidade de qualquer natureza, que viole direitos autorais e\\/ou de imagem, que estimule a pr&aacute;tica de il&iacute;citos civis e penais ou, que de qualquer outra forma, prejudique terceiros ou seja contr&aacute;rio &agrave; legisla&ccedil;&atilde;o vigente, ser&aacute; removido imediatamente. N&atilde;o nos responsabilizamos pelo conte&uacute;do publicado por terceiros em nossas redes sociais. Os termos deste subitem ser&atilde;o interpretados a crit&eacute;rio exclusivo da Surf&rsquo;s Up.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>16. CONTE&Uacute;DOS PUBLICADOS POR TERCEIROS NO SITE E NA PLATAFORMA<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>16.1. Voc&ecirc; tem ci&ecirc;ncia e concorda que poder&atilde;o ser veiculados an&uacute;ncios e propagandas de terceiros no Site e que qualquer produto, servi&ccedil;o ou conte&uacute;do disponibilizados nestes canais &eacute; de responsabilidade exclusiva das empresas anunciantes.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>17. MARCAS E NOMES DE PRODUTOS<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>17.1. Todo o conte&uacute;do, especialmente nomes, marcas e logotipos comerciais disponibilizados no Site s&atilde;o de propriedade exclusiva da Surf&rsquo;s Up e\\/ou de seus parceiros. Sendo assim, estes conte&uacute;dos n&atilde;o poder&atilde;o ser utilizados por quaisquer terceiros sem autoriza&ccedil;&atilde;o expressa da Surf&rsquo;s Up, na qualidade de detentora dos direitos de propriedade intelectual incidentes em cada caso.<\\/p>\\r\\n\\r\\n<p>18. DISPOSI&Ccedil;&Otilde;ES GERAIS<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>18.1. Estes Termos de Uso v&atilde;o reger a rela&ccedil;&atilde;o entre o Usu&aacute;rio e a Surf&rsquo;s Up enquanto ativo o seu cadastro na Plataforma.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>18.2. Os Termos de Uso est&atilde;o sujeitos a aprimoramentos constantes e a Surf&rsquo;s Up se reserva o direito de modific&aacute;-los a qualquer momento, com pr&eacute;via comunica&ccedil;&atilde;o abrangente aos Usu&aacute;rios. Voc&ecirc; deve acompanhar as atualiza&ccedil;&otilde;es e informar-se sobre o seu conte&uacute;do regularmente.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>18.3. O Usu&aacute;rio declara estar ciente de que o conte&uacute;do do Site e da Plataforma s&atilde;o protegidos pela Lei de Direitos Autorais (Lei n&ordm; 9.610\\/98), raz&atilde;o pela qual o Usu&aacute;rio somente poder&aacute; utilizar tais conte&uacute;dos para consulta, sendo vedada a utiliza&ccedil;&atilde;o para quaisquer outros fins sem a pr&eacute;via e expressa autoriza&ccedil;&atilde;o da Surf&rsquo;s Up.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>18.4. Voc&ecirc; est&aacute; ciente de que, nas situa&ccedil;&otilde;es previstas nestes Termos de Uso, podemos cancelar a qualquer tempo e sem pr&eacute;vio aviso o seu acesso de Usu&aacute;rio, n&atilde;o sendo devida indeniza&ccedil;&atilde;o caso tenha sido detectada e identificada qualquer pr&aacute;tica que contrarie esses Termos de Uso e\\/ou que cause danos &agrave; Surf&rsquo;s Up ou a terceiros.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>18.5. A n&atilde;o exig&ecirc;ncia, por parte da Surf&rsquo;s Up, do cumprimento de qualquer cl&aacute;usula ou condi&ccedil;&atilde;o aqui estabelecida, ser&aacute; considerada mera toler&acirc;ncia, n&atilde;o implicando na sua nova&ccedil;&atilde;o e tampouco na abdica&ccedil;&atilde;o do direito de exigi-la no futuro, n&atilde;o afetando a validade destes Termos de Uso e da nossa Pol&iacute;tica de Privacidade e quaisquer de suas condi&ccedil;&otilde;es.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>18.6. Estes Termos de Uso obrigam as partes e seus sucessores a qualquer t&iacute;tulo.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>18.7. Fica eleito o Foro da Comarca de S&atilde;o Paulo para dirimir eventuais d&uacute;vidas ou controv&eacute;rsias decorrentes dos presentes Termos de Uso, sem preju&iacute;zo do disposto no C&oacute;digo de Defesa do Consumidor.<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>18.8. Voc&ecirc; pode entrar em Contato conosco por telefone ou e-mail. Abaixo, nossos dados para contato:<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>contato@surfsupclub.com<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>+55 (11) 9 3938-7311<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>+55 (11) 9 9989-8462<\\/p>\\r\\n"}'),
(5, 'pp', '{"pp_heading":"Privacy Policy","pp_detail":"<p>Surf&#39;s up Club are committed to safeguarding the information we hold about our newsletter subscribers.<\\/p>\\r\\n\\r\\n<p>Surf&#39;s up Club will use the personal information you provide to us to send you news, competitions and marketing communications for our products and services by email. We may share your personal information and we may transfer your data to a jurisdiction outside the European Economic Area for our business purposes. Your entry of your details signifies your consent to this use.<\\/p>\\r\\n\\r\\n<p>You may contact us at any time by any method if you wish to change the way in which we contact you or to tell us that you do not want to receive further information from us.<\\/p>\\r\\n\\r\\n<p>You may contact us at any time by any method if you wish to view the personal information that Surf&#39;s up Club Films holds about you. You may be required to make a payment not to exceed the legal maximum in order to receive a copy of the personal information we hold about you.<\\/p>\\r\\n\\r\\n<p>Please also let us know if your email address changes so that we can keep our database up-to-date.<\\/p>\\r\\n\\r\\n<p>The information contained within this site is intended as a general information source on the films released by Surf&#39;s up Club. Every effort is made to provide accurate, relevant and up to date information.<\\/p>\\r\\n\\r\\n<h3>Contact us<\\/h3>\\r\\n\\r\\n<p>For more information on privacy, or if you would like to access or request a correction to your personal information, please contact our privacy department at:<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li>Phone:&nbsp;<a href=\\"tel:+44 (0)1753 785 922\\">+44 (0)1753 785 922<\\/a><\\/li>\\r\\n\\t<li>Email:&nbsp;<a href=\\"mailto:mailto:surfsclub@gmail.com\\">surfsclub@gmail.com<\\/a><\\/li>\\r\\n<\\/ul>\\r\\n"}'),
(6, 'contact', '{"contact_heading":"Entre em contato conosco","contact_detail":"<p>Por favor preencha o formul&aacute;rio ao lado ou entre em contato pelo email ou WhatsApp.<\\/p>\\r\\n\\r\\n<p>Ser&aacute; um prazer atend&ecirc;-lo.<\\/p>\\r\\n"}'),
(7, 'why', '{"top_main_heading":"Por que ser s\\u00f3cio?","top_main_sub_heading":"Mais flexibilidade, mais liberdade, mais surf!"}'),
(8, 'buttons', '{"surfUPPic":"2256dcf39a7194ac7d70fc0160c7473e.jpg","home_left":"Saiba mais","home_right":"Fa\\u00e7a parte do clube","whyJoinAbove_center":"Nossos planos","whyJoinAbove_Inside":"VEJA MAIS","Surf_Below_Main":"VEJA TODAS","Surf_Packages_Below_Main":"VEJA TODAS","how_button_top":"PERGUNTAS FREQUENTES","how_button_bottom":"NOSSOS PLANOS","why_button":"NOSSOS PLANOS","filter_surf_button":"Filtros avan\\u00e7ados","filter_surf_button_signout":"Reserve agora","filter_surf_button_signin":"Reserve agora","Reserve_now_button_detail":"RESERVE AGORA","contactUsPageButton":" Deixe-nos uma mensagem","my_home_button":"IN\\u00cdCIO","my_Surfs_up_button":"Surf''s up","my_Surfs_up_1stbutton":"Sobre n\\u00f3s","my_Surfs_up_2ndbutton":"Como funciona","my_Surfs_up_3rddbutton":"Por que ser s\\u00f3cio","my_plan_button":"PLANOS","my_browse_surfboard_button":"Pranchas","my_contact_button":"Contato"}');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_why`
--

CREATE TABLE IF NOT EXISTS `tbl_why` (
  `why_id` int(11) NOT NULL AUTO_INCREMENT,
  `why_title` varchar(200) NOT NULL,
  `why_image` varchar(500) NOT NULL,
  `why_description` text NOT NULL,
  `why_status` varchar(20) NOT NULL,
  PRIMARY KEY (`why_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_why`
--

INSERT INTO `tbl_why` (`why_id`, `why_title`, `why_image`, `why_description`, `why_status`) VALUES
(2, 'Surf it all', '96f3d9b9f0f20e33ffa12780138e8ccd.svg', 'É iniciante? Surfista experiente? Vai pegar marola? Gosta de ondas grandes? Tem um estilo de surfe mais clássico? Não importa, temos pranchas para todos os surfistas e condições de onda', 'active'),
(3, 'A nave', '7fb7896b82db8e935c3d4fae77c9b887.svg', 'Todo surfista tem uma prancha mágica. Aquela que funciona bem para você em todas as condições que você vá enfrentar. Ainda não encontrou? Entre para o clube e descubra a sua', 'active'),
(4, 'Demo', 'bf5820f54f5591e1f694c26ed893a5a0.svg', 'Chega de dúvidas quando for comprar uma prancha de surfe. Teste quantas vezes quiser e nunca mais tome a decisão errada na hora de escolher a sua prancha', 'active'),
(5, 'AHA', '52f5adf48e029f53f238dfef6a2ad57e.svg', 'Tem curiosidade de experimentar outros estilos de surfe? Outras tecnologias e materiais? Surfe com as mais diferentes pranchas e descubra suas novas paixões', 'active'),
(6, 'Viaje mais leve', '5f9ad5785af7c286599440c829e6154d.svg', 'Carro cheio? Não tem como levar sua prancha? Não se preocupe mais! Agora com o Surf’s Up Club você pode viajar mais leve. Reserve sua prancha e busque direto na praia', 'active'),
(7, 'Ecofriend', 'ba68f0eab41b17396c1a376fb3312889.svg', 'Chega de pranchas velhas e empoeiradas ocupando espaço na sua garagem. Agora você pode usar todas que quiser por um preço fixo', 'active'),
(8, 'Flexibilidade', 'a74b50d5c4e438d5737baa99327191ce.svg', 'Vai viajar a trabalho? Viagem com a família? Deixe sua prancha em casa e passe no nosso clube quando tiver uma janela!', 'active');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
