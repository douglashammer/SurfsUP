<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends CI_Controller {
	public $data;
	
	public function __construct(){		
		parent::__construct();
		$this->myadmin->is_login();
		if(!$this->myadmin->is_admin()){
			redirect(ADMIN);
		}
		$this->data['coupon_active'] = true;
		$allPackage= $this->db->get_where("packages",['pkg_status'=>'active'])->result_array();
		$getColumn = array_column($allPackage,'pkg_title','pkg_id');
		//die($this->db->last_query());
		$this->data['couponType'] = ([
			'all'=>'All',
			'package'=>'All Packages',
			'subscription'=>'All Subscriptions'
		]+$getColumn);
		
		//die(print_r($this->data['couponType']));
	}
	/*Show Login Page*/
	public function index()
	{
		$this->data['row'] = $this->db
							->select('*')
							->from('coupon')
							->get();
		//$this->data['packges'] = $this->db->get_where("packages",['pkg_status'=>'active']); 
		$this->data['page'] = 'coupon/coupon';
		
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	public function add($id=''){
		if(!empty($id)){
			$where = ['cop_id'=>$id];
			$this->data['row'] = $this->db->get_where("coupon",$where)->row();
		}
		$this->data['packges'] = $this->db->get_where("packages",['pkg_status'=>'active']);
		
		if($this->input->post()){
			$this->db->where($where);
			$data = $this->input->post();
			$data['cop_status'] = isset($data['cop_status'])?'active':'inactive';
			
			if(!empty($id)){
				$this->db->set($data);
				$this->db->update("coupon");
				$this->myadmin->success("coupon Successfully Updated");
				redirect(ADMIN.'/coupon/');
			}
			if($this->db->insert("coupon",$data)){
				$this->myadmin->success("coupon Successfully Saved");
				redirect(ADMIN.'/coupon/');
			}else{
				$this->myadmin->success("Error While Saving");
			}
		}
		$this->data['page'] = 'coupon/add-coupon';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	public function delete($id)
	{
		if(!empty($id)){
			$this->db->where('cop_id',$id);
			if($this->db->delete("coupon")){
				$this->myadmin->success("Successfully Deleted");
				redirect(ADMIN.'/coupon/');
			}
		}
		$this->myadmin->error("Error While Deleting");
		redirect(ADMIN.'/coupon/');
	}
	
	
}
