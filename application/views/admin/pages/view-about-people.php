
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Tables</a>
		</li>
		<li class="active">
			<strong>Data Tables</strong>
		</li>
	</ol>
	
	<h3>Add/Remove About Us Peoples</h3>
		<div class="text-right">
			<a href="<?=base_url(ADMIN.'/home/add_people');?>" class="btn btn-info btn-lg btn-icon icon-left "> <i class="entypo-plus"></i> Add Data</a>
		</div>
		<br><br>
		<script type="text/javascript">
		jQuery( window ).load( function() {
			var $table4 = jQuery( "#table-4" );
			$table4.DataTable();
		});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					<th>SN</th>
					<th>title</th>
					<th>Description</th>
					<th>Image</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					
					<td><?=$sn++;?></td>
					<td><?=$r->people_title;?></td>
					<td><?=$r->people_name;?></td>
					<td><img src="<?=base_url(UPLOAD_PATH)."website/".$r->people_image;?>" width="150"></td>
					<td><?=$r->people_description;?></td>
					<td><?=ucfirst($r->people_status);?></td>
					<td>
						<a href="<?=base_url(ADMIN."/home/add_people/".$r->people_id);?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
						
						<a href="<?=base_url(ADMIN."/home/delete_people/".$r->people_id);?>" class="btn btn-danger btn-sm btn-icon icon-left delete_confirm">
							<i class="entypo-cancel"></i>
							Delete
						</a> <!--{$r->lcc_id}-->
						
					</td>
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		
