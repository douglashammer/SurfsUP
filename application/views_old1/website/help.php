<!doctype html>
<html>
<head>
<title>Ajuda – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}	
?>
<main>


<section id="sBanner" style="background-image: url('<?=base_url(CLIENT_ASSETS);?>images/surf-1479730_1920.jpg');">
    <div class="contain">
        <div class="content">
            <h1>Perguntas Frequêntes</h1>
            <ul>
                <li><a href="index.php">Casa</a></li>
                <li>Perguntas Frequêntes</li>
            </ul>
        </div>
    </div>
</section>
<!-- sBanner -->


<section id="help">
    <div class="contain">
        <ul class="faqLst">
            <? foreach($row->result() as $r): ?>
			<li>
                <h3><?=$r->help_question;?></h3>
                <i class="fi-minus"></i>
                <div class="cntnt">
                    <p><?=$r->help_answer;?></p>
                </div>
            </li>
			<? endforeach ;?>
        </ul>
    </div>
</section>
<!-- help -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>