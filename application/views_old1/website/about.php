<!doctype html>
<html>
<head>
<title>About us – Surf's up Club</title>
<?php

require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">

<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}	
?>

<main>


<section id="sBanner" style="background-image: url('<?=base_url(UPLOAD_PATH."website/".$this->website_m->CMS('home')->about_us_banner);?>');">
    <div class="contain">
        <div class="content">
            <h1><?=$website_about->top_main_heading;?></h1>
            <p class="pre" style="margin-top:10px;"><?=$website_about->top_main_sub_heading;?></p>
        </div>
    </div>
</section>
<!-- sBanner -->

<section id="about">
    <div class="block company">
        <div class="contain">
            <div class="content text-center">
                <h1 class="secHeading"><?=$website_about->main_heading;?></h1>
                <p><?=$website_about->main_heading_sologan;?></p>
            </div>
        </div>
    </div>
    <div class="block weAre">
        <div class="contain text-center">
            <div class="content">
                <h1 class="secHeading"><?=$website_about->how_heading;?></h1>
                <p class="pre"><?=$website_about->how_detail;?></p>
            </div>
            <ul class="lst flex ckEditor">
                <li>
                    <div class="inner">
						<div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_about->sub_picture1);?>" alt=""></div>
                        <h3><?=$website_about->how_sub_heading;?></h3>
                        <p><?=$website_about->how_sub_detail;?></p>
                    </div>
                </li>
                <li>
                    <div class="inner">
						<div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_about->sub_picture2);?>" alt=""></div>
                        <h3><?=$website_about->how_sub_heading1;?></h3>
                        <p><?=$website_about->how_sub_detail1;?></p>
                    </div>
                </li>
                <li>
                    <div class="inner">
						<div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_about->sub_picture3);?>" alt=""></div>
                        <h3><?=$website_about->how_sub_heading2;?></h3>
                        <p><?=$website_about->how_sub_detail2;?></p>
                    </div>
                </li>
            </ul>
			<div class="block company non-background">
				<div class="contain">
					<div class="content text-center">
						<p><?=$website_about->abovePeopleText;?></p>
					</div>
				</div>
			</div>
        </div>
		
    </div>
	
<!--abovePeopleText
<?=@$website_about->people_picture1;?>
 -->
    <div class="block founder">
        <div class="contain">
            <h1 class="secHeading text-center"><?=$website_about->top_people_heading;?></h1>
            <ul class="founderLst flex">
             <? foreach($obout_people->result() as $abou_ppl): ?>
				<li>
                    <div class="inner">
                        <div class="ico"><img src="<?=base_url(UPLOAD_PATH."website/".$abou_ppl->people_image);?>" alt=""></div>
                        <div class="cntnt">
                            <h3><?=$abou_ppl->people_name;?> <span><?=$abou_ppl->people_title;?></span></h3>
                            <p><?=$abou_ppl->people_description;?></p>
                        </div>
                    </div>
                </li>
            <? endforeach; ?>   
            </ul>
        </div>
    </div>
    <div class="block contactUs">
        <div class="contain">
            <div class="content text-center">
                <h1 class="secHeading"><?=$website_about->contact_heading;?></h1>
                <p><?=$website_about->contact_description;?></p>
            </div>
        </div>
    </div>
</section>
<!-- about -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>