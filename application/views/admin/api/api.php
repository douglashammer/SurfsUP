
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Payment</a>
		</li>
		<li class="active">
			<strong>Pagar API Keys</strong>
		</li>
	</ol>
	
	<h3>Update Api Keys</h3>
	<form role="form" action="" method="post" class="form-horizontal form-groups-bordered">
			<p style="color:red">Note:- Not Recommended any change if you don't know what it is meant for. Thanks<br> <center>Follow the pagar order while pasting keys</center></p>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Api Key</label>
				
				<div class="col-sm-5">
					<input type="text" required name="api_public" value="<?=@$row->api_public;?>" class="form-control" id="field-1" placeholder="Paste Public Key">
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Api Encryption Key  </label>
				
				<div class="col-sm-5">
					<input required type="text" name="api_private" value="<?=@$row->api_private;?>" class="form-control" id="field-1" placeholder="Paste Private Key">
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-8">
				<center>
					<input type="submit"  class="btn btn-success btn-lg" value="Update">
				</div>
				</center>
			</div>
	</form>
