<?php
   // $page = substr(basename($_SERVER['PHP_SELF']), 0, -4);
?>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:title" content="<?=$this->website_m->website()->site_name;?>">
<meta property="og:description" content="World's best Surfboard at your fingertips.">
<meta property="og:image" content="<?=base_url(CLIENT_ASSETS);?>/images/thumbnail.jpg">
<meta property="og:url" content="<?=base_url(CLIENT_ASSETS);?>">
<meta name="twitter:card" content="thumbnail">
<meta property="og:site_name" content="<?=$this->website_m->website()->site_name;?>">
<meta name="twitter:image:alt" content="<?=$this->website_m->website()->site_name;?>">
<!-- Css files -->
<!-- Bootstrap Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/bootstrap.min.css">
<!-- Main Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/mycss.css?v=0.1">
<!-- Media-Query Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/responsive.css">
<!-- Font-Awesome Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/font-awesome.min.css">
<!-- Font-Icon Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/font-icon.min.css">
<!-- Datepicker Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/datepicker.min.css">
<!-- Select Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/select.min.css">
<!-- Owl Carousel Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/owl.carousel.min.css">
<!-- Owl Theme Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/owl.theme.min.css">
<!-- Light Slider Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/lightslider.min.css">
<!-- Light Gallery Css -->
<link type="text/css" rel="stylesheet" href="<?=base_url(CLIENT_ASSETS);?>css/lightgallery.min.css">

<!-- JS Files -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript" src="<?=base_url(CLIENT_ASSETS);?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url(CLIENT_ASSETS);?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=base_url(CLIENT_ASSETS);?>js/datepicker.min.js"></script>

<script type="text/javascript">
var base_url = '<?=base_url();?>';
var base_url_assets = '<?=base_url(CLIENT_ASSETS);?>';
var datesForDisable = [<?=isset($dateDisabled)?(empty($dateDisabled))?'':$dateDisabled:'';?>];	
</script>
<!-- Select Js -->
<script type="text/javascript" src="<?=base_url(CLIENT_ASSETS);?>js/select.min.js"></script>
<!-- Owl Carousel Js -->
<script type="text/javascript" src="<?=base_url(CLIENT_ASSETS);?>js/owl.carousel.min.js"></script>
<!-- Light Slider Js -->
<script type="text/javascript" src="<?=base_url(CLIENT_ASSETS);?>js/lightslider.min.js"></script>
<script type="text/javascript" src="<?=base_url(CLIENT_ASSETS);?>js/lightgallery-all.min.js"></script>
<script type="text/javascript" src="<?=base_url(CLIENT_ASSETS);?>js/jquery.mousewheel.min.js"></script>
<!-- Favicon -->

<!-- Google Tag Manager --> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-MMNR93G');</script>
<!-- End Google Tag Manager -->


<link type="image/png" rel="icon" href="<?=base_url(CLIENT_ASSETS);?>images/favicon.png">
<?php
if($page=='payment'){
	
	echo '<script src="https://assets.pagar.me/pagarme-js/3.0/pagarme.min.js"></script>';
}
?>
