<!doctype html>
<html>
<head>
<title>Assinar - Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php //require_once('includes/header.php'); ?>
<main>


<section id="logOn">
    <div class="flexDv">
        <div class="contain">
            <div class="logBlk">
                <div class="loginLogo"><a href="index.php"><img src="<?=base_url(CLIENT_ASSETS);?>images/logo.svg" alt=""></a></div>
                <form action="" method="post" autocomplete="off">
                    <h2>Entre com o Surf's up</h2>
				<!--Show Message Success/error-->
				<? if($this->session->flashdata('message_success')): ?>
					<div class="alert alert-success">
						<strong>Sucesso.</strong> <?=$this->session->flashdata('message_success');?>
					</div>

				<? endif; ?>
				<? if($this->session->flashdata('message_error')): ?>
					<div class="alert alert-danger">
						<strong>Erro.</strong> <?=$this->session->flashdata('message_error');?>
					</div>

				<? endif; ?>
				<!--End Show Message Success/error-->
				<? if(validation_errors()): ?>
					<div class="alert alert-danger">
						<strong>Erro.</strong> <?=validation_errors();?>
					</div>

				<? endif; ?>
                    <div class="txtGrp">
                        <input type="email" name="mem_email" required="required" autocomplete="off" id="" class="txtBox" placeholder="O email">
                    </div>
                    <div class="txtGrp">
                        <input type="password" pattern="{6,25}" name="mem_password" required="required" autocomplete="off" id="" class="txtBox" placeholder="Senha">
                    </div>
                    <div class="rememberMe">
                        <div class="lblBtn pull-left">
                            <input type="checkbox" id="rememberMe">
                            <label for="rememberMe">Lembre de mim</label>
                        </div>
                        <a href="<?=base_url();?>forgot-password" id="pass" class="pull-right">Esqueceu a senha ?</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="bTn text-center">
                        <button type="submit" class="webBtn colorBtn">Faça login em sua conta</button>
						<br><br>
						<h2>Connect With Social</h2>
                        <a href="<?=base_url("signin/facebook_login");?>" class="webBtn colorBtn" style="background-color:#29487d" ><i class="fa fa-facebook"></i> Logar com o Facebook</a>
                        <a href="<?=base_url("signin/google_login");?>" class="webBtn colorBtn" style="background-color:#b23121" ><i class="fa fa-google" ></i> Logar com o Google</a>
                    </div>
                </form>
                <div class="haveAccount text-center">
                    <span>Não tem uma conta?</span>
                    <a href="<?=base_url();?>signup">inscrever-se</a>
                </div>
                <ul class="miniNav semi">
                    <li><a href="<?=base_url();?>privacy-policy">Política de Privacidade</a></li>
                    <li><a href="<?=base_url();?>contact">Contato</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--  logOn -->


</main>
<?php //require_once('includes/footer.php');?>
</body>
</html>