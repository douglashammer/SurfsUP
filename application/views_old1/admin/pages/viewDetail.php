<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
<div class="row col-md-12">
    
        <div class="col-md-6">
            <h3><i class="fa fa-bars"></i>Member Page</h3>
            <hr class="hr-short">
           

                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label">First Name <span class="symbol required"></span></label>
                        <input type="text" name="mem_first" value="<?=@$row->mem_first?>" class="form-control" required></div>
                 </div>
				<div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label">Last Name <span class="symbol required"></span></label>
                        <input type="text" name="mem_last" value="<?=@$row->mem_last?>" class="form-control" required></div>
                 </div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Email  <span class="symbol required"></span></label>
					<input type="text" name="mem_email" value="<?=@$row->mem_email?>" class="form-control" required></div>
				</div>
				<div class="form-group">
				<div class="col-md-12">
					<label class="control-label"> Phone</label>
					<input type="text" name="mem_phone" value="<?=@$row->mem_phone?>" class="form-control" ></div>
				</div>
			
           <div class="form-group">
					<div class="col-md-12">
					<label class="control-label">Country <span class="symbol required"></span></label>
					 <select name="mem_country"  class="txtBox selectpicker" data-live-search="true">
						<option value="">Country</option>
						<?php
						$countries = $this->website_m->get_countries();
						foreach($countries->result() as $country): ?>
							<option value="<?=$country->c_id;?>" <?=@$row->mem_country==$country->c_id?'selected':'';?> ><?=$country->c_title;?></option>
						
						<? endforeach; ?>
					</select>
				</div>
			</div>
				<div class="form-group">
				<div class="col-md-12">
					<label class="control-label"> City</label>
					<input type="text" name="mem_city" value="<?=@$row->mem_city?>" class="form-control" ></div>
				</div>
			
				<div class="form-group">
				<div class="col-md-12">
					<label class="control-label"> Zip</label>
					<input type="text" name="mem_zip" value="<?=@$row->mem_zip?>" class="form-control" ></div>
				</div>
			
				<div class="form-group">
				<div class="col-md-12">
					<label class="control-label"> Address</label>
					<input type="text" name="mem_address" value="<?=@$row->mem_address?>" class="form-control" ></div>
				</div>
				<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" name="mem_status" <?=(@$row->mem_status=='active')?'checked':'';?> />
					</div>
				</div>
			</div>
           
               
                    
            </div> 
<div class="col-md-6">
            <h3><i class="fa fa-bars"></i>Member Page</h3>
			<p style="color:red">Note:- Fill only if you want to update password</p>
            <hr class="hr-short">
			<div class="form-group">
			<div class="col-md-12">
					<label class="control-label">Set New password</label>
					<input type="text" name="password"  class="form-control" ></div>
			</div>
		
			<div class="form-group">
			<div class="col-md-12">
				<label class="control-label"> Confirm New password</label>
				<input type="text" name="newpassword"  class="form-control" ></div>
			</div>
		
</div>
<div class="clearfix"></div>
                    <div class="col-md-12"><hr class="hr-short">
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-green btn-lg" value="Update Settings">
                            </div>
                        </div>
                    </div>
                    <br><br>
                </form>
			</div>
			<p>&nbsp;</p>
            <div class="clearfix"></div>
