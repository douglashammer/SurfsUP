<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />


	<link rel="icon" href="<?=base_url(ADMIN_ASSETS);?>images/favicon.ico">

	<title>Admin Panel</title>

	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>css/bootstrap.css">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>css/neon-core.css">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>css/neon-theme.css">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>css/neon-forms.css">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>css/custom.css">

	<!--
	<script src="<?=base_url(ADMIN_ASSETS);?>js/jquery-1.11.3.min.js"></script>
	-->
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]><script src="<?=base_url(ADMIN_ASSETS);?>js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body  page-fade">
	<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	<?php
		include("sidebar.php")
	?>
		<div class="main-content">
		
		<?php
			include("topnav.php");
		?>
			
		
		<?php
		require_once(APPPATH."views/admin/{$page}.php");
		//	require_once("/../{$page}.php");
		?>
		</div>
	</div>


	<!-- Sample Modal (Default skin) -->
	<div class="modal fade" id="sample-modal-dialog-1">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Widget Options - Default Modal</h4>
				</div>
				
				<div class="modal-body">
					<p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Sample Modal (Skin inverted) -->
	<div class="modal invert fade" id="sample-modal-dialog-2">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Widget Options - Inverted Skin Modal</h4>
				</div>
				
				<div class="modal-body">
					<p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Sample Modal (Skin gray) -->
	<div class="modal gray fade" id="sample-modal-dialog-3">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Widget Options - Gray Skin Modal</h4>
				</div>
				
				<div class="modal-body">
					<p>Now residence dashwoods she excellent you. Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked. Pleasant horrible but confined day end marriage. Eagerness furniture set preserved far recommend. Did even but nor are most gave hope. Secure active living depend son repair day ladies now.</p>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>




	<!-- Imported styles on this page -->
		<!-- Imported styles on this page -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>js/datatables/Buttons-1.0.3/css/buttons.jqueryui.css">
	
	<script src="<?=base_url(ADMIN_ASSETS);?>js/datatables/datatables.min.js"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>js/select2/select2.css">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>js/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="<?=base_url(ADMIN_ASSETS);?>js/rickshaw/rickshaw.min.css">
	<script src="<?=base_url(ADMIN_ASSETS);?>js/bootstrap-switch.min.js"></script>

	<!-- Bottom scripts (common) -->
	<script src="<?=base_url(ADMIN_ASSETS);?>js/gsap/TweenMax.min.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/bootstrap.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/joinable.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/resizeable.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/neon-api.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>


	<!-- Imported scripts on this page -->
	<script src="<?=base_url(ADMIN_ASSETS);?>js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/jquery.sparkline.min.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/rickshaw/vendor/d3.v3.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/rickshaw/rickshaw.min.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/raphael-min.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/morris.min.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/toastr.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/neon-chat.js"></script>

	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/datatables/Buttons-1.0.3/js/dataTables.buttons.min.js"></script>
	
	<script src="<?=base_url(ADMIN_ASSETS);?>js/datatables/dataTables.rowReorder.js"></script>
	
	<script src="<?=base_url(ADMIN_ASSETS);?>js/ckeditor/ckeditor.js"></script>
	<script src="<?=base_url(ADMIN_ASSETS);?>js/ckeditor/adapters/jquery.js"></script>

	<!-- JavaScripts initializations and stuff-->
	<script src="<?=base_url(ADMIN_ASSETS);?>js/neon-custom.js"></script>
<script>

		jQuery(document).ready(function($)
		{
			// Sample Toastr Notification
			setTimeout(function()
			{
				var opts = {
					"closeButton": true,
					"debug": false,
					"positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
					"toastClass": "black",
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};
			<? if($message_success = $this->session->flashdata('success_message')): ?>
				toastr.success("<?=addslashes($message_success);?>", opts);
			<? endif; ?>
			<? if($message_error = $this->session->flashdata('error_message')): ?>
				toastr.error("<?=addslashes($message_error);?>", opts);
			<? endif; ?>
			}, 500);
			$(".delete_confirm").each(function(){
				$(this).click(function(e){					
					check = confirm("Are you sure? This action cannot be undone.");
					if(check){
						return true;
					}
					return false;
				});
			});
		});
</script>
<style>
.breadcrumb{
	display:none;
}
</style>

</body>
</html>