<!doctype html>
<html>
<head>
<title>Minha Reserva – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php require_once('includes/header-logged.php'); ?>
<main>


<section id="dash">
    <?php require_once('includes/topbar.php'); ?>
    <div class="contain">
        <div class="mainBlk" id="reservation">
		
            <div class="blk">
                <div class="_header">
                    <h3>Minhas reservas</h3>
                    <div class="bTn text-center">
                        <a href="<?=base_url();?>browse-surfboards" class="webBtn colorBtn">Reserve agora</a>
                    </div>
                    <ul class="nav nav-tabs semi">
						<!-- <li><a href="http://herosolutions.com.pk/sarmad/surfboards/browse-surfboards" class="btn btn-info">Reserve agora</a></li> -->
                        <li class="active"><a data-toggle="tab" href="#Present">Presente (<?=count($row->result_array())>0?count($row->result_array()):0;?>)</a></li>
                        <li><a data-toggle="tab" href="#Past">Passado (<?=count($past->result_array())>0?count($past->result_array()):0;?>)</a></li>
                        <li><a data-toggle="tab" href="#Future">Futuro (<?=count($future->result_array())>0?count($future->result_array()):0;?>)</a></li>
                    </ul>
                </div>
				<? if($this->session->flashdata("error_order")):?>
					<div class="alert alert-danger">
						<strong>Aviso </strong><?=$this->session->flashdata("error_order")?>
					</div>
				<? endif; ?>
				<? if($this->session->flashdata("success_order")):?>
					<div class="alert alert-success">
						<strong>Sucesso </strong><?=$this->session->flashdata("success_order")?>
					</div>
				<? endif; ?>
                <div class="tab-content">
                    <div id="Present" class="tab-pane fade active in">
                        <div class="blockLst">
                            <table>
								<? if(count($row->result_array())): ?>
								<thead class="hidden-xs">
									<tr>
										<th>Pranchas de surf</th>
										<th>Ordem #</th>
										<th>Localização</th>
										<th>Duração</th>
										<th>Status</th>
										<th>Ação</th>
									</tr>
								</thead>
							<? endif; ?>
                                <tbody>
								<? if(isset($message)): ?>
										<tr>
											<td colspan="6"><center><?=$message;?></center></td>
										<tr>
								<? else: ?>
								<? foreach($row->result() as $col): ?>
                                    <tr orderNo="<?=$col->ord_number;?>" myhref="<?=base_url('home/product_detail/'.$col->sur_id.'/'.str_replace(" ","-",$col->sur_title).'/json');?>"  myDates="<?=date("M, d Y",$col->ord_start);?> - <?=date("M, d Y",$col->ord_end);?>">
                                        <td class="ReservePopup">
                                            <div class="icoBlk">
                                                <div class="ico" style="border-radius:2px;"><img src="<?=base_url(UPLOAD_PATH);?><?=$this->website_m->get_surfboard_image(['sur_id'=>$col->sur_id],1)->row()->sui_images;?>" alt=""></div>
                                                <a href="javascript:void(0);" >
													<div class="name"><span class="hidden-lg hidden-md">Pranchas de surf :</span> <?=$col->sur_title;?></div>
												</a>
                                            </div>
                                        </td>
                                        <td class="ReservePopup"><div class="orderId"><div class="name"><span class="hidden-lg hidden-md">Ordem : </span> #<?=$col->ord_number;?></div></td>
                                        <td class="ReservePopup"><div class="location"><span class="hidden-lg hidden-md">Localização : </span> <?=$col->lcc_title;?></div></td>
                                        <td class="ReservePopup">
                                            <div class="days"><?=date("M, d Y",$col->ord_start);?> - <?=date("M, d Y",$col->ord_end);?>
                                                <!-- <ul class="dayLst">
                                                    <li>22 Feb, 2019</li>
                                                    <li>25 Feb, 2019</li>
                                                    <li>10 Mar, 2019</li>
                                                </ul> -->
                                            </div>
                                        </td>
                                        <td><div class="status"><span class="miniLbl <?=$order[$col->ord_status];?>"><?=ucfirst($lang[$col->ord_status]);?></span></div></td>
                                        <td width="40">
                                            <div class="bTn"><a href="<?=$col->ord_status=='canceled'?'javascript:void(0);':base_url('reservation/cancel/'.$col->ord_number)?>" class="webBtn smBtn <?=$col->ord_status=='canceled'?'isDisabled':(floor(($col->ord_start-time())/60/60))<=24?'cancelNow':'cancelNow2';?>">Cancelar</a></div>
                                        </td>
                                    </tr>
							 <? endforeach; ?>		
							<? endif; ?>		
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination">
                            <!-- <li><a href="?">1</a></li>
                            <li><a href="?" class="active">2</a></li>
                            <li><a href="?">3</a></li>
                            <li><a href="?">4</a></li>
                            <li><a href="?">5</a></li> -->
                        </ul>
                    </div>
                    <div id="Past" class="tab-pane fade">
                        <div class="blockLst">
                            <table>
								<? if(count($past->result_array())): ?>
								<thead class="hidden-xs">
									<tr>
										<th>Pranchas de surf</th>
										<th>Ordem #</th>
										<th>Localização</th>
										<th>Duração</th>
										<th>Status</th>
										<th>Açao</th>
									</tr>
								</thead>
							<? endif; ?>
                                <tbody>
                                    <? foreach($past->result() as $col): ?>
                                    <tr orderNo="<?=$col->ord_number;?>" myhref="<?=base_url('home/product_detail/'.$col->sur_id.'/'.str_replace(" ","-",$col->sur_title).'/json');?>"  myDates="<?=date("M, d Y",$col->ord_start);?> - <?=date("M, d Y",$col->ord_end);?>">
                                        <td class="ReservePopup">
                                            <div class="icoBlk">
                                                <div class="ico" style="border-radius:2px;"><img src="<?=base_url(UPLOAD_PATH);?><?=$this->website_m->get_surfboard_image(['sur_id'=>$col->sur_id],1)->row()->sui_images;?>" alt=""></div>
                                                <a href="javascript:void(0);" >
													<div class="name"><div class="name"><span class="hidden-lg hidden-md">Pranchas de surf :</span> <?=$col->sur_title;?></div>
												</a>
                                            </div>
                                        </td>
                                        <td class="ReservePopup"><div class="orderId"><span class="hidden-lg hidden-md">Ordem :</span> #<?=$col->ord_number;?></div></td>
                                        <td class="ReservePopup"><div class="location"><span class="hidden-lg hidden-md">Localização :</span> <?=$col->lcc_title;?></div></td>
                                        <td class="ReservePopup">
                                            <div class="days"><?=date("M, d Y",$col->ord_start);?> - <?=date("M, d Y",$col->ord_end);?>
                                                <!-- <ul class="dayLst">
                                                    <li>22 Feb, 2019</li>
                                                    <li>25 Feb, 2019</li>
                                                    <li>10 Mar, 2019</li>
                                                </ul> -->
                                            </div>
                                        </td>
                                        <td><div class="status"><span class="miniLbl <?=$order[$col->ord_status];?>"><?=($col->ord_status=='pending')?'Expirado':ucfirst($lang[$col->ord_status]);?></span></div></td>
                                        <td width="40">
                                            <div class="bTn"><a href="javascript:void(0);" class="webBtn smBtn isDisabled">Cancelar</a></div>
                                        </td>
                                    </tr>
							 <? endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination">
                            <!-- <li><a href="?">1</a></li>
                            <li><a href="?" class="active">2</a></li>
                            <li><a href="?">3</a></li>
                            <li><a href="?">4</a></li>
                            <li><a href="?">5</a></li> -->
                        </ul>
                    </div>
                    <div id="Future" class="tab-pane fade">
                        <div class="blockLst">
                            <table>
							<? if(count($future->result_array())): ?>
								<thead class="hidden-xs">
									<tr>
										<th>Pranchas de surf</th>
										<th>Ordem #</th>
										<th>Localização</th>
										<th>Duração</th>
										<th>Status</th>
										<th>Açao</th>
									</tr>
								</thead>
							<? endif; ?>
                                <tbody>
                                    <? foreach($future->result() as $col): ?>
                                    <tr  orderNo="<?=$col->ord_number;?>" myhref="<?=base_url('home/product_detail/'.$col->sur_id.'/'.str_replace(" ","-",$col->sur_title).'/json');?>" myDates="<?=date("M, d Y",$col->ord_start);?> - <?=date("M, d Y",$col->ord_end);?>" >
                                        <td class="ReservePopup">
                                            <div class="icoBlk">
                                                <div class="ico" style="border-radius:0px;"><img src="<?=base_url(UPLOAD_PATH);?><?=$this->website_m->get_surfboard_image(['sur_id'=>$col->sur_id],1)->row()->sui_images;?>" alt=""></div>
												<a href="javascript:void(0);" >
													<div class="name"><div class="name"><span class="hidden-lg hidden-md">Pranchas de surf :</span> <?=$col->sur_title;?>
												
												</a>
												
												</div>
                                            </div>
                                        </td>
                                        <td class="ReservePopup"><div class="orderId"><span class="hidden-lg hidden-md">Ordem :</span> #<?=$col->ord_number;?></div></td>
                                        <td class="ReservePopup"><div class="location"><span class="hidden-lg hidden-md">Localização :</span> <?=$col->lcc_title;?></div></td>
                                        <td class="ReservePopup">
                                            <div class="days"><?=date("M, d Y",$col->ord_start);?> - <?=date("M, d Y",$col->ord_end);?>
                                                <!-- <ul class="dayLst">
                                                    <li>22 Feb, 2019</li>
                                                    <li>25 Feb, 2019</li>
                                                    <li>10 Mar, 2019</li>
                                                </ul> -->
                                            </div>
                                        </td>
                                        <td><div class="status"><span class="miniLbl <?=$order[$col->ord_status];?>"><?=ucfirst($lang[$col->ord_status]);?></span></div></td>
                                        <td width="40">
                                            <div class="bTn"><a href="<?=$col->ord_status=='canceled'?'javascript:void(0);':base_url('reservation/cancel/'.$col->ord_number)?>" class="webBtn smBtn <?=$col->ord_status=='canceled'?'isDisabled':(floor(($col->ord_start-time())/60/60))<=24?'cancelNow':'cancelNow2';?>">Cancelar</a></div>
                                        </td>
                                    </tr>
							 <? endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination">
                            <!-- <li><a href="?">1</a></li>
                            <li><a href="?" class="active">2</a></li>
                            <li><a href="?">3</a></li>
                            <li><a href="?">4</a></li>
                            <li><a href="?">5</a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-------------------Start Popup--------------------------->
<!------template------------->
    <div class="popup large-popup" data-popup="surfBoardReservtion" id="surfBoardReservtion">
        <div class="tableDv">
            <div class="tableCell">
                <div class="contain">
                    <div class="_inner">
					<form action="<?=base_url("reservation/reserveNow");?>" method="post">
                        <div class="crosBtn"></div>
                        <h3>Sua reserva</h3>                        
							<div class="blockLst" id="proDetail" style="padding-top:10px;padding-bottom:0px;margin-top:10px;">
								
									<div class="contain">
										
										<div class="flexRow flex">
										
												
											<div class="col col1">
												<div class="miniSlider">
													<img src=""  alt="" class="surfboard_image">
												</div>												
												
											</div>
											<!--------------------------->
											<div class="col col2">
												<div class="content ckEditor">
													<h2 class="sur_title"></h2>
													<div class="location semi">Shaper: <em class="color regular"><span class="sur_brand"></span></em></div>
													<ul style="list-style-type:none">
														<li><strong>Medidas:</strong> <span class="sur_volume"></span></li>
														<li><strong>Tamanho:</strong> <span class="sur_size"></span></li>
														<li><strong>Código de reserva:</strong> #<span style="font-weight:bold" class="orderNo"></span></li>
														<li><strong>Data de retirada:</strong> <span style="font-weight:bold" class="myDates1"></span></li>
														<li><strong>data de retorno:</strong> <span style="font-weight:bold" class="myDates2"></span></li>
														<li><div class="location semi">Localização:<em class="color regular surfboard_location"></em></div>
														<p class="location_desc"></p>
														</li>
													
													</ul>
													
													<hr>
													
												</div>
												
											</div>
										
									</div>
								</div>
								<div class="text-left" style="font-size:11px;padding-top:10px;"><?=$website_pp->policy_reservation_text;?></div>
							</div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!---------------->

<!-----------------End popup---------------------------->
<? if(isset($popup)): ?>
<!-- dash -->
    <div class="popup large-popup" data-popup="reserveConfirmation" id="reserveConfirmation">
        <div class="tableDv">
            <div class="tableCell">
                <div class="contain">
                    <div class="_inner">
                        <div class="crosBtn"></div>
                        <h3>Confirmação de reserva</h3>                        
							<div class="blockLst" id="proDetail" style="margin:0px;padding-bottom:0px;padding-top:0px;">
									<div class="contain">
										<!--Show Message Success/error-->
										<? if(isset($warning) AND !isset($error_issue)): ?>
											<div class="alert alert-info">
												<strong>Note:</strong><?=$warning;?>
											</div>
										<? endif; ?>
										<? if(isset($error_issue)): ?>
											<div class="alert alert-danger">
												<strong>Error:</strong><?=$error_issue;?>
											</div>
										<? endif; ?>
										<div class="flexRow flex">
											<div class="col col1">
												<div class="miniSlider">
													<img src="<?=base_url().UPLOAD_PATH.@$this->website_m->get_surfboard_image(['sur_id'=>$show->sur_id],1)->row()->sui_images;?>" alt="">
												</div>												
											</div>
											<?php 
														foreach($selectedDates as $dates): 
															$showDates[] = date("M,d Y",$dates);
														endforeach; 
															//echo implode(" - ",$showDates); 
														?>
											<div class="col col2">
												<div class="content ckEditor">
													<h2><?=$show->sur_title;?></h2>
													<hr>
													<div class="location semi">Shaper: <em class="color regular"><?=$show->sur_brand;?></em></div>
																														
													<ul style="list-style-type:none">
														<li><strong>Medidas:</strong> <?=$show->sur_volume;?> L</li>
														<li><strong>Tamanho:</strong> <?=$show->sur_size;?></li>
														<li><strong>Data de retirada:</strong> <?=$showDates[0];?></li>
														<li><strong>data de retorno:</strong> <?=end($showDates);?></li>
														<li><div class="location semi">Localização: <em class="color regular"><?=$show->lcc_title;?></em></div>
														<p><?=$show->lcc_description;?></p>
														</li>
													
													</ul>
													<hr>
													
												</div>
											</div>
											
											
										</div>
										<div class="text-left" style="font-size:11px; padding-top:5px;"><?=$website_pp->policy_reservation_text;?></div>
									</div>
							</div>
						<? if(!isset($error_issue)):?>
						<form action="<?=base_url("reservation");?>" method="post">
                            <div class="bTn text-center">
                                <input type="submit" name="confirmNow" class="webBtn colorBtn lgBtn" value="Confirme a reserva">
							</div>
                        </form>
						 <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>
<script type="text/javascript">
<?=isset($popup)?'$("#reserveConfirmation").show();':'';?>
    $(function(){
        var hash = window.location.hash;
        hash && $('.nav.nav-tabs li a[href="' + hash + '"]').tab('show');
        $('.nav.nav-tabs li a').click(function(e) {
            $(this).tab('show');
            var scrollmem = $('body').scrollTop();
            window.location.hash = this.hash;
            $('html,body').scrollTop(scrollmem);
        });
    });
	$(function(){
		$(".cancelNow").each(function(){
			$(this).click(function(){
				
				res = confirm("Você têm certeza que deseja cancelar esta reserva? Cancelando agora, será descontando 1 crédito da sua conta.");
				if(res){
					return true;
				}else{
					return false;
				}
			});
		});
		$(".cancelNow2").each(function(){
			$(this).click(function(){
				
				res = confirm("Tem certeza de que deseja cancelar esta reserva?");
				if(res){
					return true;
				}else{
					return false;
				}
			});
		});
	});
</script>
<style>

.isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
}

</style>
</main>
<?php require_once('includes/footer.php');?>
</body>
</html>