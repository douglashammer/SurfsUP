<!doctype html>
<html>
<head>
<title>esqueci a senha - Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php //require_once('includes/header.php'); ?>
<main>


<section id="logOn">
    <div class="flexDv">
        <div class="contain">
            <div class="logBlk">
                <div class="loginLogo"><a href="index.php"><img src="<?=base_url(CLIENT_ASSETS);?>images/logo.svg" alt=""></a></div>
                <form action="" method="post">
                    <h2>Redefinir Senha</h2>
                    <p>Digite o endereço de e-mail associado à sua conta e lhe enviaremos um link para redefinir sua senha.</p>
					<?=isset($message)?'<p style="color:red">'.$message.'</p>':'';?>
                    <div class="txtGrp">
                        <input type="email" name="email" id="" class="txtBox" placeholder="Email Address">
                    </div>
                    <div class="bTn text-center">
                        <button type="submit" class="webBtn colorBtn">Enviar email</button>
                    </div>
                </form>
                <ul class="miniNav semi">
                    <li><a href="<?=base_url();?>privacy-policy">Política de Privacidade</a></li>
                    <li><a href="<?=base_url();?>contact">Contato</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--  logOn -->


</main>
<?php //require_once('includes/footer.php');?>
</body>
</html>