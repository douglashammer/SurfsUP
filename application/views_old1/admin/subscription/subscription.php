
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Subscription</a>
		</li>
		<li class="active">
			<strong>View Subscription</strong>
		</li>
	</ol>
	
	<h3>Add/Remove subscription</h3>
		<div class="text-right">
			<a href="<?=base_url(ADMIN.'/subscription/add');?>" class="btn btn-info btn-lg btn-icon icon-left "> <i class="entypo-plus"></i> Add Subscription</a>
		</div>
		<br><br>
		<script type="text/javascript">
		jQuery( window ).load( function() {
			var $table4 = jQuery( "#table-4" );
			$table4.DataTable({	
				dom: 'Bfrtip',
				buttons: [
					 
					 {
						  extend: 'excel',
						  text: 'Export to Excel',
						  exportOptions: {
								columns: 'th:not(:last-child)'
							}
					 }
				]});
		});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					<th>SN</th>
					<th>Subscription</th>
					<th>Type</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					
					<td><?=$sn++;?></td>
					<td><?=$r->pkg_title;?></td>
					<td><?=$r->pkg_title;?></td>
					<td><?=ucfirst($r->pkg_status);?></td>
					<td>
						<a href="<?=base_url(ADMIN."/subscription/add/{$r->pkg_id}");?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
						
						<a href="<?=base_url(ADMIN."/subscription/delete/{$r->pkg_id}");?>" class="btn btn-danger btn-sm btn-icon icon-left delete_confirm">
							<i class="entypo-cancel"></i>
							Delete
						</a>
						
					</td>
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		
