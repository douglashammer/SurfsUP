<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surfboards extends CI_Controller {
	public $data;
	
	public function __construct(){		
		parent::__construct();
		$this->myadmin->is_login();
		if(!$this->myadmin->is_admin()){
			redirect(ADMIN);
		}
		$this->data['surfboard_active'] = true;
	}
	public function view_type(){
		
		$this->data['row'] = $this->db->get("sur_type");
		$this->data['page'] = 'surfboard/view-types';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function add_type($id=''){
		if($data = $this->input->post()){
			$data['type_status'] = isset($data['type_status'])?1:0;
			$this->db->set($data);
			if(!empty($id)){
				$this->db->where("type_id",$id);
				$this->db->update("sur_type");
			}else{
				$this->db->insert("sur_type");
			}
			$this->myadmin->success("Successfully Reorder Surfboards");
			redirect(ADMIN.'/surfboards/view_type');
		}
		
		$this->data['row'] = $this->db->get("sur_type",["type_id"=>$id])->row();
		$this->data['page'] = 'surfboard/add-type';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function view_brand(){
		
		$this->data['row'] = $this->db->get("sur_brand");
		$this->data['page'] = 'surfboard/view-brand';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function add_brand($id=''){
		if($data = $this->input->post()){
			$data['brand_status'] = isset($data['brand_status'])?1:0;
			$this->db->set($data);
			if(!empty($id)){
				$this->db->where("brand_id",$id);
				$this->db->update("sur_brand");
			}else{
				$this->db->insert("sur_brand");
			}
			$this->myadmin->success("Successfully Reorder Surfboards");
			redirect(ADMIN.'/surfboards/view_brand');
		}
		
		$this->data['row'] = $this->db->get("sur_brand",["type_id"=>$id])->row();
		$this->data['page'] = 'surfboard/add-brand';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function index()	{

		if($data = $this->input->post()){
			foreach($data['order'] as $key=>$val){
				$this->db->set('o_by',$key);
				$this->db->where('sur_id',$val);
				$this->db->update('surfboards');
				//echo ($this->db->last_query());
				
			}
			//var_dump($data);exit;
			$this->myadmin->success("Successfully Reorder Surfboards");
			redirect(ADMIN.'/surfboards/');
		}
		//$this->db->where('s.sur_status','active');
		$this->db->order_by('s.o_by','ASC');
		$this->db->from("surfboards as s");
		$this->db->join("location as l","s.sur_location = l.lcc_id"); 
		$this->db->join("location_category as lc","lc.loc_id = l.loc_id");
		$this->db->join("sur_brand as sb","sb.brand_id = s.sur_brand");
		$this->db->join("sur_type as st","st.type_id = s.sur_type");
		//$this->db->limit(5);
		$this->data['row'] = $this->db->get();
		//echo $this->db->last_query();
		//$this->data['row'] = $this->db->get("surfboards");
		$this->data['page'] = 'surfboard/surfboards';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	public function emManutencao($id='',$inmanutencao=0){
		$data = $this->input->post();

		if(!empty($id)){
			/*primeiro verificando se esta em manutencao, e retornando variavel*/
			$inmanutencao = $inmanutencao==NULL||$inmanutencao==0?1:0;

			$this->db->where('sur_id',$id);
			$this->db->set(array('sur_inmanutencao' => $inmanutencao));
			$this->db->update("surfboards");
			//$this->properties("update",$id);  //update Properties

			$this->delete_images($id);
			$msg= $inmanutencao==1?'A Surfboard agora esta em manutençao!!!':"A Surfboard agora esta concertada";
			$this->myadmin->success($msg);
		}else{
			$this->myadmin->error("Error While Saving");

		}
		redirect(ADMIN.'/surfboards/');

	}
	
	public function add($id=''){
		if(!empty($id)){
			$where = ['sur_id'=>$id];
			$this->db->where('sur_id',$id);
		}
		if($this->input->post()){
			$data = $this->input->post();
			$data['sur_status'] = isset($data['sur_status'])?'active':'inactive';
			
			unset($data['sbp_property']);
			unset($data['sbp_value']);
			unset($data['sbp_status']);
			unset($data['images']);
			
			if(!empty($id)){  //update data
				$this->db->set($data);
				$this->db->update("surfboards");
				$this->properties("update",$id);  //update Properties
				if($images_uploads = $this->upload_files($_FILES['images'])){ //update Images
					$this->images_add($id,$images_uploads,$type="update");
				}
				$this->delete_images($id);
				$this->myadmin->success("Successfully Updated");
				redirect(ADMIN.'/surfboards/');
				exit;
			}
			if($this->db->insert("surfboards",$data)){ //insert data
				$insert_id = $this->db->insert_id();
				$this->properties("insert",$insert_id); //insert Properties
				if($images_uploads = $this->upload_files($_FILES['images'])){ //add Images
					$this->images_add($insert_id,$images_uploads);
				}
				$this->myadmin->success("Successfully Saved");
				redirect(ADMIN.'/surfboards/');
			}else{
				$this->myadmin->success("Error While Saving");
			}
		}
		if(!empty($id)){
			$this->data['row'] = $this->db->get_where("surfboards",$where)->row();
			$this->data['properties'] = $this->db->get_where("surfboards_properties",$where);
			$this->data['images'] = $this->db->get_where("sur_images",$where);
		}
		$this->data['brand'] = $this->db->get("sur_brand");
		$this->data['type'] = $this->db->get("sur_type");
		
		
		$this->data['page'] = 'surfboard/add-surfboards';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function delete($id)
	{
		if(!empty($id)){
			$this->db->where('sur_id',$id);
			if($this->db->delete("surfboards")){
				$this->myadmin->success("Successfully Deleted");
				redirect(ADMIN.'/surfboards/');
			}
		}
		$this->myadmin->error("Error While Deleting");
		redirect(ADMIN.'/surfboards/');
	}
	private function delete_images($id,$data=''){
		$this->load->helper("file");
		$imgdata = $this->input->post("images");
		if(!empty($imgdata)){
			if(is_array($imgdata)){
				foreach($imgdata as $id=>$value){
					if($value=='deleted'){
						$del = $this->db->get_where("sur_images",["sui_id"=>$id])->row();
						$this->db->where("sui_id",$id);
						if($this->db->delete("sur_images")){							
							@unlink(UPLOAD_PATH.$del->sui_images);
						}
					}
				}
			}
			
		}
	}
	private function images_add($id,$data,$type='insert'){
		if(!empty($data) AND !empty($id)){
			foreach($data as $images){
				$set = $images;
				$where = ['sur_id'=>$id];
				//////////////////
				$this->db->set($set);
				$this->db->set($where);
				$this->db->insert("sur_images");
			}
			$q = $this->db->last_query();
			
		}	
		//die($q);		
	}
	private function properties($do='insert',$id=''){
		$data = $this->input->post();
		if(empty($id)){
			return false;
		}
		if($do=='update'){
			$this->db->where('sur_id',$id);
			$this->db->delete("surfboards_properties");
		}
		for($i=0;$i<count($data['sbp_property']);$i++){
			if(!empty($data['sbp_property'][$i]) AND !empty($data['sbp_value'][$i])){
				$this->db->set('sbp_property',$data['sbp_property'][$i]);
				$this->db->set('sbp_value',$data['sbp_value'][$i]);
				$this->db->set('sbp_status',isset($data['sbp_status'][$i])?'active':'inactive');
				$this->db->set('sur_id',$id);
				$this->db->insert("surfboards_properties");
			}

		}
		//die($this->db->last_query());
		return true;
		
		
	}
	private function upload_files($files)
    {
		if(empty($files['name'])){
			return false;
		}
		$path = UPLOAD_PATH;
		$title = time();
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|gif|png|jpeg|bmp|svg',
            'overwrite'     => 1,               
        );

        $this->load->library('upload', $config);

        $images = array();
		
        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = $title .'_'. $image;

            

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
				$images[]['sui_images'] = $fileName;
                $this->upload->data();
            } 
        }
		if(empty($images)){
			return false;
		}
        return $images;
    }
	

	
}
