<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public $data;
	/*Home Page*/
	public function __construct(){
		parent::__construct();
		
		$this->data['showSteps'] = true;
		if(isset($_COOKIE['first'])){
			$this->data['showSteps'] = false;
		}
		
		$this->db->flush_cache();
		$this->db->where('s.sur_status','active');
		$this->db->order_by('s.o_by','ASC');
		$this->db->from("surfboards as s");
		$this->db->where('sur_home',1);
		$this->db->join("location as l","s.sur_location = l.lcc_id"); 
		$this->db->join("location_category as lc","lc.loc_id = l.loc_id");		
		$this->data['surfboards'] = $this->db->get();
		
		$this->data['website_home'] = $this->website_m->CMS('home');
		$this->data['website_about'] = $this->website_m->CMS('about');		
		$this->data['website_pp'] = $this->website_m->CMS('pp');		
		$this->data['website_tos'] = $this->website_m->CMS('tos');		
		$this->data['website_contact'] = $this->website_m->CMS('contact');
		$this->data['website_how'] = $this->website_m->CMS('how');
		$this->data['buttons'] = $this->website_m->CMS('buttons');
	}
	public function index()
	{
		$this->data['row'] = $this->db->get_where("slider",['slider_status'=>'active']);
		$this->data['packages'] = $this->website_m->get_packages(['pkg_type'=>'PACKAGES']);
		$this->data['subscription'] = $this->website_m->get_packages(['pkg_type'=>'SUBSCRIPTIONS']);
		
		$this->data['page'] = 'home';
		$this->load->view('website/home',$this->data);
	}
	/*About Us page Page*/
	public function about()
	{
		$this->data['page'] = 'sobre-nos';
		$this->data['obout_people'] = $this->db->get_where("people",['people_status'=>'active']);
		$this->load->view('website/about',$this->data);
	}
	/*How it Works Page*/
	public function how()
	{
		$this->data['page'] = 'como-funciona';
		$this->load->view('website/how-it-works',$this->data);
	}
	/*why_join Page*/
	public function why_join()
	{
		$this->data['website_why'] = $this->website_m->CMS('why');
		$this->data['row'] = $this->db->get_where("why",['why_status'=>'active']);
		$this->data['page'] = 'why-join';
		$this->load->view('website/why-join',$this->data);
	}
	/*Plans Page*/

	public function plans_old($id='')
	{
		$this->data['showSteps'] = false;
		
		if($this->session->flashdata("showSteps")){
			$this->data['showSteps'] = true;
		}else{
			if(!$this->website_m->getPlan()){
				$this->data['showSteps'] = true;
			}
		}
		if(!empty($id)){
			$data = $this->website_m->get_packages(['pkg_id'=>$id],1)->row();
			if(!empty($data)){
				$this->session->set_userdata("planid",$data);
				//die(print_r($data));
				redirect("payment");
			}
		}
		$this->data['packages'] = $this->website_m->get_packages(['pkg_type'=>'PACKAGES','pkg_status'=>'active']);
		$this->data['subscription'] = $this->website_m->get_packages(['pkg_type'=>'SUBSCRIPTIONS','pkg_status'=>'active']);
		$this->data['page'] = 'planos';
		$this->load->view('website/plans',$this->data);
	}

	public function plans($id='')
	{
		$this->data['showSteps'] = false;
		if($this->session->userdata('mem_login')){
			if($this->session->plan){
		           $this->data['showSteps'] = false;
			} else {
			     $this->data['showSteps'] = true;
			}
		}



		if(!empty($id)){
			$data = $this->website_m->get_packages(['pkg_id'=>$id],1)->row();
			if(!empty($data)){
				$this->session->set_userdata("planid",$data);
				//die(print_r($data));
				redirect("payment");
			}
		}
		$this->data['packages'] = $this->website_m->get_packages(['pkg_type'=>'PACKAGES','pkg_status'=>'active']);
		$this->data['subscription'] = $this->website_m->get_packages(['pkg_type'=>'SUBSCRIPTIONS','pkg_status'=>'active']);
		$this->data['page'] = 'planos';
		$this->load->view('website/plans',$this->data);
		//var_dump($this->data);die('--');

	}

	/*Plans Page*/
	public function browse_surfboards()
	{
		if(!$this->website_m->is_login('header')){
			$this->data['showSteps'] = false;
		}
		
		/*
		if(!$this->website_m->getPlan()){
			$this->data['showSteps'] = true;
		}
		*/
		$this->data['currentPage'] = $page = ($this->input->get('per_page'))?$this->input->get('per_page'):0;
		$showPerPage = 12;
		$page = $page>1?($page-1):0;
		$limitStart = $page*$showPerPage;

		$this->data['location'] = '';
		//die(print_r($this->input->get()));
		if($this->input->get("apply_filter")){ //filter apply
			$input = $this->input->get();
			
			//if(!empty())
			
			if(!empty($input['sur_dates'])){
				$today = strtotime(date("Y-m-d"));				
				$exp = explode("-",$input['sur_dates']);
				$exp = array_map("trim",$exp);
				$exp = array_map(array($this,'date_to_time') , $exp);
				$PreOrders = $this->website_m->getAllOrders($exp[0],$exp[1]);
				$allSurfBo = $this->website_m->getAllSurfboard();
				$differ = array_diff($allSurfBo,$PreOrders);
				
				foreach($differ as $sur_ids){
					$this->db->or_where("s.sur_id",$sur_ids);
				}
				
			}
			if(!empty($input['sur_length'])){
				
				if(strpos($input['sur_length'],";")){
					$searchType = explode(";",$input['sur_length']);
				}				
				$this->db->where("s.sur_length between {$searchType[0]} AND {$searchType[1]}");	
			}
			if(!empty($input['sur_volume'])){

				if(strpos($input['sur_volume'],";")){
					$searchType = explode(";",$input['sur_volume']); 
				}				
				$this->db->where("s.sur_volume between {$searchType[0]} AND {$searchType[1]}");	
			}
			if(!empty($input['sur_type'])){
				$this->db->reset_query();
				foreach($input['sur_type'] as $surfboard_type){
					$this->db->or_where("s.sur_type",$surfboard_type);
				}
			}
			if(!empty($input['sur_brand'])){
				$this->db->reset_query();
				foreach($input['sur_brand'] as $surfboard_types){
					$this->db->or_where("s.sur_brand",$surfboard_types);
				}
			}
			
		}
		if($loc_submit = $this->input->get('location')){
			if(!empty($loc_submit)){
				$this->db->reset_query();				
				//if(!empty($loc)){
					$this->db->where(['l.lcc_id'=>$loc_submit]);
				//}
			}
		}
		if(!empty($query)){
			$this->db->where('s.sur_location',$query);
		}
		
		if($this->input->get("query")){ //filter apply
			$input = $this->input->get();
			if(!empty($input['query'])){
				$this->db->reset_query();
				$this->db->or_like('s.sur_title',$input['query']);
				$this->db->or_like("sb.sur_brands ",$input['query']);
				$this->db->or_like("st.sur_types",$input['query']);
				$this->db->or_like("s.sur_volume ",str_replace("’","",$input['query']));
				$this->db->or_like("s.sur_size",$input['query']);
				$this->db->or_like("l.lcc_title",$input['query']);
			
				//$this->data['query'] = $input['query'];
			}
		}
		 
		$this->db->select("    *, st.sur_types as sur_type , s.sur_type as sst , sb.sur_brands as sur_brand , s.sur_brand as ssb ");
		$this->db->from("surfboards as s");
		$this->db->order_by('s.o_by','ASC');
		$this->db->join("sur_type as st","st.type_id = s.sur_type");
		$this->db->join("sur_brand as sb","sb.brand_id = s.sur_brand");
		$this->db->join("location as l","s.sur_location = l.lcc_id");
		$this->db->join("location_category as lc","lc.loc_id = l.loc_id");
		$this->db->where('s.sur_status','active');
		$this->db->where('l.lcc_status','active');
		$this->db->where('lc.loc_status','active');
		$this->db->where('sb.brand_status',1);
		$this->db->where('st.type_status',1);
		
		$getall = $this->db->get();
		$this->data['surfboards'] = $this->db->query($this->db->last_query()." LIMIT $limitStart , $showPerPage");
		//die($this->db->last_query()); 
		/////////////////////////
		$total_rec = count($getall->result_array());
		$pageQuery = $this->input->get();
		unset($pageQuery['per_page']);
		$this->load->library('pagination');
		$config['base_url'] = base_url(uri_string())."?".http_build_query($pageQuery);
		//$config['uri_segment'] = 2;		
		$config['total_rows'] = $total_rec;
		$config['per_page'] = $showPerPage;
		$config['page_query_string'] = TRUE;
		$config['use_page_numbers'] = TRUE;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['attributes']['rel'] = FALSE;
		$this->pagination->initialize($config);
		////////////////////////////////
		if($this->website_m->is_login('header')){
			setcookie("first","done",time()+12*60*60);
		}
		$this->data['brand_S'] = $this->db->get("sur_brand");
		$this->data['type_S'] = $this->db->get("sur_type");
		$this->data['page'] = 'browse-surfboards';
		$this->load->view('website/browse-surfboards',$this->data);
	}
	/*Product Details Page*/
	public function product_detail($id,$title='',$replytype='html',$num_order='',$ord_id='')
	{
		$today = strtotime(date("Y-m-d"));
		$board = $this->website_m->get_surfboard(['sur_id'=>$id],1)->row();
		if(empty($board)){
			
			redirect("browse-surfboards");
		}
		
		//store Flash Session for Reservation
		$this->session->set_userdata("product_id",$board);
		//get its properties
		$webImages = $this->website_m->get_surfboard_image(['sur_id'=>$id]);
		$properties = $this->website_m->get_sur_properties($board->sur_id);
		
		$disabledDates = $this->db
							->where("sur_id",$id)
							->where(" ord_status = 'pending' ")
							//->or_where("ord_status","canceled")
							->where("ord_surfboard_date >= ",$today)
							->get("order")->result_array();
		$dates = array_column($disabledDates,"ord_surfboard_date");
		$mydate = array_map(array($this,"convert_to_date"),$dates);
		if(count($mydate)>0){
			$mydate = implode(",",$mydate);
		}
		$this->data["dateDisabled"] = $mydate;
		$this->data['webImages'] = $webImages;
		$this->data['board'] = $board;
		$this->data['properties'] = $properties;
		$this->data['page'] = 'product-detail';
		$this->data['inmanutencao'] = $board->sur_inmanutencao;
		//var_dump($this->data);exit;
		//for json return type
		if($replytype=='json'){
			$json['surfboard'] = $board;
			$json['properties'] = $properties->result_array();
			$json['surfboard_image'] = $webImages->result_array();
			$mydate = str_replace("'","",$mydate);
			if(!empty($mydate)){
				if(strpos($mydate,",")){
					$mydate = explode(",",$mydate);
				}else{
					$mydate = '';
				}
			}else{
				$mydate = '';
			}
			$json['disabled'] = $mydate;
			$json['num_order'] = $num_order;
			$json['ord_id'] = $ord_id;
			return $this->output
            ->set_content_type('application/json')
			->set_output(json_encode($json));
		}
		//var_dump($this->data);exit();
		$this->load->view('website/product-detail',$this->data);
		
	}
	public function convert_to_date($date){
		return "'".date("d/m/Y",$date)."'";
	}
	public function date_to_time($date,$seprater='/'){
		$exp = explode($seprater,$date);
		$exp2 = array_map('trim',$exp);
		list($day,$month,$year) = $exp2;
		return strtotime("{$year}-{$month}-{$day}"); //date in unix timstamp format
	
	}
	
	/*Contact Page*/

	public function contact()
	{
		$this->form_validation->set_rules($this->website_m->formValidation("contact_us"));
		if($this->form_validation->run()){
			if($var = $this->input->post()){
				
				$this->db->insert("contact",$var);
				$this->myadmin->sendMail('',"Message from {$var['cnt_name']} On Surfboards","
				<br>
				<h1>Subject: {$var['cnt_subject']}</h1>
				<h3>Message:</h3><p>{$var['cnt_message']}</p>
				",$var['cnt_email']);
				$this->session->set_flashdata('message_success', "Sua mensagem enviada com sucesso, obrigado por nos contatar");
				//redirect("contact");
				redirect("contato");
			}
		}
		$this->data['page'] = 'contato';
		$this->load->view('website/contact',$this->data);
	}
	/*Privicy Policy Page*/
	public function privacy_policy()
	{
		$this->data['page'] = 'privacy-policy';
		$this->load->view('website/privacy-policy',$this->data);
	}
	/*help Page*/
	public function help()
	{
		$this->data['row'] = $this->db->get_where("help",["help_status"=>'active']);
		$this->data['page'] = 'help';
		$this->load->view('website/help',$this->data);
	}
	
	/*Privicy Policy Page*/
	public function toc()
	{
		$this->data['page'] = 'toc';
		$this->load->view('website/terms-and-conditions',$this->data);
	}
	/*forgot-password Page*/
	public function forgot_password()
	{
		if($this->input->post()){
			$data = $this->db->get_where("members",["mem_email"=>$this->input->post("email")])->row();
				$randomHash = sha1(rand(0,9999).time().'d@It');
			if(!empty($data)){
				$this->db->insert("reset",[
					'reset_hash'=>$randomHash,
					'mem_id'=>$data->mem_id
				]);
				$this->myadmin->sendMail($data->mem_email,"Surfboards Resest Password","
				<br>
				<h1>Redefinir senha Surf's Up</h1>
				<h3><a href='".base_url()."verify/{$randomHash}'>Clique aqui para redefinir sua senha</a></h3>
				
				");
				$this->data['message'] = 'E-mail Envie com sucesso para o seu endereço de e-mail. Por favor, verifique e-mail e siga as instruções';
			}else{
				$this->data['message'] = 'Endereço de e-mail não encontrado, corrija seu endereço de e-mail ou clique em Inscreva-se';
			}
		}
		$this->data['page'] = 'forgot-password';
		$this->load->view('website/forgot-password',$this->data);
	}
	public function verify($id=''){
		if(empty($id)){
			redirect(base_url());
		}
		$row = $this->db->get_where("reset",["reset_hash"=>$id,"reset_status"=>"active"])->row();
		if(!empty($row)){
			if($data = $this->input->post()){
				if(md5($data['newpassword'])==md5($data['renewpassword']) AND !empty($data['renewpassword'])){
					$this->db->where("mem_id",$row->mem_id);
					$this->db->set("mem_password",$this->myadmin->doEncode($data['newpassword']));
					$this->db->update("members");
					
					$this->db->where(["reset_hash"=>$id]);
					$this->db->set("reset_status","inactive");
					$this->db->update("reset");
					
					$this->session->set_flashdata('message_success', "Senha com sucesso. ");
					redirect("signin");
				}else{
					$this->data['message'] = 'Senha não correspondida com a senha confirmada';
				}
			}
		}else{
			$this->data['message'] = 'Link inválido ou expirado';
		}
		$this->data['page'] = 'reset-password';
		$this->load->view('website/reset-password',$this->data);
	}
	/****************/
	/*Extra Pages View*/
	public function checkCoupon(){
		$show = false;
		$allPackage= $this->db->get_where("packages",['pkg_status'=>'active'])->result_array();
		$getColumn = array_column($allPackage,'pkg_title','pkg_id');
		$couponType = ([
			'all'=>'All',
			'package'=>'All Packages',
			'subscription'=>'All Subscriptions'
		]+$getColumn);
		
		$output = ['valid'=>'no'];
		if($data = $this->input->post()){
			
			$this->db->where(" upper(cop_code) ",strtoupper($data['coupon']));
			$this->db->where("cop_status",'active');
			$coupon = $this->db->get("coupon")->row();
			$session = $this->session->userdata("planid");
			//die(print_r($session));
			if(!empty($coupon)){
				if(md5(strtoupper($coupon->cop_code))==md5(strtoupper($data['coupon']))){
					if(in_array($couponType[$coupon->pkg_id],$couponType)){
						if($session->pkg_id==$coupon->pkg_id){
							$show = true;
						}
						else{
							if('all'==$coupon->pkg_id){
								$show = true;
							}else if('package'==$coupon->pkg_id AND $session->pkg_type=='PACKAGES') {
								$show = true;
							}
							else if('subscription'==$coupon->pkg_id AND $session->pkg_type=='SUBSCRIPTIONS') {
								$show = true;
							}
						}
						
					}
				}
			}
		}
		if($show){
			$output = ['valid'=>"yes","discount"=>$coupon->cop_discount];
		}
		die(json_encode($output));
	}
	public function scheduleJob(){
		$now = time();
		$today = strtotime(date("Y-m-d"));
		$day = 1*24*60*60;
		$yestday = $today-$day;
		$tomorrow = $today+$day;
		///////////////////////////////////
		$this->db->select("*");
		$this->db->from("order as o");
		$this->db->where("o.ord_status","pending");
		$this->db->where("o.ord_surfboard_date",$yestday);
		//$this->db->or_where("o.ord_surfboard_date",$today);
		$this->db->order_by('o.ord_surfboard_date','ASC');
		$data = $this->db->get()->result_array();
		
		print_r($data);
		//die();
		$ourDates = array_column($data,"ord_surfboard_date","ord_id");
		foreach($ourDates as $ord_id=>$dt){
			if((($now-$dt)/60/60)<25 && (($now-$dt)/60/60)>24) {
				$this->db->where("ord_id",$ord_id);
				$this->db->set(["ord_status"=>'canceled']);
				$this->db->update("order");
				//echo $ord_id."=>expired, ";
			}
		}
	}
	
	public function congratulations(){
		
		$this->data['page']='congratulations';
		$this->load->view('website/congratulations',$this->data);
		
	}
	public function em_manutencao(){
		$this->data['page']='em-manutencao';
		$this->load->view('website/em-manutencao',$this->data);

	} 

	public function comunicado(){
		$this->data['page']='comunicado';
		$this->load->view('website/comunicado',$this->data);

	} 

	
	public function error()
	{
		$this->load->view('website/404');
	}
	/*
	 * Esta função carrega a tela inicial com os dados da tabela clientes.
	 * Carrego o model de clientes, depois chamo função clientes() dentro do model que me traz os dados do cliente
	 */
	public function modalteste()
	{
		$this->data['page']='modalteste';

		$this->load->model("m_clientes", "clientes");
		
		$dados['clientes'] = $this->clientes->clientes();
		$dados['total_regitros'] = $this->clientes->totalRegistros();
		$this->load->view('website/modalteste', $dados);
	}

}
