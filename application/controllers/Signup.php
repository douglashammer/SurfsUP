<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {
	public $data;
	/*Home Page*/
		/*Sign Up Page*/
	public function index()
	{
		$this->data['showSteps'] = true;
		
		$return = $this->form_validation->set_rules($this->website_m->formValidation("members"));
		if($this->form_validation->run()){
			if($data = $this->input->post()){
				setcookie("first","done",time()-12*60*60);
				//split firstname and last name
				if(substr_count($data['mem_name']," ")){
					$exp = explode(" ",$data['mem_name']);
					list($data["mem_first"] , $data["mem_last"]) = $exp;
					
				}else{
					$data["mem_first"] = $data["mem_name"];
					$data["mem_last"] = '';
				}
				$data['mem_created_time'] = retornaDateTimePTBR();
				unset($data['mem_repassword']); //remove mem_password
				unset($data['confirm']); //remove 
				unset($data['mem_name']); //remove 
				$data['mem_password'] = $this->myadmin->doEncode($data['mem_password']);

				if($this->db->insert("members",$data)){
					$id = $this->db->insert_id();
					$randomHash = sha1(rand(0,9999).time().'d@It');					
					
					/*
					$this->db->insert("reset",[
							'reset_hash'=>$randomHash,
							'mem_id'=>$id
						]);
					*/
					$subject = $this->website_m->website()->email_subject;
					$template= $this->website_m->website()->email_template;
					$link = "<a href='".base_url()."signup/verify/{$randomHash}'>Clique aqui para ativar sua conta</a>";
					
					$template = str_replace('{firstname}',$data["mem_first"],$template);
					$template = str_replace('{lastname}',$data["mem_last"],$template);
					//$template = str_replace('{link}',$link,$template);
					
					
					$this->myadmin->sendMail($data['mem_email'],$subject,$template);
					
					//{firstname} , {lastname} and {link}
					
					$this->session->set_flashdata('message_success', "Cadastrado com sucesso.");
					$this->session->set_flashdata('redirect', "packages");
					if($this->website_m->do_login([
						"mem_email"=>$data['mem_email'],
						"mem_password"=>$this->input->post('mem_repassword')
					])){
						$this->session->set_flashdata("showSteps","yes");
						redirect("plans");
					}else{
						redirect("signin");
					}
				}else{
					$this->session->set_flashdata('message_success', "Erro não identificado.");
				}
			}
		}
		$this->data['page'] = 'signup';
		$this->load->view('website/signup',$this->data);
	}
	public function verify($id=''){
		if(empty($id)){
			redirect(base_url());
		}
		$row = $this->db->get_where("reset",["reset_hash"=>$id,"reset_status"=>"active"])->row();
		if(!empty($row)){
			$this->db->where("mem_id",$row->mem_id);
			$this->db->set("mem_verify","1");
			$this->db->update("members");
			
			$this->db->where(["reset_hash"=>$id]);
			$this->db->set("reset_status","inactive");
			$this->db->update("reset");
			
			$this->session->set_flashdata('message_success', "Email verificado com sucesso.");
			redirect("signin");
		}
		$this->session->set_flashdata('message_error', "Link inválido ou expirado.");
		redirect("signin");
		
	}
	public function  SignupEmailCheck($email){
		/*
			This is formvalidation callback function to check Email 
			its configuration present in Register Model=> Register_m

		*/
		$result = $this->db->get_where('members',['mem_email'=>$email])->row();
		if(empty($result)){
			return true;
		}
		$this->form_validation->set_message('SignupEmailCheck', "{$email} já existe.");
		return false;
    }
}
