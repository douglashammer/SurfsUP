
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Order</a>
		</li>
		<li class="active">
			<strong><?=$heading;?></strong>
		</li>
	</ol>
	
	<h3><?=$heading;?></h3>
		
		<br><br>
		<script type="text/javascript">
		$( function() {
			var $table4 = jQuery( "#table-4" );
			$table4.DataTable({	
				dom: 'Bfrtip',
				buttons: [
					 
					 {
						  extend: 'excel',
						  text: 'Exportar para Excel',
						  exportOptions: {
								columns: 'th:not(:last-child)'
							}
					 }
				],
				rowReorder: {
					selector: '.myreorder'
				},
				reorder:true,

				columnDefs: 
				[
					{ orderable: true, className: 'reorder', targets: 0 },
					{ orderable: true, className: 'reorder', targets: 1 },
					{ orderable: true, className: 'reorder', targets: 2 },
					{ orderable: true, className: 'reorder', targets: 3 },
					{ orderable: true, className: 'reorder', targets: 4 },
					{ orderable: true, className: 'reorder', targets: 5 },
					{ orderable: true, className: 'reorder', targets: 6 },
					{ orderable: true, className: 'reorder', targets: 7 },
					{ orderable: true, className: 'reorder', targets: 8 },
					{ orderable: false, targets: '_all' }
				],
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": true,
				"bInfo": false,
				"bAutoWidth": false,
				"language": {
						"sEmptyTable":   	"Nenhum dado na tabela",
						"sInfo":         	"_START_ bis _END_ von _TOTAL_ entradas",
						"sInfoEmpty":    	"0 a 0 de 0 entradas",
						"sInfoFiltered": 	"(filtrado por entradas _MAX_)",
						"sInfoPostFix":  	"",
						"sInfoThousands":  	".",
						"sLengthMenu":   	"_MENU_ Mostrar entradas",
						"sLoadingRecords": 	"Carregando...",
						"sProcessing":   	"Aguarde...",
						"sSearch":       	"Pesquisar",
						"sZeroRecords":  	"Nenhuma entrada disponível.",
						"oPaginate": {
							"sFirst":    	"Primeiro",
							"sPrevious": 	"Voltar",
							"sNext":     	"Próximo",
							"sLast":     	"Último"
						},
						"oAria": {
							"sSortAscending":  ": Ativar a ordenação da coluna crescente",
							"sSortDescending": ": Ativar a ordenação da coluna decrescente"
					}
				}});
			
			$('.sure_check').each(function(){
				$(this).click(function(e){
					//e.preventDefault();
					con = confirm("Você tem certeza? Você quer emitir.");
					if(con){
						return true;
					}
					return false;
				});
			});
			$('.cancel_check').each(function(){
				$(this).click(function(e){
					
					con = confirm("Você tem certeza? Você quer cancelar.");
					if(con){
						return true;
					}
					return false;
				});
			});
		});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					<th>SN</th>
					<th>Prancha de surf</th>
					<th>Localização</th>
					<th>Data de reserva</th>
					<th>Nome do pacote</th>
					<th>Data do pedido</th>
					<th>Status do pedido</th>
					<th>Nome</th>
					<th>O email</th>
					
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					
					<td><?=$r->ord_id;?></td>
					
					<td><?=$r->sur_title?></td>
					<td><?=$r->lcc_title?></td>
					<td><?=date("d/m/y",$r->ord_start);?> - <?=date("d/m/y",$r->ord_end);?></td>
					<td><?=$r->pkg_title?></td>
					<td><?=date("d/m/y h:i:s A",$r->ord_time);?></td>
					<td><?=ucfirst($r->ord_status)?></td>
					<td><?=$r->mem_first?> <?=$r->mem_last?></td>
					<td><?=$r->mem_email?></td>
					
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		