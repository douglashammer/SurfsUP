<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signout extends CI_Controller {
	public $data;
	
	public function __construct(){		
		parent::__construct();
		
	}
	/*Show Login Page*/
	//usr_id	usr_email	usr_password	usr_type	usr_users	usr_status

	public function index()
	{
		unset($_SESSION["admin"]);
		redirect(ADMIN);
		
	}
}