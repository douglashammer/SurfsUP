<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
<div class="row col-md-12">
    
        <div class="col-md-6">
            <h3><i class="fa fa-bars"></i>Change Website Buttons Text</h3>
            <hr class="hr-short">
           

                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label"> Home Page Slider left Button <span class="symbol required"></span></label>
                        <input type="text" name="home_left" value="<?=@$row->home_left?>" class="form-control" required></div>
                    </div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Home Page Slider Right Button  <span class="symbol required"></span></label>
						<input type="text" name="home_right" value="<?=@$row->home_right?>" class="form-control" required></div>
						
					</div>
		
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Center of Page Above Why Join Button  <span class="symbol required"></span></label>
						<input type="text" name="whyJoinAbove_center" value="<?=@$row->whyJoinAbove_center?>" class="form-control" required></div>
						
					</div>
			
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Inside Above Why Join Button  <span class="symbol required"></span></label>
						<input type="text" name="whyJoinAbove_Inside" value="<?=@$row->whyJoinAbove_Inside?>" class="form-control" required></div>
						
					</div>
			
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Below Why Join Banner (Surf's up studio) <span class="symbol required"></span></label>
						<? if(isset($row->surfUPPic)): ?> 
							<img src="<?=base_url(UPLOAD_PATH."website/").$row->surfUPPic;?>" width="150">
						<? endif; ?>
						 <input type="file" name="surfUPPic"  class="form-control" >
					</div>
				</div>
			
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Below Surf board Main Page Button  <span class="symbol required"></span></label>
						<input type="text" name="Surf_Below_Main" value="<?=@$row->Surf_Below_Main?>" class="form-control" required></div>
						
					</div>
			
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Below Packages Main Page Button  <span class="symbol required"></span></label>
						<input type="text" name="Surf_Packages_Below_Main" value="<?=@$row->Surf_Below_Main?>" class="form-control" required></div>
						
				</div>
				
			
			<h3><i class="fa fa-bars"></i>How it works Page Button</h3>
            <hr class="hr-short">
           

                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label"> Top Button <span class="symbol required"></span></label>
                        <input type="text" name="how_button_top" value="<?=@$row->how_button_top?>" class="form-control" required></div>
                    </div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Bottom Button  <span class="symbol required"></span></label>
						<input type="text" name="how_button_bottom" value="<?=@$row->how_button_bottom?>" class="form-control" required></div>

					</div>
		</div>
		<div class="col-md-6">
				
				<h3><i class="fa fa-bars"></i> Why join Pages Buttons</h3>
				<hr class="hr-short">
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Button  <span class="symbol required"></span></label>
					<input type="text" name="why_button" value="<?=@$row->why_button?>" class="form-control" required></div>
				</div>
				
			<h3><i class="fa fa-bars"></i> Browse SurfBoard Page Buttons</h3>
            <hr class="hr-short">
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Filter Button  <span class="symbol required"></span></label>
					<input type="text" name="filter_surf_button" value="<?=@$row->filter_surf_button?>" class="form-control" required></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Select Surfboard Button Before Sign In  <span class="symbol required"></span></label>
					<input type="text" name="filter_surf_button_signout" value="<?=@$row->filter_surf_button_signout?>" class="form-control" required></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Select Surfboard Button After Sign In  <span class="symbol required"></span></label>
					<input type="text" name="filter_surf_button_signin" value="<?=@$row->filter_surf_button_signin?>" class="form-control" required></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Reserve Now Button on SurfBoard Detail Page  <span class="symbol required"></span></label>
					<input type="text" name="Reserve_now_button_detail" value="<?=@$row->Reserve_now_button_detail?>" class="form-control" required></div>
				</div>
			
			<h3><i class="fa fa-bars"></i>Contact Us Page</h3>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Contact Us page Button <span class="symbol required"></span></label>
					<input type="text" name="contactUsPageButton" value="<?=@$row->contactUsPageButton?>" class="form-control" required></div>
				</div>
		</div>		
		
		<div class="row col-md-12">
    
        <div class="col-md-6">
            
		
			<div class="col-md-6">
				<h3><i class="fa fa-bars"></i>Change Navigation Buttons Text</h3>
					<hr class="hr-short">
				<h3><i class="fa fa-bars"></i>Website Top  Buttons</h3>
				<hr class="hr-short">
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label">Home Button  <span class="symbol required"></span></label>
					<input type="text" name="my_home_button" value="<?=@$row->my_home_button?>" class="form-control" required></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label">Surfs Up Plus Category  <span class="symbol required"></span></label>
					<input type="text" name="my_Surfs_up_button" value="<?=@$row->my_Surfs_up_button?>" class="form-control" required></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label">Surfs Up 1st Sub Button  <span class="symbol required"></span></label>
					<input type="text" name="my_Surfs_up_1stbutton" value="<?=@$row->my_Surfs_up_1stbutton?>" class="form-control" required></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label">Surfs Up 2nd Sub Button  <span class="symbol required"></span></label>
					<input type="text" name="my_Surfs_up_2ndbutton" value="<?=@$row->my_Surfs_up_2ndbutton?>" class="form-control" required></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label">Surfs Up 3rd Sub Button  <span class="symbol required"></span></label>
					<input type="text" name="my_Surfs_up_3rddbutton" value="<?=@$row->my_Surfs_up_3rddbutton?>" class="form-control" required></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label">Plan Button  <span class="symbol required"></span></label>
					<input type="text" name="my_plan_button" value="<?=@$row->my_plan_button?>" class="form-control" required></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label">browse Surfboard Button  <span class="symbol required"></span></label>
					<input type="text" name="my_browse_surfboard_button" value="<?=@$row->my_browse_surfboard_button?>" class="form-control" required></div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label">Contact Us Button  <span class="symbol required"></span></label>
					<input type="text" name="my_contact_button" value="<?=@$row->my_contact_button?>" class="form-control" required></div>
				</div>
			</div>
			
		
		</div>
		
		
		
           <div class="form-group">
		   
				
               
                    <div class="clearfix"></div>
                    <div class="col-md-12"><hr class="hr-short">
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-green btn-lg" value="Update Settings">
                            </div>
                        </div>
                    </div>
                    <br><br>
                </form>
            </div>            <p>&nbsp;</p>
            <div class="clearfix"></div>
