	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Order</a>
		</li>
		<li class="active">
			<strong><?=$heading;?></strong>
		</li>
	</ol>
	
	<h3><?=$heading;?></h3>
		
		<br><br>
		<script type="text/javascript">
		$( function() {
			var $table4 = jQuery( "#table-4" );
			$table4.DataTable({	
				dom: 'Bfrtip',
				"scrollX":true,
				buttons: [
					 {
						  extend: 'excel',
						  text: 'Exportar para Excel',
						  exportOptions: {
								columns: 'th:not(:last-child)'
							}
					 }
				],
				rowReorder: {
					selector: '.myreorder'
				},
				reorder:true,

				columnDefs: 
				[
					{ orderable: true, className: 'reorder', targets: 0},
					{ orderable: true, className: 'reorder', targets: 1 },
					{ orderable: true, className: 'reorder', targets: 2,  'visible':true },
					{ orderable: true, className: 'reorder', targets: 3 },
					{ orderable: true, className: 'reorder', targets: 4,  'visible':true },
					{ orderable: true, className: 'reorder', targets: 5,  'visible':true },
					{ orderable: true, className: 'reorder', targets: 6 , 'visible':true },
					{ orderable: true, className: 'reorder', targets: 7,  'visible':true },
					{ orderable: true, className: 'reorder', targets: 8 , 'visible':true },
					{ orderable: true, className: 'reorder', targets: 9 , 'visible':true },
					{ orderable: true, className: 'reorder', targets: 10 , 'visible':true},
					{ orderable: true, className: 'reorder', targets: 11 , 'visible':true},
					{ orderable: true, className: 'reorder', targets: 12 , 'visible':true},
					{ orderable: true, className: 'reorder', targets: 13 , 'visible':true},
					{ orderable: true, className: 'reorder', targets: 14 , 'visible':true},
					{ orderable: true, className: 'reorder', targets: 15 , 'visible':true},
					{ orderable: false, targets: '_all' }
				],
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": true,
				"bInfo": false,
				"bAutoWidth": false,
				"language": {
						"sEmptyTable":   	"Nenhum dado na tabela",
						"sInfo":         	"_START_ bis _END_ von _TOTAL_ entradas",
						"sInfoEmpty":    	"0 a 0 de 0 entradas",
						"sInfoFiltered": 	"(filtrado por entradas _MAX_)",
						"sInfoPostFix":  	"",
						"sInfoThousands":  	".",
						"sLengthMenu":   	"_MENU_ Mostrar entradas",
						"sLoadingRecords": 	"Carregando...",
						"sProcessing":   	"Aguarde...",
						"sSearch":       	"Pesquisar",
						"sZeroRecords":  	"Nenhuma entrada disponível.",
						"oPaginate": {
							"sFirst":    	"Primeiro",
							"sPrevious": 	"Voltar",
							"sNext":     	"Próximo",
							"sLast":     	"Último"
						},
						"oAria": {
							"sSortAscending":  ": Ativar a ordenação da coluna crescente",
							"sSortDescending": ": Ativar a ordenação da coluna decrescente"
					}
				}});
			
			$('.sure_check').each(function(){
				$(this).click(function(e){
					//e.preventDefault();
					con = confirm("Você tem certeza? Você quer emitir.");
					if(con){
						return true;
					}
					return false;
				});
			});
			$('.cancel_check').each(function(){
				$(this).click(function(e){
					
					con = confirm("Você tem certeza? Você quer cancelar.");
					if(con){
						return true;
					}
					return false;
				});
			});
		});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>SN</th>
					<th>Data e hora do pedido</th>
					<th>Cód da prancha</th>
					<th>Nome da prancha</th>
					<th>Data Horário retirada</th>
					<th>Data prevista devolução</th>
					<th>Data e horário devolução</th>
					<th>Dias atraso</th>
					<th>Status reserva</th>
					<th>Local</th>
					<th>Nome do cliente</th>
					<th>Telefone cliente</th>
					<th>Funcionário retirada</th>
					<th>Obs retirada</th>
					<th>Funcionario devolução</th>
					<th>Obs devolução</th>
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					
					<td><?=$sn++;?></td>
					<td><?=$r->ord_start!=null?date("d-m-y",$r->ord_start):"-";?></td>
					<td><?=$r->sur_id;?></td><!--cod da prancha--->
					<td><?=$r->sur_title;?></td><!--nome da prancha--->
					<td><?=$r->date_issued!=null?date("d-m-y H:i:s",$r->date_issued):"-";?></td><!--dt retirada/issued e Horario retirada--->
					<td><?=$r->ord_end!=null?date("d-m-y",$r->ord_end):'-';?></td><!--dt prevista devolução--->
					<td><?=$r->date_completed!=null?date("d-m-y H:i:s",$r->date_completed):'-';?></td><!--dt e hr devolução/ data da tabela status change_date completed--->
					<td><?=$r->status_atual=='pending'||$r->status_atual=='issued'?$r->difDates>0?$r->difDates:'0':"-";?></td><!--dias atraso(se ord_end < change_date/então mostra quantidade de dias)--->
					<td><?=ucfirst($r->status_atual);?></td><!--dstatus reserva--->
					<td><?=$r->lcc_title;?></td><!--localização--->
					<td><?=$r->mem_first?> <?=$r->mem_last?></td><!--nome do membro--->
					<td><?=$r->mem_phone?></td><!--fone do membro--->
					<td><?=$r->func_issued!=null?$r->func_issued:"---";?></td><!--func retirada--->
					<td><?=$r->obs_issued!=null?$r->obs_issued:"---";?></td><!--obs retirada--->
					<td><?=$r->func_completed!=null?$r->func_completed:"---";?></td><!--funcionario devolução--->
					<td><?=$r->obs_completed!=null?$r->obs_completed:"---";?></td><!--obs devolução--->
					
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>

