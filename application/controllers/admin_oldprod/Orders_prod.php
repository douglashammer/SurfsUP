<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {
	public $data;
	public $today;
	public $where;
	public function __construct(){		
		parent::__construct();	
		$this->myadmin->is_login();
		$this->today = strtotime(date("Y-m-d"));
		if($location = $this->myadmin->is_admin('location')){
			$this->where = ['l.lcc_id'=>$location];
		}
		$this->data['reservations_active'] = true;
		
	}
	/*Show Login Page*/
	public function index()
	{
		/*		tabela: order
				Parece ser tabela de ordem, que guarda todas as ações do site
				`ord_id`
				`sur_id`
				`ord_surfboard_date` as range_date (entrega)
				`mem_id`
				`pkg_id`
				`trx_id`
				`ord_time`
				`ord_status`
				`ord_number`
				`ord_start`
				`ord_end`
				`ord_type`
		*/

		$this->data['heading'] = 'Todas as Reservas';
		$this->db->select(" * , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("order as o");
		$this->db->join("surfboards as s","s.sur_id=o.sur_id");
		$this->db->join("members as m","m.mem_id=o.mem_id");
		$this->db->join("packages as p","p.pkg_id=o.pkg_id");
		$this->db->join("location as l","l.lcc_id=s.sur_location");
		$this->db->group_by('o.ord_number');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$this->db->order_by('o.ord_id', 'desc');
		$get = $this->db->get();
		$this->data['row'] = $get;
		
		//echo $this->db->last_query();
		
		//die(print_r($get->result_array()));
		/*var_dump($get->result_array());
		die('...');*/
		
		$this->data['page'] = 'orders/all';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function delayedSurf()
	{
		$this->data['heading'] = 'Surfboard\'s com Pagamentos Atrasados';
		$this->db->select("* , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("order as o");
		$this->db->join("surfboards as s","s.sur_id=o.sur_id");
		$this->db->join("members as m","m.mem_id=o.mem_id");
		$this->db->join("packages as p","p.pkg_id=o.pkg_id");
		$this->db->join("location as l","l.lcc_id=s.sur_location");
		//$this->db->where("o.ord_end < ", time());
		$this->db->where("o.ord_status",'issued');
		$this->db->where("o.ord_end <=",$this->today-86400);		
		$this->db->group_by('o.ord_number');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$get = $this->db->get();
		$this->data['row'] = $get;
		//echo $get->row()->ord_end;
		//print_r($this->db->last_query());
		//exit;
		$this->data['page'] = 'orders/delayed';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function today()
	{
		$this->data['heading'] = 'Resevas de hoje';
		$this->db->select("* , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("order as o");
		$this->db->join("surfboards as s","s.sur_id=o.sur_id");
		$this->db->join("members as m","m.mem_id=o.mem_id");
		$this->db->join("packages as p","p.pkg_id=o.pkg_id");
		$this->db->join("location as l","l.lcc_id=s.sur_location");
		$this->db->where("o.ord_status",'pending');
		$this->db->where("o.ord_surfboard_date",$this->today);
		//$this->db->or_where("o.ord_end <=",$this->today);		
		$this->db->group_by('o.ord_number');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$get = $this->db->get();
		$this->data['row'] = $get;
		
		//die(print_r($get->result_array()));
		
		$this->data['page'] = 'orders/orders';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function issued()
	{
		$this->data['heading'] = 'Surfboard emitidas hoje';
		$this->db->select("* , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("order as o");
		$this->db->join("surfboards as s","s.sur_id=o.sur_id");
		$this->db->join("members as m","m.mem_id=o.mem_id");
		$this->db->join("packages as p","p.pkg_id=o.pkg_id");
		$this->db->join("location as l","l.lcc_id=s.sur_location");
		$this->db->where("o.ord_surfboard_date ",$this->today);
		//$this->db->where("o.ord_end <=",$this->today);
		$this->db->where("o.ord_status",'issued');
		$this->db->group_by('o.ord_number');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$get = $this->db->get();
		$this->data['row'] = $get;
		
		//die(print_r($get->result_array()));
		
		$this->data['page'] = 'orders/issued';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function completed()
	{
		$this->data['heading'] = 'Pedidos Concluídas';
		$this->db->select("* , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("order as o");
		$this->db->join("surfboards as s","s.sur_id=o.sur_id");
		$this->db->join("members as m","m.mem_id=o.mem_id");
		$this->db->join("packages as p","p.pkg_id=o.pkg_id");
		$this->db->join("location as l","l.lcc_id=s.sur_location");
		//$this->db->where("o.ord_surfboard_date",$this->today);
		$this->db->where("o.ord_status",'completed');
		$this->db->group_by('o.ord_number');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$get = $this->db->get();
		$this->data['row'] = $get;
		
		//echo $this->db->last_query();
		
		//die(print_r($get->result_array()));
		
		$this->data['page'] = 'orders/completed';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function canceled()
	{
		$this->data['heading'] = 'Pedidos Cancelados';
		$this->db->select("* , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("order as o");
		$this->db->join("surfboards as s","s.sur_id=o.sur_id");
		$this->db->join("members as m","m.mem_id=o.mem_id");
		$this->db->join("packages as p","p.pkg_id=o.pkg_id");
		$this->db->join("location as l","l.lcc_id=s.sur_location");
		//$this->db->where("o.ord_surfboard_date",$this->today);
		$this->db->where("o.ord_status",'canceled');
		$this->db->group_by('o.ord_number');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$get = $this->db->get();
		$this->data['row'] = $get;
		
		//die(print_r($get->result_array()));
		
		$this->data['page'] = 'orders/completed';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function issue($id=''){
		if(empty($id)){
			redirect(ADMIN."/orders/today");
		}
		$this->db->set("ord_status","issued");
		$this->db->where("ord_number",$id);
		$this->db->update("order");
		$this->db->insert("order_status",[
			"ord_id"=>$id,
			"ors_status"=>"issued"
		]);
		redirect(ADMIN."/orders/today");
	}
	public function cancel($id=''){
		if(empty($id)){
			redirect(ADMIN."/orders/today");
		}
		$this->db->set("ord_status","canceled");
		$this->db->where("ord_number",$id);
		$this->db->update("order");
		$this->db->insert("order_status",[
			"ord_id"=>$id,
			"ors_status"=>"canceled"
		]);
		redirect(ADMIN."/orders/today");
	}
	public function complete($id=''){
		if(empty($id)){
			redirect(ADMIN."/orders/completed");
		}
		$this->db->set("ord_status","completed");
		$this->db->where("ord_number",$id);
		$this->db->update("order");
		$this->db->insert("order_status",[
			"ord_id"=>$id,
			"ors_status"=>"completed"
		]);
		redirect(ADMIN."/orders/completed");
	}

	
}
/*
    [9] => Array
        (
            [ord_id] => 10
            [sur_id] => 14
            [ord_surfboard_date] => 1551567600
            [mem_id] => 3
            [pkg_id] => 2
            [ord_time] => 1551545479
            [ord_status] => pending
            [sur_title] => Zabo Total Flex
            [sur_location] => 14
            [sur_volume] => Zabo Total Flex
            [sur_size] => 5'4 x 78945 x 554
            [sur_condition] => New product
            [sur_quantity] => 10
            [sur_description] => The Plunder is a fuller-plane shape board designed to bring versatility and fun to small waves without forgetting about that superior performance spark, too. The unique surface area in the outline allows the board to trim with surprising speed in the weakest of conditions, while the soft diamond tail paired with the kick in the rail-line rocker toward the back third, gives the Plunder an ability to respond and turn when a good section lines up.
            [sur_status] => active
            [mem_first] => Developer
            [mem_last] => Testing
            [mem_email] => sarmad711@gmail.com
            [mem_password] => d545481c46ebe5d1e958b7dea80d954044269f7b
            [mem_phone] => 3458341704
            [mem_country] => 168
            [mem_city] => Any
            [mem_zip] => 81400
            [mem_address] => N.S.T , Sargodha
            [mem_pic] => 3a1855f21019cc54b9ca91494752afbb.jpg
            [mem_status] => active
            [mem_verify] => 0
            [pkg_title] => Monthly
            [pkg_type] => SUBSCRIPTIONS
            [pkg_price] => 149
            [pkg_time] => 3
            [pkg_duration] => month
            [pkg_booking_per_day] => 3
            [pkg_cancel_hours] => 0
            [pkg_status] => active
        )
*/