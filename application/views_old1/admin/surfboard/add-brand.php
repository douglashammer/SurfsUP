
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Location</a>
		</li>
		<li class="active">
			<strong>Add/Edit Location</strong>
		</li>
	</ol>
	
	<h3>Add/Edit Surfboard Brands</h3>
	<form role="form" action="" method="post" class="form-horizontal form-groups-bordered">
			
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Title</label>
				
				<div class="col-sm-5">
					<input type="text" name="sur_brands" value="<?=@$row->sur_brands;?>" class="form-control" id="field-1" placeholder="Title">
				</div>
			</div>
			
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" name="brand_status" <?=(@$row->brand_status==1)?'checked':'';?> />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-8">
					<input type="submit"  class="btn btn-success btn-lg" value="Save">
				</div>
			</div>
	</form>
