<!doctype html>
<html>
<head>
<title>Parabéns – Surf's up Club</title>
<?php

require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">

<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}	
?>

<main>
<br></br>
<section id="congratulations">
    <div class="block teste">
        <div class="contain">
            <div class="content text-center congratulations">
                <h1 class="secHeading">PAGAMENTO EFETUADO COM SUCESSO</h1>
                <br></br>
                <p> Você é o mais novo membro do Surf's Up Club. Sinta-se a vontade para</p>
                <p> surfar com a prancha que quiser, quando e onde quiser.</p>

                <br></br>
                <br></br>
                <br></br>
                <p> Reserve sua prancha online filtrando por localidade e/ou filtros avançados</p>
                <br></br>
            </div>
            <div class="bTn text-center">
                <a href="<?=base_url();?>browse-surfboards" class="webBtn lgBtn colorBtn">Reservar agora</a>
            </div>

        </div>
    </div>
    <div class="block weAre">
    </div>
</section>
<!-- about -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>