<!doctype html>
<html>
<head>
<title>Home – Surf's up Club</title>
<?php
require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}
$sn=0;	
?>
<main>


<section id="banner" class="flexBox">
    <div class="flexDv">
        <div class="contain">
            <div class="content text-center">
                <h1 style="<?
					if(!empty($website_home->headin_top_color)){
						echo "color:{$website_home->headin_top_color};";
					}
					if(!empty($website_home->headin_top_size)){
						echo "font-size:{$website_home->headin_top_size}px;";
					}
					if(!empty($website_home->headin_top_family)){
						echo "font-family:'{$website_home->headin_top_family}';";
					}
					if(!empty($website_home->headin_top_letter_space)){
						echo "letter-spacing:{$website_home->headin_top_letter_space}px;";
					}
				?>"><?=$website_home->headin_top_text;?><span style="<?
					if(!empty($website_home->main_color)){
						echo "color:{$website_home->main_color};";
					}
					if(!empty($website_home->main_size)){
						echo "font-size:{$website_home->main_size}px;";
					}
					if(!empty($website_home->main_family)){
						echo "font-family:'{$website_home->main_family}';";
					}
					if(!empty($website_home->main_family_space)){
						echo "letter-spacing:{$website_home->main_family_space}px;";
					}
				?>"><?=$website_home->main_heading;?></span></h1>
                <p style="<?
					if(!empty($website_home->heading_sologan_color)){
						echo "color:{$website_home->heading_sologan_color};";
					}
					if(!empty($website_home->heading_sologan_size)){
						echo "font-size:{$website_home->heading_sologan_size}px;";
					}
					if(!empty($website_home->heading_sologan_family)){
						echo "font-family:'{$website_home->heading_sologan_family}';";
					}
					if(!empty($website_home->heading_sologan_space)){
						echo "letter-spacing:{$website_home->heading_sologan_space}px;";
					}
				?>"><?=$website_home->main_heading_sologan;?></p>
                <div class="bTn">
                    <a href="#works" class="webBtn lgBtn scrollToHow" data-spy="scroll" data-target="#works"><?=$buttons->home_left?></a>
                    <a href="<?=base_url();?>signup" class="webBtn colorBtn lgBtn"><?=$buttons->home_right?></a>
                </div>
            </div>
        </div>
    </div>
    <div id="slider" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
		<? foreach($row->result() as $r): $sn++; ?>
            <div class="item <?=$sn==1?'active':'';?>" style="background-image: url('<?=base_url(UPLOAD_PATH."website/".$r->slider_image);?>');"></div>
		<? endforeach; $sn=0; ?>
        </div>
        <!-- Indicators -->
        <ol class="carousel-indicators">
		<? foreach($row->result() as $r): $sn++; ?>
            <li data-target="#slider" data-slide-to="0" class="<?=$sn==1?'active':'';?>"></li>
           
		<? endforeach; ?>
        </ol>
        <div class="lBtn">
            <a class="prev" href="#slider" role="button" data-slide="prev">
                <i class="fi-arrow-left"></i>
            </a>
        </div>
        <div class="rBtn">
            <a class="next" href="#slider" role="button" data-slide="next">
                <i class="fi-arrow-right"></i>
            </a>
        </div>
    </div>
</section>
<!-- banner -->


<section id="works" class="worksInd">
    <div class="block working">
        <div class="contain">
            <div class="content text-center">
                <h1 class="secHeading"><?=$website_home->how_heading;?></h1>
                <p class="pre"><?=$website_home->how_detail;?></p>
    	        <ul class="listing flex">
    	            <li>
    	                <div class="inner">
                            <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_home->first_image);?>" alt=""></div>
                            <h4><?=$website_home->how_text1;?></h4>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_home->second_image);?>" alt=""></div>
                            <h4><?=$website_home->how_text2;?></h4>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_home->third_image);?>" alt=""></div>
                            <h4><?=$website_home->how_text3;?></h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="block whyUs">
        <div class="contain text-center">
            <h1 class="secHeading"><?=@$website_how->howItMainHeading2;?></h1>
            <ul class="lst flex">
                <li>
                    <div class="inner">
                        <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->why_picture1);?>" alt=""></div>
                        <div class="cntnt">
                            <h3><?=@$website_how->why_heading1;?></h3>
                            <p><?=@$website_how->why_text1;?></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->why_picture2);?>" alt=""></div>
                        <div class="cntnt">
                            <h3><?=@$website_how->why_heading2;?></h3>
                            <p><?=@$website_how->why_text2;?></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->why_picture3);?>" alt=""></div>
                        <div class="cntnt">
                            <h3><?=@$website_how->why_heading3;?></h3>
                            <p><?=@$website_how->why_text3;?></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->why_picture4);?>" alt=""></div>
                        <div class="cntnt">
                             <h3><?=@$website_how->why_heading4;?></h3>
                            <p><?=@$website_how->why_text4;?></p>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="bTn text-center"><a href="<?=base_url();?>plans" class="webBtn lgBtn colorBtn"><?=$buttons->whyJoinAbove_center?></a></div>
        </div>
    </div>
</section>
<!-- works -->
<!-- join -->

<section id="studio" class="flexBox">
    <div class="flexDv">
        <div class="contain">
            <div class="flexRow flex">
                <div class="col col1">
                    <div class="content">
                        <h1 class="secHeading"><?=$website_home->middle_banner_heading;?></h1>
                        <p><?=$website_home->middle_banner_description;?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="<?=base_url(UPLOAD_PATH);?>website/<?=@$buttons->surfUPPic;?>" alt="">
</section>

<section id="join">
    <div class="contain">
        <div class="flexRow flex">
            <div class="col col1">
                <div class="content">
                    <h1 class="secHeading"><?=$website_home->why_heading;?></h1>
                    <p><?=$website_home->why_description;?></p>
                    <a href="<?=base_url();?>why-join" class="webBtn colorBtn lgBtn"><?=$buttons->whyJoinAbove_Inside?></a>
                </div>
            </div>
            <div class="col col2">
                <ul class="lst flex">
                    <li>
                        <div class="inner">
                            <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_home->why_image1);?>" alt=""></div>
                            <h4><?=$website_home->why_heading1;?></h4>
                            <p><?=$website_home->why_text1;?></p>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_home->why_image2);?>" alt=""></div>
                            <h4><?=$website_home->why_heading2;?></h4>
                            <p><?=$website_home->why_text2;?></p>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_home->why_image3);?>" alt=""></div>
                            <h4><?=$website_home->why_heading3;?></h4>
                            <p><?=$website_home->why_text3;?></p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- studio -->


<section id="surfboards">
    <div class="contain text-center">
        <h1 class="secHeading">Pranchas de surf</h1>
        <div id="owl-surfs" class="owl-carousel owl-theme">
           <? foreach($surfboards->result() as $board ): ?>
		   <div class="inner">
                <div class="iTm">
                    <div class="image">
                        <a href="<?=base_url();?>product-detail/<?=$board->sur_id?>/<?=str_replace(" ","-",$board->sur_title);?>"><img src="<?=UPLOAD_PATH.@$this->website_m->get_surfboard_image(['sur_id'=>$board->sur_id],1)->row()->sui_images;?>" alt=""></a>
                    </div>
                    <div class="txt">
                        <h4><a href="<?=base_url();?>product-detail"><?=$board->sur_title; ?></a></h4>
                        <div class="specs"><?=$board->sur_size; ?></div>
                        <div class="ltr"><?=$board->sur_volume; ?></div>
                    </div>
                </div>
            </div>
			<? endforeach; ?>
        </div>
        <div class="bTn"><a href="<?=base_url();?>browse-surfboards" class="webBtn lgBtn colorBtn"><?=$buttons->Surf_Below_Main?></a></div>
    </div>
</section>
<!-- surfboards -->


<section id="plan">
    <div class="contain text-center">
        <h1 class="secHeading">Preços</h1>
        <ul class="lst flex">
		<? $perPkg = $packages->row();  ?>
            <li>
                <div class="planBlk">
                    <!-- <h3>Flex</h3> -->
                    <div class="icon"><i class="fi-glass"></i></div>
                    <div class="price"><?=$perPkg->pkg_title;?></div>
                    <small>R$<?=$perPkg->pkg_price;?>/por <?=$perPkg->pkg_duration;?></small>
                    <ul class="list">
					<? foreach($this->website_m->get_properties_by_id($perPkg->pkg_id)->result() as $property): ?>
                        <li><?=$property->tpp_property;?> <em> <?=$property->tpp_value;?></em></li>
					<? endforeach; ?>
                    </ul>
                    <div class="bTn"><?=$this->website_m->showButton($perPkg->pkg_id);?></div>
                </div>
            </li>
		<? $perSub = $subscription->row(); ?>
            <li>
                <div class="planBlk">
                    <!-- <h3>Prime</h3> -->
                    <div class="icon"><i class="fi-diamond"></i></div>
                    <div class="price"><?=$perSub->pkg_title;?></div>
                    <small>R$<?=$perSub->pkg_price;?>/por <?=$perSub->pkg_duration;?></small>
                    <ul class="list">
                    <? foreach(@$this->website_m->get_properties_by_id($perSub->pkg_id)->result() as $property): ?>
                        <li><?=$property->tpp_property;?> <em> <?=$property->tpp_value;?></em></li>
					<? endforeach; ?>
                    </ul>
                    <div class="bTn"><?=$this->website_m->showButton($perSub->pkg_id);?></div>
                </div>
            </li>
        </ul>
        <div class="bTn text-center"><a href="<?=base_url();?>plans" class="webBtn lgBtn colorBtn"><?=$buttons->Surf_Packages_Below_Main?></a></div>
    </div>
</section>
<!-- plan -->


<section id="location">
    <div class="contain">
        <h1 class="secHeading text-center">Localizações</h1>
        <div class="flexRow flex">
		<? foreach($this->myadmin->get_location_category()->result() as $category): ?>
            <div class="col">
				<div class="icon"><img src="<?=base_url(UPLOAD_PATH.'website/'.$category->loc_image);?>" alt=""></div>
                <h3><?=$category->loc_title?></h3>
                <ul class="lst">
                <? foreach($this->myadmin->get_location(['loc_id'=>$category->loc_id,'lcc_status'=>'active'])->result() as $location): ?>
					<li><a href="<?=base_url('browse-surfboards');?>/0/<?=$location->lcc_id;?>"><?=$location->lcc_title?></a></li>
                <? endforeach; ?>
                </ul>
            </div>
		<? endforeach; ?>
           
        </div>
    </div>
</section>
<!-- location -->


</main>


<style>
html {
  scroll-behavior: smooth;
}

</style>


<?php require_once('includes/footer.php');?>
</body>
</html>