<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	public function __construct(){
		parent::__construct();
		/*
		if (!$this->input->is_ajax_request()) {
		   redirect(base_url());
		}
		*/
	}
	public function login(){
		$return = [
			'error'=>1,
			'message'=>'Invalid Request',
		];
		if($data = $this->input->post()){
			
		}
		return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
	}
	public function register(){
		$return = [
			'error'=>1,
			'message'=>'Invalid Request',
		];
		if($data = $this->input->post()){
			
		}
		return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
	}
	function fb_callback() {
		include_once APPPATH . "libraries/Facebook/autoload.php";
		$fb = new Facebook\Facebook(array(
			'app_id' => FB_APP_ID, // Replace {app-id} with your app id
			'app_secret' => FB_SECRET,
			'default_graph_version' => 'v2.9'
		));
		$helper = $fb->getRedirectLoginHelper();
		try {
			$accessToken = $helper->getAccessToken();
		} catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}

		if (!isset($accessToken)) {
			if ($helper->getError()) {
				header('HTTP/1.0 401 Unauthorized');
				echo "Error: " . $helper->getError() . "\n";
				echo "Error Code: " . $helper->getErrorCode() . "\n";
				echo "Error Reason: " . $helper->getErrorReason() . "\n";
				echo "Error Description: " . $helper->getErrorDescription() . "\n";
			} else {
				header('HTTP/1.0 400 Bad Request');
				echo 'Bad request';
			}
			exit;
		}
		$this->session->set_userdata('fb_access_token', (string) $accessToken);
		$res = $fb->get('/me?scope=email', $accessToken);
		$user = $res->getGraphObject();
		//echo "<pre>";
		//die(print_r($user));
		$data = array();
		$data['access_token'] = $accessToken;
		$data['name'] = $user->getProperty('name');
		$data['email'] = $user->getProperty('email');
		$data['social_id'] = trim($user->getProperty('id'));
		if (!empty($data['name']) && !empty($data['social_id']) && !empty($data['access_token'])) {
			if ($mem = $this->member->socialIdExists('facebook', $data['social_id'])) {

		         $this->session->set_userdata("mem_login",$mem);
		         $count		= $this->db->get_where("tbl_transaction",['mem_id'=>$mem->mem_id,'trx_status'=>'active'])->num_rows();
				if($count==0){
					$this->session->set_flashdata("showSteps","yes");
					redirect("plans");
					exit();
				} else {
				redirect("browse-surfboards");
				exit();
                               }

				
			} else {
				$image='';
				if(!empty($data['image'])){
					
					$image = file_get_contents($data['image']);
					$file_name=md5(rand(100, 1000)) . '_' .time() . '_' . rand(1111, 9999). '.jpg';

					$dir = UPLOAD_VPATH . 'vp/'.$file_name;
					@file_put_contents($dir, $image);

					generate_thumb(UPLOAD_VPATH . "vp/", UPLOAD_VPATH . "p50x50/", $file_name, 50);
					generate_thumb(UPLOAD_VPATH . "vp/", UPLOAD_VPATH . "p150x150/", $file_name, 150);
					generate_thumb(UPLOAD_VPATH . "vp/", UPLOAD_VPATH . "p300x300/", $file_name, 300);

					$image=$file_name;
				}

				if($data['email']!=''){
					$mem_row = $this->member->emailExists($data['email']);
					if (count($mem_row) > 0)
						$data['email']='';

				}

				$arr = explode(" ", $data['name']);
				$new_vals = array(
					'mem_social_type' => 'facebook',
					'mem_social_id' => $data['social_id'],
					'mem_first' => ucfirst($arr[0]),
					'mem_last' => ucfirst($arr[1]),
					'mem_email' => $data['email'],
					'mem_status' => '1',
					'mem_verify' => '1',
					'mem_pic' => $image
				);
				
				$mem_id = $this->member->saveNow($new_vals);
				$this->session->set_userdata('mem_login', $mem_id);
				// $this->sendEmail();
			}
			$this->session->set_flashdata("message_success","Successfully Connected With Facebook");
						
			redirect('account', 'refresh');
			exit;
		}
	}

	function google_callback() {
		include_once APPPATH . "libraries/Google/autoload.php";

		$client_id = GOOGLE_CLIENT_ID;
		$client_secret = GOOGLE_SECRET;
		$redirect_uri = base_url('ajax/google_callback');

		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirect_uri);

		$client->authenticate($_GET['code']);
		$accessToken = $client->getAccessToken();
		$client->setAccessToken($accessToken);

		$service = new Google_Service_Oauth2($client);
		$data = array();
        $user = $service->userinfo->get(); //get user info 

        $data['access_token'] = $accessToken;
        $data['social_id'] = $user->id;
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['image'] = $user->picture;
        if (!empty($data['name']) && !empty($data['social_id']) && !empty($data['access_token'])) {


        	if ($mem = $this->member->socialIdExists('google', $data['social_id'],$data['email'])) {

        		$this->session->set_userdata('mem_login', $mem);
		         $count		= $this->db->get_where("tbl_transaction",['mem_id'=>$mem->mem_id,'trx_status'=>'active'])->num_rows();
				if($count==0){
					$this->session->set_flashdata("showSteps","yes");
					redirect("plans");
					exit();
				} else {
				redirect("browse-surfboards");
				exit();
                                }

        	} else {

        		$image='';
        		if(!empty($data['image'])){
        			
        			$image = file_get_contents($data['image']);
        			$file_name=md5(rand(100, 1000)) . '_' .time() . '_' . rand(1111, 9999). '.jpg';

        			$dir = UPLOAD_VPATH . 'vp/'.$file_name;
        			@file_put_contents($dir, $image);

        			generate_thumb(UPLOAD_VPATH . "vp/", UPLOAD_VPATH . "p50x50/", $file_name, 50);
        			generate_thumb(UPLOAD_VPATH . "vp/", UPLOAD_VPATH . "p150x150/", $file_name, 150);
        			generate_thumb(UPLOAD_VPATH . "vp/", UPLOAD_VPATH . "p300x300/", $file_name, 300);

        			$image=$file_name;
        		}
				
        		$arr = explode(" ", $data['name']);
        		$new_vals = array(
        			'mem_social_type' => 'google',
        			'mem_social_id' => $data['social_id'],
        			'mem_first' => $arr[0],
        			'mem_last' => $arr[1],
        			'mem_email' => $data['email'],
        			'mem_status' => '1',
        			'mem_verify' => '1',
        			'mem_pic' => $image
        		);

        		$this->session->set_userdata('mem_login', $mem_id);
        		$mem_id = $this->member->saveNow($new_vals);
				if($this->website_m->checkEmail()){
					redirect('browse-surfboard', 'refresh');
				}
        		
        		// $this->sendEmail();
        	}
        }
		$this->session->set_flashdata("message_success","Successfully Connected With Google");
		
        redirect('account', 'refresh');
        exit;
    }
    
	
}