<!doctype html>
<html>
<head>
<title>Comunicado – Surf's up Club</title>
<?php

require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">

<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}	
?>

<main>
<br></br>
<section id="comunicado">
    <div class="block teste">
        <div class="contain">
            <div class="content text-center comunicado">
                <h1 class="secHeading">COMUNICADO CORONAVÍRUS (COVID-19) </h1>
                <h1 class="secHeading">Retomada das atividades Surf’s Up Club no dia 1/7</h1>
                <br></br>
                <p>&Eacute; com enorme pesar que anunciamos a suspens&atilde;o das nossas opera&ccedil;&otilde;es do dia 30/04 ao dia 01/07. Seguindo as recomenda&ccedil;&otilde;es das autoridades m&eacute;dicas, todos nossos estabelecimentos parceiros estar&atilde;o fechados durante este per&iacute;odo.<br>
N&atilde;o estamos incentivando a pr&aacute;tica do surfe neste momento e nos isentamos da responsabilidade de qualquer dano que por ventura possa ser causado &agrave; sa&uacute;de de nossos clientes.</p>

<p>
Agradecemos a compreens&atilde;o,<br>
Surf's Up Team</p>

<p>*para d&uacute;vidas ou reclama&ccedil;&otilde;es entrar em contato no contato@surfsupclub.com</p>

                <br></br>                 
            </div>          

        </div>
    </div>
    <div class="block weAre">
    </div>
</section>
<!-- about -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>