<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['about'] = 'home/about';
$route['how-it-works'] = 'home/how';
$route['why-join'] = 'home/why_join';
$route['plans'] = 'home/plans';
$route['plans/select(:num)noplan'] = 'home/plans/$1';
$route['browse-surfboards'] = 'home/browse_surfboards';
$route['browse-surfboards/(:any)/(:num)'] = 'home/browse_surfboards/$1/$2';
$route['product-detail'] = 'home/product_detail';
$route['product-detail/(:num)/(:any)'] = 'home/product_detail/$1/$2';
$route['contact'] = 'home/contact';
$route['signin'] = 'signin/index';
$route['signup'] = 'signup/index';
$route['account'] = 'account/index';
$route['help'] = 'home/help';
$route['toc'] = 'home/toc';
$route['forgot-password'] = 'home/forgot_password';
$route['privacy-policy'] = 'home/privacy_policy';
$route['404_override'] = 'home/error';
$route['translate_uri_dashes'] = FALSE;
//**Below is Admin Panel Links**//
$route[ADMIN] = ADMIN.'/login/index';
/*$route[ADMIN . '/logout'] = ADMIN . '/index/logout';
$route[ADMIN . '/profile'] = ADMIN . '/index/profile';
$route[ADMIN . '/settings'] = ADMIN . '/index/settings';
$route[ADMIN . '/change_password'] = ADMIN . '/index/change_password';
$route[ADMIN . '/forgot'] = ADMIN . '/index/forgot';*/
