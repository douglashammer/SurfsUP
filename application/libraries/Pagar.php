<?php
class Pagar{
	public function __construct($api=''){
		$this->api = $api;
		
	}
	
	/*
	Create Plan Parms
	1: amount*months
	2: month
	3: name
	//////return 
	reuturn 431581
	*/
	public function create_plan($data){
		//echo 'em create_plan()';
		$this->url = 'https://api.pagar.me/1/plans';
		
		$this->body = [
			"amount"=>(int)($data['amount']),
			"api_key"=>$this->api,
			"days"=>$data['days'],//*$data['month'], 
			"installments"=>(int)$data['month'],
			//"installments"=>1,
			"name"=>$data['name'],			
			"payments_methods"=>["credit_card"],
			"charges"=>(int)$data['month']
		];

		//echo '<pre>';var_dump($this->body);die();
		$response = json_decode($this->sendRequest());
		//echo "<pre>";die(print_r($response));
		return $response->id;
	}
	/*
	
	"api_key" : "YOUR API KEY" ,
   "card_number" : "4242424242424242" ,
   "card_cvv" : "122" ,
   "card_holder_name" : " Aardvark Silva " ,
   " card_expiration_date " : " 1220 " ,
   " customer " : {
     " email " : " aardvark.silva@pagar.me " ,
     " name " : " name ",
     "document_number" : "35965816804" ,
    "address" : {
       "zipcode" : "06350270" ,
       "neighborhood" : "neighborhood" ,
       "street" : "street" ,
       "street_number" : "122"
    },
    "phone" : {
       "number" : "87654321" ,
       "ddd" : "11"
    }
  },
  "plan_id" : "89226" ,
   "amount" : 10000
	
	*/
	public function create_subscription($data){
		//echo'create_subscription';

		$this->url = 'https://api.pagar.me/1/subscriptions';
		$this->body = [
			//"amount"=>500,
			"api_key"=>$this->api,
			"card_number"=>$data['cardNo'],
			"card_cvv"=>$data['cardCVC'],
			"card_holder_name"=>$data['cardNo'],
			"card_expiration_date"=>$data['expMonth'].$data['expYear'],
			"customer"=>[
				"address"=>[
					"neighborhood"=>"Cidade",
					"street"=>"Rua Dr.Geraldo Campos Moreira", 
					"street_number"=>"240", 
					"zipcode"=>"04571020"
				],
				"document_number"=>"92545278157",
				"email"=>$data["mem_email"], 
				"name"=>$data["fullName"], 
				"phone"=>[
					"ddd"=>"11", 
					"number"=>"15510101"
				]
			],
		   "plan_id"=>$data["plan_id"], 		
		];
		
		//echo '<pre>';var_dump($this->body);//die();
		$response = json_decode($this->sendRequest());
		//print_r($response);die('fim do debug');
		if(@$response->current_transaction->status=='paid'){
			return [
						"error"=>"no",
						"id"=>$response->id,
						"message"=>$response
					];
					
		}else{
			return [
					"error"=>"yes",
					"message"=>"Algum erro encontrado!"
				];
		}
	}
	/**
	 * CANCELANDO ASSINATURA
	 */
  public function cancelSubscription($idSubscription){
    $this->url = 'https://api.pagar.me/1/subscriptions/'.$idSubscription.'/cancel';
		$this->body = [
			"api_key"=>$this->api
		];
		
		$response = json_decode($this->sendRequest());
		if(@$response->current_transaction->status=='paid'){
			return [
						"error"=>"no",
						"id"=>$response->id,
						"message"=>$response
					];
					
		}else{
			return [
					"error"=>"yes",
					"message"=>"Algum erro encontrado!"
				];
		}
  }


	/**
	 * BUSCANDO UM PLANO JÁ EXISTENTE APENAS PELO VALOR
	 */
	public function findPlan($data){
		//var_dump($data);die();
		//https://api.pagar.me/1/plans?count=1&page=1&amount=7450&days=30&api_key=minha_api_key
		$this->url = 'https://api.pagar.me/1/plans';
		$this->body = [
			"api_key"=>$this->api,
			"amount"=>(int)$data['amount'],
			"days"=> $data['days'], 
			"page"=> "1"
		];

		$response = json_decode($this->sendRequest("GET"));
//		echo '<pre>';
//		var_dump($response);die();
		return $response[0]->id;

	}

	public function create_card($data){
		$this->url = 'https://api.pagar.me/1/cards';
		$this->body = [
			"api_key"=>$this->api,
			"card_hash"=>$data['card_id']	
		];
		$response = json_decode($this->sendRequest());

		return $response->id;
	}
	
	/**	  segundo vem para o sendRequest	 */
	private function sendRequest($customrequest='POST'){

		$curl = curl_init();
	
	    curl_setopt_array($curl, array(
	        CURLOPT_RETURNTRANSFER => true,
	        CURLOPT_URL => $this->url,
	        CURLOPT_CUSTOMREQUEST => $customrequest,
	        CURLOPT_HTTPHEADER => ['content-type: application/json'],
			CURLOPT_POSTFIELDS => json_encode($this->body),
			CURLOPT_ENCODING => 'utf-8'
	    ));
	
	    $response = curl_exec($curl);
	    $err = curl_error($curl);

		curl_close($curl);
		return $response;
	}
	
	
	public function chargeNow($data = array()){
		$this->url = 'https://api.pagar.me/1/transactions';
		if(empty($data)){
			return false;
		}
		$data['mem_phone'] = "+90078601";
		
		//echo'<pre>';var_dump($data['trx_amount']);die;
		$template =$this->template();
		$template->api_key = $this->api;
		$template->amount = $data['trx_amount'];
		$template->card_number = $data['cardNo'];
		$template->card_cvv = $data['cardCVC'];
		$template->card_expiration_date = $data['expMonth'].$data['expYear'];
		$template->card_holder_name = $data['fullName'];
		$template->customer->type= "individual";//campo obrigatório
		$template->customer->external_id = "#surf".time()."boards";
		$template->customer->name = $data['fullName'];
		$template->customer->country = strtolower($data['mem_country']);
		$template->customer->email = $data['mem_email'];
		$template->customer->documents[0]->type = strpos($data['mem_passport'],'.')>2?'cpf':'passport';
		if(!empty($data['mem_passport'])){
			$template->customer->documents[0]->number = preg_replace('/[^0-9]+/', '', $data['mem_passport']);
		}else{
			$template->customer->documents[0]->number ='none';
		}
		if(!empty($data['mem_phone'])){
			$template->customer->phone_numbers = [$data['mem_phone']];
		}else{
			$template->customer->phone_numbers = '999999999';
		}
		$template->billing->name = $data['fullName'];
		$template->billing->address->country = strtolower($data['mem_country']);
		if(!empty($data['mem_zip'])){
			$template->billing->address->state = $data['mem_zip'];
		}else{
			$template->billing->address->state = 'none';
		}
		if(!empty($data['mem_city'])){
			$template->billing->address->city = $data['mem_city'];
		}else{
			$template->billing->address->city = 'none';
		}
		if(!empty($data['mem_address'])){
			$template->billing->address->street = $data['mem_address'];
		}else{
			$template->billing->address->street = 'none';
		}
		if(!empty($data['mem_zip'])){
			$template->billing->address->zipcode = $data['mem_zip'];
		}else{
			$template->billing->address->zipcode = 'none';
		}
		
		$this->body = $template;

		$res = json_decode($this->sendRequest());

		if(@$res->status=='paid'){
			$return = [
						"error"=>"no",
						"message"=>$res
					];
					
		}else{
			$return = [
					"error"=>"yes",
					"message"=>$res->errors[0]->message
				];
		}
		return $return;
		
	}
	
	/*	primeiro chama este template	*/
	private function template(){

		$return  =   json_decode('{
				"api_key": "SUA_API_KEY",
				"amount": 100,
				"card_number": "4111111111111111",
				"card_cvv": "",
				"card_expiration_date": "",
				"card_holder_name": "",
				"customer": {
				  "external_id": "",
				  "name": "",
				  "type": "individual",
				  "country": "br",
				  "email": "",
				  "documents": [
					{
					  "type": "cpf",
					  "number": "00000000000"
					}
				  ],
				  "phone_numbers": ["+5511999998888"],
				  "birthday": "1970-01-01"
				},
				"billing": {
				  "name": "Trinity Moss",
				  "address": {
					"country": "br",
					"state": "sp",
					"city": "Cotia",
					"neighborhood": "Rio Cotia",
					"street": "Rua Matrix",
					"street_number": "9999",
					"zipcode": "06714360"
				  }
				},
				"shipping": {
				  "name": "Neo Reeves",
				  "fee": 0,
				  "delivery_date": "2000-12-21",
				  "expedited": true,
				  "address": {
					"country": "br",
					"state": "sp",
					"city": "Cotia",
					"neighborhood": "Rio Cotia",
					"street": "Rua Matrix",
					"street_number": "9999",
					"zipcode": "06714360"
				  }
				},
				"items": [
				  {
					"id": "pkg001",
					"title": "pkgSurfboard",
					"unit_price": 0,
					"quantity": 1,
					"tangible": true
				  }
				]
		}
			');


		return $return;
	}
	
	
}