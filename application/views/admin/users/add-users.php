
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Users</a>
		</li>
		<li class="active">
			<strong>Add/Edit Users</strong>
		</li>
	</ol>
	
	<h3>Adicionar / editar usuários</h3>
	<form role="form" action="" method="post" class="form-horizontal form-groups-bordered">
			
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">O email</label>
				
				<div class="col-sm-5">
					<input type="email" required autocomplete="off" name="usr_email" value="<?=@$row->usr_email;?>" class="form-control" id="field-1" placeholder="Email Address">
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Definir nova senha</label>
				
				<div class="col-sm-5">
					<input type="password" autocomplete="off" required name="usr_password"  class="form-control" id="field-1" placeholder="Set New Password">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Selecione o local</label>
				
				<div class="col-sm-5">
					<select name="lcc_id" required class="form-control">
						<option value="">--Select--</option>
					<? foreach($get_category->result() as $location): ?>
						<option <?=$location->lcc_id==@$row->lcc_id?'selected':'';?> value="<?=$location->lcc_id;?>"><?=$location->lcc_title;?></option>
					<? endforeach; ?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" name="usr_status" <?=(@$row->usr_status=='active')?'checked':'';?> />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-8">
					<input type="submit"  class="btn btn-success btn-lg" value="Save">
				</div>
			</div>
	</form>
